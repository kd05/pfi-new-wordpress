Change Log
All notable changes to this project will be documented in this file, formatted via [this recommendation](http://keepachangelog.com/).

## [1.1.1] - 2018-03-19
### Fixed
- JS file not loading on frontend, causing error.

## [1.1.0] - 2018-03-15
### Changed
- Refactored addon and improved code.

### Fixed
- Zero (0) math equation answers not being allowed.

## [1.0.3] - 2017-06-20
### Fixed
- Issue with QA PHP validation.

## [1.0.2] - 2017-06-15
### Fixed
- Missing input class causing equation validation issues.

## [1.0.1] - 2017-06-13
### Changed
- Updated captcha field to new field class format.

### Fixed
- Field size issue in the form builder when using Math format.

## [1.0.0] - 2016-08-03
- Initial release.
