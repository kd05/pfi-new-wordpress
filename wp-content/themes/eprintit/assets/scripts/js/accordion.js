jQuery(document).ready(function ($) {
    toggleAccordianOnClick();
    openAccordianOnLoad();

    /*************************************************
     * Opens accordians on Load that have class open
     *************************************************/
    function openAccordianOnLoad(){
        var accordians = $('.gp-accordian-container');
        $.each(accordians, function(){
            if($(this).hasClass('open')){
                $(this).closest('.gp-accordian-container').find('.accordian-content').slideDown();
            }
        });
    }
    /*************************************************
     * Toggle Accordian on Click
     *************************************************/
    function toggleAccordianOnClick(){
        $('.gp-accordian-container .accordian-title').on('click', function(){
            $(this).closest('.gp-accordian-container').toggleClass('open');
            if($(this).closest('.gp-accordian-container').hasClass('open')){
                $(this).closest('.gp-accordian-container').find('.accordian-content').slideDown();
            } else {
                $(this).closest('.gp-accordian-container').find('.accordian-content').slideUp();
            }
        });
        $('.gp-accordian-container .sub-accordian-title').on('click', function(){
            $(this).closest('.sub-accordian-container').toggleClass('open');
            if($(this).closest('.sub-accordian-container').hasClass('open')){
                $(this).closest('.sub-accordian-container').find('.sub-accordian-content').slideDown();
            } else {
                $(this).closest('.sub-accordian-container').find('.sub-accordian-content').slideUp();
            }
        });
    }
});