jQuery(document).ready(function ($) {


    // Hamburger menu click handler
    $("#nav-btn").on("click", function (ev) {
        ev.preventDefault();

        $(this).toggleClass("is-active");
        $("nav").toggleClass('active');
        $("body").toggleClass("navactive");
    });

    var menu = $("nav ul.menu");

    menu.find('.menu-item-has-children').each(function () {
        $(this).find("> a").after('<span class="toggle-submenu">&#xf107;</span>');
    });

    $('body').on('click', '.toggle-submenu', function () {

        var submenu = $(this).parent().find('ul.sub-menu');
        var icon = $(this);

        if (!$(this).hasClass('open') && !submenu.hasClass('animating')) {

            submenu.addClass('animating').slideDown(300, function () {
                icon.addClass('open');
                submenu.removeClass('animating');
            });
        } else {
            submenu.addClass('animating').slideUp(300, function () {
                icon.removeClass('open');
                submenu.removeClass('animating');
            });
        }
    });


});
