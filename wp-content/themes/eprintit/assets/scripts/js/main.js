(function ($) {

    var eprintit = {};

    eprintit.shortcodeFix = function(){
        var p = $('.shortcode p:empty');
        $.each(p, function(){
            $(this).remove();
        });
    };

    /**
     * REVEAL CONTENT
     * Reveals the content when the user scrolls to it
     * @param       elem        element     The element to reveal
     */
    eprintit.revealContent = function(elem) {

        var scrolledAmount = $(window).scrollTop();
        var elemTop        = elem.offset().top;
        var windowHeight   = window.innerHeight;

        if (elem[0].hasAttribute('data-delay')) {
            elem.css({
                '-webkit-transition-delay': elem.attr('data-delay') + 'ms',
                '-moz-transition-delay': elem.attr('data-delay') + 'ms',
                '-o-transition-delay': elem.attr('data-delay') + 'ms',
                'transition-delay': elem.attr('data-delay') + 'ms'
            });
        }

        if (((scrolledAmount + windowHeight) - (windowHeight / 12) >= elemTop) && !elem.hasClass('show')) {
            elem.addClass('show');
        }
    };

    eprintit.header = function () {

        var self = this;

        // an empty placeholder div behind the fixed header to ensure
        // content never starts on top of the header. the css will try to
        // set the height correctly, but javascript will make sure its always
        // exactly correct
        self.set_placeholder_height = function () {

            var header = $('.site-header');
            var ph = $('.fixed-header-placeholder');

            if (ph.length > 0) {
                if (header.hasClass('is-fixed')) {
                    var header_height = header.length > 0 ? parseInt(header.css('height')) : undefined;
                    if (header_height) {
                        ph.css('height', header_height + 'px');
                    }
                } else {
                    ph.css('height', 0);
                }
            }
        };

        // add a class to the header indicating its scrolled down the page
        // (possibly by only more than 0 pixels), so that we can add a box shadow
        // if needed.
        self.scrolled_class = function () {

            var header = $('.site-header');

            if (header.length > 0) {

                var top = parseInt($(window).scrollTop());

                if (top === 0) {
                    header.removeClass('scrolled');
                } else {
                    header.addClass('scrolled');
                }
            }

        };

        self.set_placeholder_height();
        self.scrolled_class();
        $(window).on('resize', function () {
            self.set_placeholder_height();
        });

        $(window).on('scroll', function () {
            self.scrolled_class();
        });
    };

    // Doc Ready
    $(document).ready(function () {
        eprintit.header();
        eprintit.shortcodeFix();

        // desktop only
        if (window.innerWidth > 1024) {

            $('body.home .scroll-animate').each(function() {
                var elem = $(this);
                eprintit.revealContent(elem);
                $(window).on('scroll', function() {
                    eprintit.revealContent(elem);
                });
            });

        } else {
            $('.scroll-animate').addClass('show');
        }

        $(window).on('resize', function () {

            setTimeout(function() {

                if ($(window).outerWidth() <= 1024) {
                    $('.scroll-animate').addClass('show');
                } else if ($(window).outerWidth() > 1024) {
                    $(window).trigger('scroll');
                    $('body.home .scroll-animate').each(function () {
                        var elem = $(this);
                        eprintit.revealContent(elem);

                        $(window).on('scroll', function () {
                            eprintit.revealContent(elem);
                        });
                    });
                }
            }, 500);

        });
    })

}(jQuery));