var mapEl = document.getElementById('hero-map');
if (mapEl) {
    var mapIcon = theme.directory + '/assets/images/marker-icon-sm.png';
    var posIcon = theme.directory + '/assets/images/marker-icon.png';
    var markersArray = [];
    var resultMap;
    var defaultLat = 43.653226;
    var defaultLng = -79.38318429999998;
    var defaultZoom = 10;
    var closestLocations = [];
    // Since there is no geolocation without https, use defaults
    var currentPos = {
        lat: defaultLat,
        lng: defaultLng
    };
    var infoWindow, bounds, i, listHtml;
}

jQuery(document).ready(function ($) {

    $('.PrintLocations .sidebar-tab').on('click', function () {
        var sidebar = $(this).parent();
        var sidebarWidth = sidebar.outerWidth();
        var tab = $(this);

        if (tab.hasClass('closed') && !tab.hasClass('animating')) {
            tab.addClass('animating');
            sidebar.animate({left: 0}, 400, function () {
                tab.removeClass('animating closed');
            });
        } else if (!tab.hasClass('animating')) {
            tab.addClass('animating');
            sidebar.animate({left: '-' + sidebarWidth + 'px'}, 400, function () {
                tab.removeClass('animating').addClass('closed');
            });
        }
    });

    var sidebar = $(".PrintLocations #search-wrapper");
    var resultsList = sidebar.find(".results-wrapper");

    // Sidebar Search form submission
    sidebar.find("form").on('submit', function (ev) {
        ev.preventDefault();

        sidebar.addClass("open");
        resultsList.removeClass("show-details");
    });

    sidebar.on("click", ".results-list .result", function (ev) {
        resultsList.addClass("show-details");
    });

    sidebar.on("click", ".back-to-results", function (ev) {
        resultsList.removeClass("show-details");
    });
});

/**
 * Sets the current geo-position on map
 */
function setCurrentPosition() {

    // Try HTML5 geolocation.
    if (navigator.geolocation) {

        infoWindow = new google.maps.InfoWindow;

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var content = 'Your Location';
                addDefaultMarker(pos, content);
                // Set
                currentPos = pos;
                resultMap.setCenter(pos);
            }, function () {
                handleLocationError(true, infoWindow, resultMap.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false, infoWindow, resultMap.getCenter());
        }

    } else {
        handleLocationError(false, infoWindow, resultMap.getCenter());
    }
}

/**
 * Add a new marker to the map based on the provided location (latlng).
 */
function addMarker(latLng, infoWindowData, infoWindow, storeId) {

    var marker, store;
    if (storeId) {
        store = storeId;
    }
    marker = new google.maps.Marker({
        position: latLng,
        map: resultMap,
        animation: google.maps.Animation.DROP,
        optimized: false, //fixes markers flashing while bouncing
        title: '',
        icon: mapIcon,
        storeId: store
    });

    // Store the marker for later use.
    markersArray.push(marker);

    // Build infowindow content
    var content = '<p>' + infoWindowData.NAME + '</p><p>' + infoWindowData.CITY + '</p><p>' + infoWindowData.DISPLAYNAME + '</p>';

    // Allow each marker to have an info window
    google.maps.event.addListener(marker, 'click', (function (marker, i) {
        return function () {
            infoWindow.setContent(content);
            infoWindow.open(resultMap, marker);
        }
    })(marker, i));

}

/**
 * Adds all locations to map
 */
function addToMap(locations) {
    // Add locations
    for (var i in locations) {
        // Add the location marker to the map
        var latLng = new google.maps.LatLng(locations[i].LATITUDE, locations[i].LONGITUDE);
        addMarker(latLng, locations[i], infoWindow);
    }

    var maxZoom = 10;
    bounds = new google.maps.LatLngBounds();
    // Make sure we don't zoom to far.
    google.maps.event.addListenerOnce(resultMap, "bounds_changed", function (event) {
        if (this.getZoom() > maxZoom) {
            this.setZoom(maxZoom);
        }
    });
}

/**
 * Gets the locations within a distance
 * @param locationDistance
 */
function getClosestLocations(locationDistance) {
    if (!locationDistance) {
        locationDistance = 50; // 50 Kilometers
    }

    var locations = map_locations;
    locations = JSON.parse(JSON.stringify(locations));
    for (var i in locations) {
        // Build position
        var pos = {
            lat: locations[i].LATITUDE,
            lng: locations[i].LONGITUDE
        };
        // Get the distance
        var distance = calculateDistance(currentPos, pos);
        if (distance <= locationDistance) {
            closestLocations.push(locations[i]);
        }
    }

}

/**
 * Geolocation error handler
 * @param browserHasGeolocation
 * @param infoWindow
 * @param pos
 */
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');

    // Use the default pos if found
    if (currentPos) {
        var content = 'Using Default Location.';
        addDefaultMarker(currentPos, content);
        resultMap.setCenter(pos);
    }
}

function addDefaultMarker(pos, content) {

    var marker = new google.maps.Marker({
        position: pos,
        map: resultMap,
        animation: google.maps.Animation.DROP,
        optimized: false, //fixes markers flashing while bouncing
        title: '',
        icon: posIcon
    });

    // Allow each marker to have an info window
    google.maps.event.addListener(marker, 'click', (function (marker, i) {
        return function () {
            infoWindow.setContent(content);
            infoWindow.open(resultMap, marker);
        }
    })(marker, i));
}

/**
 * Calculate distance between two positions
 * @param posA
 * @param posB
 * @returns {number}
 * @source http://www.geodatasource.com
 */
function calculateDistance(posA, posB) {

    var lon1 = posA.lng;
    var lat1 = posA.lat;
    var lon2 = posB.lng;
    var lat2 = posB.lat;
    var unit = "K"; // kilometers

    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
        dist = 1;
    }
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    if (unit == "K") {
        dist = dist * 1.609344
    }
    if (unit == "N") {
        dist = dist * 0.8684
    }
    return dist
}

/**
 * Map Initialization
 * Initialized in google maps initialization using callback
 */
function mapInitialize() {

    // Make sure there is a map on page
    if (!mapEl) {
        return;
    }

    var styles = [
        {
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#444444"
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "administrative.locality",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "administrative.locality",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "administrative.neighborhood",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#f2f2f2"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "hue": "#ff0000"
                }
            ]
        },
        {
            "featureType": "landscape.man_made",
            "elementType": "geometry",
            "stylers": [
                {
                    "lightness": "100"
                }
            ]
        },
        {
            "featureType": "landscape.man_made",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "landscape.natural",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "lightness": "100"
                }
            ]
        },
        {
            "featureType": "landscape.natural",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "landscape.natural.landcover",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "landscape.natural.terrain",
            "elementType": "geometry",
            "stylers": [
                {
                    "lightness": "100"
                }
            ]
        },
        {
            "featureType": "landscape.natural.terrain",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "off"
                },
                {
                    "lightness": "23"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "all",
            "stylers": [
                {
                    "saturation": -100
                },
                {
                    "lightness": 45
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#ffaf11"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "all",
            "stylers": [
                {
                    "color": "#ffd900"
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#cccccc"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        }
    ];

    var mapOptions = {
        zoom: defaultZoom,
        center: new google.maps.LatLng(defaultLat, defaultLng),
        styles: styles
    };

    // Get the map
    resultMap = new google.maps.Map(mapEl, mapOptions);

    // Start geocode
    var geocoder = new google.maps.Geocoder();

    // Geolocate
    setCurrentPosition();

    // Get the closest locations
    getClosestLocations();

    // Add to map
    addToMap(closestLocations);

}