(function ($) {
    'use strict';
    var j = $.noConflict();


    j(document).ready(function () {

        let customerSlider = j('.customers-slider'),
            testimonialSlider = j('.testimonial-slider'),
            featuredSlider = j('.featured-slider'),
            instructionSlider = j('.instruction-slider'),
            logoSlider = j('.logo-slider'),
            potentialSlider = j('.potentials-slider'),
            accreditationSlider = j('.accreditations-slider'),
            ourteamSlider = j('.team-slider'),
            ourteamTabletSlider = j('.team-slider-tablet'),
            ourteamPhoneSlider = j('.team-slider-phone')


        if (ourteamSlider) {

            ourteamSlider.slick({
                variableWidth: true,
                infinite: true,
                centerMode: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                draggable: true,
                speed: 300,
                prevArrow:'.left-team-arrow',
                nextArrow: '.right-team-arrow',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 855,
                        settings: {
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })

            // ourteamTabletSlider.slick({
            //     variableWidth: true,
            //     infinite: true,
            //     centerMode: true,
            //     slidesToShow: 1,
            //     slidesToScroll: 1,
            //     dots: false,
            //     arrows: true,
            //     draggable: true,
            //     speed: 300,
            //     prevArrow:'.left-team-tablet-arrow',
            //     nextArrow: '.right-team-tablet-arrow',
            //     responsive: [
            //         {
            //             breakpoint: 1024,
            //             settings: {
            //                 slidesToScroll: 3,
            //                 infinite: true,
            //             }
            //         }
            //         // You can unslick at a given breakpoint now by adding:
            //         // settings: "unslick"
            //         // instead of a settings object
            //     ]
            // })

            ourteamPhoneSlider.slick({
                variableWidth: true,
                infinite: true,
                centerMode: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                draggable: true,
                speed: 300,
                prevArrow:'.left-team-phone-arrow',
                nextArrow: '.right-team-phone-arrow',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 855,
                        settings: {
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })
        }


        if (potentialSlider) {

            potentialSlider.slick({
                variableWidth: true,
                infinite: true,
                centerMode: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                draggable: true,
                centerPadding: '60px',
                speed: 300,
                prevArrow: '.left-potential-arrow',
                nextArrow: '.right-potential-arrow',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 855,
                        settings: {
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })

            // potentialSlider.on('beforeChange', function (e, slick, currentSlide, nextSlide) {
                // console.log('====================================');
                // console.log('next slide', nextSlide);
                // console.log('====================================');
            // })
        }

        if (logoSlider) {

            logoSlider.slick({
                variableWidth: true,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                prevArrow: '.left-arrow',
                nextArrow: '.right-arrow',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 855,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })
        }
        if (accreditationSlider) {

            accreditationSlider.slick({
                variableWidth: true,
                infinite: true,
                draggable: true,
                slidesToShow: 7,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                prevArrow: '.left-arrow',
                nextArrow: '.right-arrow',
                responsive: [
                    {
                        breakpoint: 1440,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 855,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })
        }
        if (instructionSlider) {

            instructionSlider.slick({
                infinite: true,
                centerMode: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                centerPadding: '30px',
                dots: false,
                arrows: true,
                draggable: true,
                speed: 300,
                prevArrow: '.left-arrow',
                nextArrow: '.right-arrow',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 855,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })
        }


        if (customerSlider) {

            customerSlider.slick({
                variableWidth: true,
                infinite: true,
                slidesToShow: 5,
                slidesToScroll: 1,
                centerPadding: '30px',
                dots: false,
                arrows: true,
                draggable: true,
                speed: 300,
                prevArrow: '.left-arrow',
                nextArrow: '.right-arrow',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 855,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })
        }

        if (testimonialSlider) {

            testimonialSlider.slick({
                variableWidth: true,
                infinite: true,
                centerMode: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                draggable: true,
                speed: 300,
                prevArrow: '.left-testimonial-arrow',
                nextArrow: '.right-testimonial-arrow',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            // slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 855,
                        settings: {
                            // slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: true,
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            })
        }


        if (featuredSlider) {

            featuredSlider.slick({
                variableWidth: true,
                dots: false,
                arrows: true,
                infinite: true,
                draggable: true,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 1,
                prevArrow: '.left-arrow',
                nextArrow: '.right-arrow',
                responsive: [
                    {
                        breakpoint: 1368,
                        settings: {
                            draggable: false,
                        }
                    },
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 855,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 640,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 499,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        }


    });

}(jQuery));
