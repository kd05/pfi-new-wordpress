(function ($) {

    // TODO: optimize / clean this up

    /**
     * CHANGE STEP
     * Changes visible stepper content
     *
     * @param       stepper         element         The current stepper being interacted with
     * @param       control         element         The control that was clicked
     */
    function changeStep(stepper, control) {

        var targetStep;
        var currentActiveStep = stepper.find('.step.active');
        var stepperContent    = stepper.find('.stepper-content-wrapper');
        var nextContentArrow  = stepper.find('.stepper-content-arrows .content-arrow.next');
        var prevContentArrow  = stepper.find('.stepper-content-arrows .content-arrow.prev');

        // check if arrow was clicked
        if (control.hasClass('content-arrow')) {

            if (control.hasClass('next')) {

                // don't trigger if active step is last step (because there will be nothing after)
                if (!currentActiveStep.is(':last-of-type')) {
                    targetStep = currentActiveStep.next('.step');

                    // trigger shiftStepperNav function to move nav over if necessary
                    if (targetStep.prev('.step').hasClass('last-visible') && stepper.find('.step-arrow.next').hasClass('show')) {
                        stepper.find('.step-arrow.next').click();
                    }
                }
            } else if (control.hasClass('prev')) {

                // don't trigger if active step is first step (because there will be nothing before)
                if (!currentActiveStep.is(':first-of-type')) {
                    targetStep = currentActiveStep.prev('.step');

                    // trigger shiftStepperNav function to move nav over if necessary
                    if (targetStep.hasClass('before-first-visible') && stepper.find('.step-arrow.prev').hasClass('show')) {
                        stepper.find('.step-arrow.prev').click();
                    }
                }
            }
        } else if (control.hasClass('step-inner')) {
            targetStep = control.parent('.step');
        }

        // hide / show content arrows depending on if there are more steps in the given direction
        if (targetStep.is(':last-of-type')) {
            nextContentArrow.removeClass('show');
        } else {
            nextContentArrow.addClass('show');
        }
        if (targetStep.is(':first-of-type')) {
            prevContentArrow.removeClass('show');
        } else {
            prevContentArrow.addClass('show');
        }

        // mark every previous step as done and replace icon number with checkmark
        targetStep.prevAll('.step').removeClass('before-active active').addClass('done').each(function() {
            $(this).find('.icon > span').html('<i class="fa fa-check"></i>');
        });

        // remove special classes from each step ahead and restore icon numbers
        targetStep.nextAll('.step').removeClass('before-active done active').each(function() {
            $(this).find('.icon > span').html($(this).attr('data-step'));
        });

        // make target step active & replace icon number
        targetStep.removeClass('before-active done').addClass('active').find('.icon > span').html(targetStep.attr('data-step'));
        targetStep.prev('.step').addClass('before-active');
        stepperContent.find('.stepper-content.active').removeClass('active');
        stepperContent.find('.stepper-content[data-content="' + targetStep.attr('data-step') + '"]').addClass('active');
    }


    /**
     * SHIFT STEPPER NAV
     * Shifts the stepper navigation via arrows
     *
     * @param       stepper         element         The current stepper being interacted with
     * @param       control         element         The control that was clicked
     */
    function shiftStepperNav(stepper, control) {

        var nextPosition;
        var nextLastVisible;
        var nextBeforeFirstVisible;
        var nextVisibleStepIndex;
        var currentPosition    = parseFloat(stepper.find('.step').first().css('left').split('px')[0]);
        var stepWidth          = parseFloat(stepper.find('.step').outerWidth());
        var currentLastVisible = stepper.find('.step.last-visible');
        var nextStepArrow      = stepper.find('.step-arrow.next');
        var prevStepArrow      = stepper.find('.step-arrow.prev');

        // assign the next last visible step (for styling) & calculate new position
        if (control.hasClass('next')) {
            nextLastVisible = currentLastVisible.next('.step');
            nextPosition    = (currentPosition - stepWidth);
        } else {
            nextLastVisible = currentLastVisible.prev('.step');
            nextPosition    = (currentPosition + stepWidth);
        }

        // remove all extra classes
        stepper.find('.step').removeClass('last-visible before-first-visible');

        // find the next before first visible step based on index of next visible step
        nextVisibleStepIndex   = nextLastVisible.index();

        if (nextVisibleStepIndex > 3) {
            nextBeforeFirstVisible = stepper.find('.step').eq(parseInt(nextVisibleStepIndex) - 4);
        }

        // move each step to new position
        stepper.find('.step').each(function() {
            currentPosition = $(this).css('left').split('px')[0];
            $(this).css('left', nextPosition + 'px');
        });

        // assign new last visible step & before first visible step (for styling)
        nextLastVisible.addClass('last-visible');

        // show/hide nav arrows depending on if there are more steps to show or not
        if (typeof nextBeforeFirstVisible === 'undefined') {
            prevStepArrow.removeClass('show');
        } else {
            nextBeforeFirstVisible.addClass('before-first-visible');
            prevStepArrow.addClass('show');
        }
        if (nextLastVisible.is('.step:last-of-type')) {
            nextStepArrow.removeClass('show');
        } else {
            nextStepArrow.addClass('show');
        }
    }


    /**
     * RESET STEPPER NAV
     * Resets the position of the stepper nav elements to avoid breakage
     *
     * @param           stepper             element             The stepper to reset
     */
    function resetStepperNav(stepper) {

        stepper.find('.step').each(function() {
            $(this).css('left', 0).removeClass('done last-visible before-active before-first-visible active');
            $(this).find('.icon > span').html($(this).attr('data-step'));
        });

        stepper.find('.step:first-of-type').addClass('active');
        stepper.find('.step').eq(3).addClass('last-visible');

        stepper.find('.step-arrow.prev').removeClass('show');

        if (stepper.find('.step').length > 4) {
            stepper.find('.step-arrow.next').addClass('show');
        }
    }


    // Doc Ready
    $(document).ready(function () {

        if ($('.Stepper').length) {

            $('body').on('click', '.stepper-wrapper .content-arrow.show, .stepper-wrapper .step-inner', function() {

                if (!$(this).parent('.step').hasClass('active')) {
                    changeStep($(this).closest('.stepper-wrapper'), $(this));
                }
            });

            $('body').on('click', '.stepper-wrapper .step-arrow.show', function() {
                shiftStepperNav($(this).closest('.stepper-wrapper'), $(this));
            });

            $(window).on('resize', function() {

                // reset each stepper
                $('.Stepper').each(function() {
                    resetStepperNav($(this));
                });
            });
        }
    });

}(jQuery));