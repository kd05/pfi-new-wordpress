jQuery(document).ready(function ($) {

    // Video Pop Up Initiate
    $(".js-modal-btn, .popup-video").modalVideo();
    $(".popup-vimeo-video").modalVideo({channel:'vimeo'});



    // On CLick Scroll
    const scrollEffect = function (nextPanelId, aTag) {
        console.log(`Next panel id: ${nextPanelId}`);
        if(nextPanelId != ""){
            console.log("Not Empty Panel ID");
            $("#"+nextPanelId+" .learn-more , #"+nextPanelId+" .solution-top, #"+nextPanelId+" .solution-main-content").removeClass("scroll-animate");
            $('html, body').animate({
                scrollTop: $("#"+nextPanelId).offset().top
            }, 1700);
        } else{
            aTag.closest( ".shortcode" ).css({
                "color": "black",
                "border": "2px solid green"
            });

            console.log(autoSelectNextPanel.next());
            // autoSelectNextPanel.remove();
            $('html, body').animate({
                scrollTop: autoSelectNextPanel.offset().top
            }, 1700);
        }

    };

    const animateFunc = function (nextPanel) {
        for(var i = 0; i<nextPanel.length; i++) {
            nextPanel[i].addEventListener('click',function (e) {
                var nextPanelId = $( this ).attr("data-animate");
                // Remoing animation

                // scrollEffect(nextPanelId,this);


                if(nextPanelId != ""){
                    console.log("Not Empty Panel ID");
                    $("#"+nextPanelId+" .learn-more , #"+nextPanelId+" .solution-top, #"+nextPanelId+" .solution-main-content").removeClass("scroll-animate");
                    $('html, body').animate({
                        scrollTop: $("#"+nextPanelId).offset().top
                    }, 1700);
                } else{
                    // console.log(e);
                   var autoSelectNextPanel = this.closest( ".shortcode" );

                   console.log(autoSelectNextPanel.nextElementSibling);

                    // console.log(autoSelectNextPanel.next());
                    // autoSelectNextPanel.remove();
                    $('html, body').animate({
                        scrollTop: autoSelectNextPanel.nextElementSibling.offset().top
                    }, 1700);
                }




            });
        }
    };


    const setFixSelect = function (panel_id) {
        $(".fix-circle").removeClass("active");
        $(".LeftFixScrollAnimation ."+panel_id).addClass("active");
        $(".fix-circle."+panel_id+" span").text($("#"+panel_id).attr("scroll-name"));
    };


    // ************   Auto Select Fix Scroll    ************
    const autoFixSelect = function (nextPanel) {
        // console.log(nextPanel);
        var i = 0;
        Array.from(nextPanel).forEach(function (panel, index) {
            // console.log(panel, typeof panel, panel.getAttribute("data-animate"));
            let panel_animation = panel.getAttribute("data-animate");

            var distance_top = $('#'+panel_animation).offset().top;
            var distance_botom = $('#'+panel_animation).offset().top + $('#'+panel_animation).outerHeight(true);
            var $window = $(window);
            // console.log("Windows :"+$window.scrollTop() +" || distance" + distance_top+ " || Bottom: "+ distance_botom);

            if ( $window.scrollTop() >= distance_top && $window.scrollTop() <= distance_botom) {
                i = 1;
                setFixSelect(panel_animation);
            } else{
                if(i == 0){
                    $(".fix-circle").removeClass("active");
                }
            }
        });
    };


    // ************   Next Panel Scroll    ************
    var isNextPanel = document.getElementsByClassName('next-panel-animate');
    if (isNextPanel.length > 0) {
        var nextPanel = document.querySelectorAll('.next-panel-animate a');
        animateFunc(nextPanel);
    }


    var isScrollNextPanel = document.getElementsByClassName('LeftFixScrollAnimation');
    if (isScrollNextPanel.length > 0) {
        var scrollNextPanel = document.querySelectorAll('.LeftFixScrollAnimation .fix-circle');
        animateFunc(scrollNextPanel);

        window.addEventListener("scroll",function (e) {
            autoFixSelect(scrollNextPanel);
        });
    }





    // News Mobile Slick Scroll
    const news_device_width = 768;
    function mobileNewsSlider() {

        $(".news-container").slick({
            dots: false,
            infinite: true,
            // centerMode: true,
            slidesToShow: 1,
            slidesToScroll: 1
        });
    }
    // console.log(window.innerWidth,news_device_width);
    if(window.innerWidth < news_device_width) {
        // console.log("here");
        mobileNewsSlider();
    }

    $(window).resize(function(e){
        if(window.innerWidth < news_device_width) {
            if(!$('.news-container').hasClass('slick-initialized')){
                mobileNewsSlider();
            }

        }else{
            if($('.news-container').hasClass('slick-initialized')){
                $('.news-container').slick('unslick');
            }
        }
    });




    // ****************    Three Panel     ****************

    function open_single_three_panel() {
        $(".single_column_three_panel").addClass( "hide_panel" ).removeClass("full_width");
        $(".single-column-content").removeClass( "show_content" );

        $(this).parent( ".single_column_three_panel" ).removeClass( "hide_panel" ).addClass("full_width");
        $(this).siblings().addClass( "show_content" );
    }


    function close_single_three_panel() {
        $(".single_column_three_panel").removeClass( "hide_panel" ).removeClass("full_width");
        $(".single-column-content").removeClass( "show_content" );
    }


    $(".single-column-img-conatiner").on('click', open_single_three_panel);

    $(".tc-content-close").on('click', close_single_three_panel);






    // ****************   Header Menu scroll  (Solution Tab)****************

    if(window.location.hash) {
        setTimeout(function() {
            window.scrollTo(0, 0);
            var scoll_from_menu = window.location.hash.substr(1);
            $('html, body').animate({
                scrollTop: $("#"+scoll_from_menu).offset().top
            }, 1500);
        }, 1);
    }





});


