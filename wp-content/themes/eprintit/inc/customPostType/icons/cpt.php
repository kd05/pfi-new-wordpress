<?php

/******************************************************************
 *
 *
 *                A custom post type template.
 *
 *                            o  o
 *                          --------
 *
 * The less time you spend googling the more time we can have fun...
 ******************************************************************/


// cpt > tax > tax term

// let's create the function for the custom type
function gp_icons_post()
{
	// creating (registering) the custom type
	register_post_type('grid-icons', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array('labels'              => array(
			'name'               => __('Grid Icons', 'wpBabywp'), /* This is the Title of the Group */
			'singular_name'      => __('Grid Icons', 'wpBabywp'), /* This is the individual type */
			'all_items'          => __('All Icons', 'wpBabywp'), /* the all items menu item */
			'add_new'            => __('Add New', 'wpBabywp'), /* The add new menu item */
			'add_new_item'       => __('Add New Icon', 'wpBabywp'), /* Add New Display Title */
			'edit'               => __('Edit', 'wpBabywp'), /* Edit Dialog */
			'edit_item'          => __('Edit Icons', 'wpBabywp'), /* Edit Display Title */
			'new_item'           => __('New Icon', 'wpBabywp'), /* New Display Title */
			'view_item'          => __('View Icon', 'wpBabywp'), /* View Display Title */
			'search_items'       => __('Search Icons', 'wpBabywp'), /* Search Custom Type Title */
			'not_found'          => __('Nothing found in the Database.', 'wpBabywp'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('Nothing found in Trash', 'wpBabywp'), /* This displays if there is nothing in the trash */
			'parent_item_colon'  => ''
		), /* end of arrays */
		      'description'         => __('These are all the grid icons', 'wpBabywp'), /* Custom Type Description */
		      'public'              => true,
		      'publicly_queryable'  => true,
		      'exclude_from_search' => true,
		      'show_ui'             => true,
		      'query_var'           => true,
		      'menu_position'       => 13, /* this is what order you want it to appear in on the left hand side menu */
		      'menu_icon'           => 'dashicons-images-alt', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) => dashicons-book */
		      'rewrite'             => array('slug' => 'grid-icon', 'with_front' => false), /* you can specify its url slug */
		      'has_archive'         => false, /* you can rename the slug here */
		      'capability_type'     => 'post',
		      'hierarchical'        => false,
			/* the next one is important, it tells what's enabled in the post editor */
			  'supports'            => array('title', 'editor', 'thumbnail', 'page-attributes')
		) /* end of options */
	); /* end of register post type */

}

// adding the function to the Wordpress init
add_action('init', 'gp_icons_post');
