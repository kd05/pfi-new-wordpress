<?php
/**
 * Register custom taxonomy
 */
function register_grid_icons_tax()
{
	register_taxonomy('grid_icons_cat',
		array('grid-icons'),
		array('hierarchical'      => true,     /* if this is true, it acts like categories */
		      'labels'            => array(
			      'name'              => __('Categories', 'WPBaby'), /* name of the custom taxonomy */
			      'singular_name'     => __('Category', 'WPBaby'), /* single taxonomy name */
			      'search_items'      => __('Search Categories', 'WPBaby'), /* search title for taxomony */
			      'all_items'         => __('All Categories', 'WPBaby'), /* all title for taxonomies */
			      'parent_item'       => __('Parent Category', 'WPBaby'), /* parent title for taxonomy */
			      'parent_item_colon' => __('Parent Category:', 'WPBaby'), /* parent taxonomy title */
			      'edit_item'         => __('Edit Category', 'WPBaby'), /* edit custom taxonomy title */
			      'update_item'       => __('Update Category', 'WPBaby'), /* update title for taxonomy */
			      'add_new_item'      => __('Add New Category', 'WPBaby'), /* add new title for taxonomy */
			      'new_item_name'     => __('New Category Name', 'WPBaby') /* name title for taxonomy */
		      ),
		      'show_admin_column' => true,
		      'show_ui'           => true,
		      'query_var'         => true,
		      'rewrite'           => array('slug' => 'grid-icons-cat'),
		)
	);
}

add_action('init', 'register_grid_icons_tax', 10);