<?php

// Specify what columns you are adding to the table
add_filter( 'manage_testimonials_posts_columns', 'add_testimonial_columns' );

function add_testimonial_columns( $columns ) {

	// param1: id of column,
	// param2: column title,
	// param3: what you would like to place it ahead of,
	// param4: column variable
	$columns = gp_insert_before_array_key( 'id', 'ID', 'categories', $columns );

	return $columns;

}

function testimonials_custom_column_content( $column ) {

	// Get the post object for this row so we can output relevant data
	global $post;

	// Check to see if $column matches our custom column names
	switch ( $column ) {

		case 'id' :
			// Retrieve post meta
			$post_id = $post->ID;

			// Echo output and then include break statement
			echo( ! empty( $post_id ) ? "$post_id" : '' );
			break;
	}
}

 // Let WordPress know to use our action: make CPT Table
 add_action( 'manage_testimonials_posts_custom_column', 'testimonials_custom_column_content' );