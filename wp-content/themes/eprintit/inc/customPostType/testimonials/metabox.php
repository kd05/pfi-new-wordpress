<?php
function wpbaby_add_custom_meta_box()
{
	add_meta_box(
		'user_type',       // $id
		'User Type',                  // $title
		'show_custom_meta_box',  // $callback
		'testimonials',             // $page
		'normal',                  // $context
		'high'                     // $priority
	);
}

add_action('admin_init', function () {
	add_action('add_meta_boxes', 'wpbaby_add_custom_meta_box');
});

//showing custom form fields
function show_custom_meta_box()
{
	global $post;
	$id = $post->ID;
	$user_type_url = get_post_meta($id, 'user_type', true);

	// Use nonce for verification to secure data sending
	wp_nonce_field(basename(__FILE__), 'testimonials_nonce');

	//I KNOW I KNOW STYLES TAGS LOL: TAKE A BREATH AND MOVE ON.
	$html = "
    <style>
        #user_type {
            width:100%;
        }
    </style>
        <input type='text' name='user_type' width='100%' id='user_type' spellcheck='true' value='$user_type_url'>
    ";

	echo $html;
}


function save_testomonial_user_type($post_id, $post, $update)
{
	# code...

	// autosave
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;

	// ajax ( quick edit ? )
	if (defined('DOING_AJAX') && DOING_AJAX)
		return;

	// post type
	if (get_post_type($post) !== 'testimonials')
		return;

	// capabilities (could use edit_posts but i think this is fine)
	if (!current_user_can('administrator'))
		return;

	// post revision (not sure this is necessary)
	if (false !== wp_is_post_revision($post_id))
		return;


	$user_type = gp_if_set($_POST, 'user_type');

	update_post_meta($post->ID, 'user_type', $user_type);

	// exit;
}

add_action('save_post', 'save_testomonial_user_type', 10, 3);