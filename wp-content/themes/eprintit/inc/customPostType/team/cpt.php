<?php

/******************************************************************
 *
 *
 *                A Custom Post type template.
 *
 *                            o  o
 *                          --------
 *
 * The less time you spend googling the more time we can have fun...
 ******************************************************************/


// cpt > tax > tax term

// let's create the function for the custom type
function custom_post_team()
{
	// creating (registering) the custom type
	register_post_type('team', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array('labels'              => array(
			'name'               => __('Team', 'wpBabywp'), /* This is the Title of the Group */
			'singular_name'      => __('Team', 'wpBabywp'), /* This is the individual type */
			'all_items'          => __('All Members', 'wpBabywp'), /* the all items menu item */
			'add_new'            => __('Add New', 'wpBabywp'), /* The add new menu item */
			'add_new_item'       => __('Add New Member', 'wpBabywp'), /* Add New Display Title */
			'edit'               => __('Edit', 'wpBabywp'), /* Edit Dialog */
			'edit_item'          => __('Edit Members', 'wpBabywp'), /* Edit Display Title */
			'new_item'           => __('New Member', 'wpBabywp'), /* New Display Title */
			'view_item'          => __('View Member', 'wpBabywp'), /* View Display Title */
			'search_items'       => __('Search Member', 'wpBabywp'), /* Search Member Title */
			'not_found'          => __('Nothing found in the Database.', 'wpBabywp'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('Nothing found in Trash', 'wpBabywp'), /* This displays if there is nothing in the trash */
			'parent_item_colon'  => ''
		), /* end of arrays */
		      'description'         => __('This is the team Member type', 'wpBabywp'), /* Member Description */
		      'public'              => true,
		      'publicly_queryable'  => true,
		      'exclude_from_search' => true,
		      'show_ui'             => true,
		      'query_var'           => true,
		      'menu_position'       => 11, /* this is what order you want it to appear in on the left hand side menu */
		      'menu_icon'           => 'dashicons-universal-access', /* the icon for the Member type menu. uses built-in dashicons (CSS class name) */
		      'rewrite'             => array('slug' => 'team', 'with_front' => false), /* you can specify its url slug */
		      'has_archive'         => 'team', /* you can rename the slug here */
		      'capability_type'     => 'post',
		      'hierarchical'        => false,
			/* the next one is important, it tells what's enabled in the post editor */
			  'supports'            => array('title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'page-attributes')
		) /* end of options */
	); /* end of register post type */

}

// adding the function to the Wordpress init
add_action('init', 'custom_post_team');
