<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

function LeftVideoRightText_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'min-height' => '625px',
		'bg-color'   => false,
		'bg-img'     => false,
		'video'      => '145',
		'poster'     => '132'
	];
	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = ($props['min-height']) ? $props['min-height'] : '655px';
	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
	$poster_img = $props['poster'];

	$bg_img = ($props['bg-img']) ? $props['bg-img'] : false;

	if ($bg_img) {
		$bg_img = gp_get_file_url($bg_img, 'full');
		$bg_img = "background-image: url( $bg_img );";
	}

	$bg_video = $props['video'];
	if( is_numeric($props['video']) ) {
		$bg_video = gp_get_file_url($props['video']);
	}

	$min_height_style = "min-height: $min_height;";
	$bg_color = "background-color:$bg_color;";
	$poster_img = gp_get_img_url($poster_img);

	if ($bg_video) {
//		$bg_video = "<video controls poster><source src='$bg_video' type='video/mp4'></video>";
		$bg_video = "<a href='$bg_video' title='Play Video' data-fancybox='video'><img src='$poster_img' alt='' /></a>";

	}

	$style = "style='$min_height_style $bg_color $bg_img'";
	$content = apply_filters('the_content', $content);

	$html = "
    <div class='shortcode LeftVideoRightText $text_type' $style>
      <div class='container'>
        <div class='row ai-center jc-between'>
          <div class='col s6 videocol'>
            $bg_video
          </div>
          <div class='col s5 contentcol'>
          	<div class='content general-content'>$content</div>
          </div>
        </div>
      </div>
    </div>";

	return $html;
}

add_shortcode('gp_LeftVideoRightText', 'LeftVideoRightText_shortcode');