<?php

if (!defined('ABSPATH')) {
	exit;
} // Exit if accessed directly

function IconGrid_shortcode($atts, $content = null)
{

	$defaults = [
		'min-height' => '450px',
		'bg-color'   => '#f4f4f4',
		'category'   => false,
	];

	$props = shortcode_atts($defaults, $atts);

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : '';
	$bg_color = "background-color:$bg_color;";
	$cat = $props['category'] ? $props['category'] : false;

	$bg_style = $bg_color;
	$min_height_style = gp_min_height_from_shortcode_props($props);
	$style = "style='$bg_style $min_height_style'";
	$_content = apply_filters('the_content', $content);

	$icons = '';
	$args = array(
		'post_type'      => 'grid-icons',
		'post_status'    => 'published',
		'orderby'        => 'menu_order',
		'order'          => 'DESC',
		'posts_per_page' => -1,
	);

	if ($cat) {
		$args['tax_query'] = [
			[
				'taxonomy' => 'grid_icons_cat',
				'field'    => 'slug',
				'terms'    => $cat,
			],
		];
	}

	$q = new WP_Query($args);
	if ($q->have_posts()) {
		$icons .= "<ul>";
		// Start looping over the query results.
		while ($q->have_posts()) {
			$q->the_post();

			$icon = '';
			$font_awesome_icon = get_field('icon');
			if ($font_awesome_icon) {
				$icon = $font_awesome_icon;
			}

			if (has_post_thumbnail()) {
				$thumbnail = get_the_post_thumbnail_url(get_the_ID(), 'full');
				$icon = "<img src='$thumbnail' alt='' />";
			}

			$link = get_field('link');

			$title = get_the_title();
			$icons .= "<li>";
			if ($link) {
				$icons .= "<a href='$link'>";
			}
			$icons .= "<div class='icon'>$icon</div>";
			$icons .= "<div class='icon-content'>";
			$icons .= "<h3 class='title'>$title</h3>";
			$icons .= get_the_content();
			$icons .= "</div>";
			if ($link) {
				$icons .= "</a>";
			}
			$icons .= "</li>";
		}
		$icons .= "</ul>";
	}

	$hero = "
    <div class='shortcode IconGrid' $style>
      <div class='container'>
        <div class='content general-content'>$_content</div>
        <div class='row'>
        	<div class='col iconscol'>$icons</div>
        </div>
      </div>
    </div>";

	return $hero;
}

add_shortcode('gp_IconGrid', 'IconGrid_shortcode');