<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_PotentialCPT
//   text-type='dark'  
//   min-height= '910px'

//   title= ''

//   bg-image= ''
//   bg-color= ''
// ]
function PotentialCPT_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'min-height' => false,
		'title'      => 'Our Potential',
		'subtitle'   => '',
		'bg-color'   => false,
		'bg-image'   => false,
		'category'   => false,
	];
	$props = shortcode_atts($defaults, $atts);

	$min_height = ($props['min-height']) ? $props['min-height'] : '910px';

	$title = ($props['title']) ? $props['title'] : '';
	$subtitle = ($props['subtitle']) ? $props['subtitle'] : '';

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : 'none';
	$bg_img = ($props['bg-image']) ? $props['bg-image'] : '';
	$cat = $props['category'] ? $props['category'] : false;

	$read_more_text = __('Read More', 'wpBabywp');

	if ($bg_img) {
		$bg_img = gp_get_file_url($bg_img);
		$bg_img = "background-image: url( $bg_img );";
	}

	$min_height_style = "min-height: $min_height;";
	$bg_color = "background-color: $bg_color;";

	$style = "style='$min_height_style $bg_color $bg_img'";

	$args = array(
		'post_type'      => 'case-studies',
		'post_status'    => 'publish',
		'posts_per_page' => -1,
	);
	if ($cat) {
		$args['tax_query'][] = array(
			'taxonomy' => 'case_study_cat',
			'field'    => 'slug',
			'terms'    => $cat,
		);
	}
	$q = new WP_Query($args);

	$slides = '';

	if ($q->have_posts()) {

		// Start looping over the query results.
		while ($q->have_posts()) {

			$q->the_post();

			$potential_id = get_the_ID();
			$potential_url = gp_get_img_url_from_post($potential_id);
			$potential_title = get_the_title();
			$potential_excerpt = get_the_excerpt();

			if (!empty($potential_excerpt)) {
				$excerpt = substr($potential_excerpt, 0, 200);
				if (strlen($potential_excerpt) > 200) {
					$potential_excerpt = $excerpt . '...';
				}
			}
			$potential_date = get_the_date();
			$top_img_styles = "style='background-image: url($potential_url)'";
			$external_link = get_field('external_link');
			$potential_link = !$external_link || empty($external_link) ? get_permalink() : $external_link;
			$slides .= "
                <div class='potential'>
                	<div class='card'>
	                    <div class='overlay'></div>
	                    <div class='cimg' $top_img_styles><a href='$potential_link' class='full-study-link' title='$potential_title'></a></div>
	                    <div class='cbody'>
	                    	<h3 class='title'>$potential_title</h3>
	                    	<p>$potential_excerpt</p>
	                    </div>
	                    <div class='cfooter'><a href='$potential_link' class='text-red'>$read_more_text</a></div>
              		</div>
                </div>";
		}
		wp_reset_postdata();
	}

	$html = "
    <div class='shortcode PotentialCPT flex ai-center' $style>
      <div class='container'>
        <div class='row'>
          <div class='col s12 mb-5'>
            <h3 class='text-center mb-1 scroll-animate'>$title</h3>
            <h2 class='text-center scroll-animate'>$subtitle</h2>
          </div>
          <div class='col s12 p-0'>
            <div class='potentials-slider'>$slides</div>
            <div class='arrow slick-prev left-potential-arrow text-red'>
              <span><i class='fa fa-chevron-left'></i></span>
            </div>
            <div class='arrow slick-next right-potential-arrow text-red'>
            	 <span><i class='fa fa-chevron-right'></i></span>
            </div>
          </div>
        </div>
      </div>
    </div>";
	return $html;
}

add_shortcode('gp_PotentialCPT', 'PotentialCPT_shortcode');