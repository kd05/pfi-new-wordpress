<?php
/**
 * MultiIconPanel Shortcode
 */
if (!defined('ABSPATH')) exit; // Exit if accessed directly

function gp_MultiIconPanel_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'     => 'dark',
		'min-height'    => false,
		'bg-color'      => false,
		'bg-image'      => false,
		'icon1-img'     => 4365,
		'icon1-title'   => '',
		'icon1-content' => '',
		'icon2-img'     => 4369,
		'icon2-title'   => '',
		'icon2-content' => '',
		'icon3-img'     => 4367,
		'icon3-title'   => '',
		'icon3-content' => '',
		'icon4-img'     => '',
		'icon4-title'   => '',
		'icon4-content' => '',
		'icon5-img'     => '',
		'icon5-title'   => '',
		'icon5-content' => '',
		'icon6-img'     => '',
		'icon6-title'   => '',
		'icon6-content' => '',
        'icon7-img'     => '',
        'icon7-title'   => '',
        'icon7-content' => '',
        'icon8-img'     => '',
        'icon8-title'   => '',
        'icon8-content' => '',
        'icon9-img'     => '',
        'icon9-title'   => '',
        'icon9-content' => '',
	];

	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = $props['min-height'] ? $props['min-height'] : '';

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
	$bg_img = ($props['bg-image']) ? $props['bg-image'] : false;

	if ($bg_color) {
		$bg_color = "background-color: $bg_color;";
	}

	if ($bg_img) {
		$bg_img = gp_get_file_url($bg_img);
		$bg_img = "background-image: url($bg_img);";

	}

	// Check for icons
	$icons_class = '';
	$icons = '<div class="icon-wrapper">';
	// Max 9 items
	for ($i = 1; $i <= 9; $i++) {
	    if(!isset($props['icon'.$i.'-img']) || !isset($props['icon'.$i.'-title']) || !isset($props['icon'.$i.'-content'])) {
	        continue;
        }
//	    print_r($i);
//	    print_r($props['icon'.$i.'-img']);
//	    var_dump($props['icon'.$i.'-title']);
//	    var_dump($props['icon'.$i.'-content']);
		if(!empty($props['icon'.$i.'-img']) && !empty($props['icon'.$i.'-title']) && !empty($props['icon'.$i.'-content'])) {
			$img_src = gp_get_file_url($props['icon'.$i.'-img']);
			$icons .= '<div class="unit">';
			$icons .= '<img src="'. $img_src .'" alt="Icon" />';
			$icons .= '<h3 class="title">'. $props['icon'.$i.'-title'] .'</h3>';
			$icons .= '<p>'. $props['icon'.$i.'-content'] .'</p>';
			$icons .= '</div>';
			if($i > 3) {
				$icons_class = ' wide';
			}
		}
	}
	$icons .= '</div>';

	$bg_style = (!$bg_img) ? $bg_color : $bg_img;
	$min_height_style = !empty($min_height) ? "min-height:$min_height;" : '';
	$style = "style='$bg_style $min_height_style'";

	$content = apply_filters('the_content', $content);
	$hero = "
    <div class='shortcode MultiIconPanel flex ai-center $text_type$icons_class' $style>
      <div class='container'>
        <div class='row'>
          <div class='col s12'>
              <div class='content general-content'>$content$icons</div>
          </div>
        </div>
      </div>
    </div>";

	return $hero;
}

add_shortcode('gp_MultiIconPanel', 'gp_MultiIconPanel_shortcode');