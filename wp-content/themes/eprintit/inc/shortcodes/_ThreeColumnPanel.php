<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly


function gp_three_column_start_shortcode($atts, $content = null)
{
    $defaults = [
        'id'  => '',

        'navigate_next_panel_id' => '',
        'scroll_name' => ""
    ];

    $props = shortcode_atts($defaults, $atts);
    $id = !empty($props['id']) ? $props['id'] : "";

    $navigate_next_panel_id = !empty($props['navigate_next_panel_id']) ? $props['navigate_next_panel_id'] : "";
    $navigate_next_panel_text = !empty($navigate_next_panel_id) ?
                '<div class="next-panel-animate">
                    <a href="javascript:void(0)"  data-animate="'.$navigate_next_panel_id.'"><i class="fa fa-chevron-down"></i></a>
                </div>' : "";
    $scroll_name = !empty($props['scroll_name']) ? $props['scroll_name'] : "";

    $html = "<div class='shortcode pfi-shortcode ThreeColumnPanel'  id='$id' > 
                $navigate_next_panel_text
            ";
    return $html;
}
add_shortcode('gp_ThreeColumnStart', 'gp_three_column_start_shortcode');



function gp_three_column_end_shortcode($atts, $content = null)
{
    $html = "</div> ";
    return $html;
}
add_shortcode('gp_ThreeColumnEnd', 'gp_three_column_end_shortcode');





function gp_single_column_three_panel_shortcode($atts, $content = null)
{
    $defaults = [
        'bg-img' => '',
        'bg-color' => false,
        'bg-video'    => false,
        'text'    => false,

        'title'      => '',
        'youtube_id' => "",
        'youtube_text' => "SEE OUR END TO END SOLUTION"
    ];
    $props = shortcode_atts($defaults, $atts);


    $title = !empty($props['title']) ? $props['title'] : "";
    $text = !empty($props['text']) ? $props['text'] : "LEARN MORE";

    $bg_img = ($props['bg-img']) ? $props['bg-img'] : false;
    $bg_color = ($props['bg-color']) ? $props['bg-color'] : false;

    if ($bg_color) {
        $bg_color = "background-color: $bg_color;";
    }
    if ($bg_img) {
        $bg_img = gp_get_file_url($bg_img);
        $bg_img = "background-image: url($bg_img);";
    }
    $bg_style = (!$bg_img) ? $bg_color : $bg_img;


    $bg_video = ($props['bg-video']) ? $props['bg-video'] : false;
    $bg_video = gp_get_file_url($bg_video);
    $video_url = !empty($props['video']) ? $props['video'] : $bg_video;
    if ($bg_video) {
        $bg_video = "
                    <video autoplay loop poster muted class='bg-video'>
                       <source src='$bg_video' type='video/mp4'>
                     </video>
                    ";
    }




    $youtube_id = $props['youtube_id'];
    $youtube_text = $props['youtube_text'];

    $youtube_text = !empty($youtube_id) ?
        "<div class='three-panel-video'>
             <a href='javascript:void(0)' class='popup-video'  data-video-id='$youtube_id'>$youtube_text</a>
        </div>" : "";


    $style = "style='$bg_style '";
    $content = apply_filters('the_content', $content);

    $html = "
        <div class='single_column_three_panel ' >
            <div class='single-column-content '>
                <div class='tc-inner-content'>
                    <div class='tc-info'>
                        $content
                        $youtube_text
                        
                    </div>
                </div>
                <div class='tc-content-close'><i class='fa fal fa-times'></i></div>                
            </div>
            <div class='single-column-img-conatiner ' $style>
                $bg_video
                <div class='grey-overlay'></div>
                <a href='javascript:void(0)' class='main-title'><h1>$title</h1></a>
                <div class='learn-more'>
                    <a href='javascript:void(0)'>$text</a> 
                </div>          
            </div>
        </div>
    ";
    return $html;
}
add_shortcode('gp_SingleColumnThreePanel', 'gp_single_column_three_panel_shortcode');