<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

function gp_left_content_on_image_shortcode($atts, $content = null)
{
    $defaults = [

        'id' => '',
        'title'    => 'Digital Transformation Starts With Viewing Solutions Differently',

        'bg-color'=>"",
        'bg-img'=>"",
        'min-height'=>"",
        'text-type'=>"",

    ];
    $props = shortcode_atts($defaults, $atts);

    $text_type = trim($props['text-type']) == "" ? "light" : "dark";

    $id = !empty($props['id']) ? $props['id'] : "";
    $title = !empty($props['title']) ? $props['title'] : "";
    $content = apply_filters('the_content', $content);


    $bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
    $bg_img = ($props['bg-img']) ? $props['bg-img'] : false;
    if ($bg_color) {
        $bg_color = "background-color: $bg_color;";
    }
    if ($bg_img) {
        $bg_img = gp_get_file_url($bg_img);
        $bg_img = "background-image: url($bg_img);";
    }
    $bg_style = (!$bg_img) ? $bg_color : $bg_img;
    $min_height = $props['min-height'];
    $min_height_style = "min-height:$min_height;";

    $style = "style='$bg_style $min_height_style'";

    $html = "
        <div class='shortcode pfi-shortcode LeftContentOnImage $text_type' id='$id' > 
                <div class='lcoi-img-bg' $style>
                <div class='grey-overlay'></div>
                <div class='lcoi-inner'>                
                    <div class='ab-left'>
                        <div class='lcoi-title'>
                            <h1>$title</h1>
                        </div>
                        <div class='lcoi-content'>
                            <h1>$content</h1>
                        </div>                        
                    </div>                     
                </div>   
                </div>
                  
        </div>            
    ";
    return $html;

}
add_shortcode('gp_LeftContentOnImage', 'gp_left_content_on_image_shortcode');