<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_RightTextOverVidOrImageWithButton
//   text-type= 'dark'
//   min-height= ''
//   title= ''
//   description= ''
//   btn-text=''
//   btn-url=''
//   bg-color= ''
//   bg-img= ''
//   bg-video= ''
// ]
function RightTextOverVidOrImageWithButton_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'min-height' => '655px',

		'title'       => false,
		'description' => false,

		'btn-text' => false,
		'btn-url'  => false,

		'bg-color' => false,
		'bg-img'   => false,
		'bg-video' => false,
	];
	$props = shortcode_atts($defaults, $atts);

	$min_height = ($props['min-height']) ? $props['min-height'] : '655px';
	$title = ($props['title']) ? $props['title'] : '';
	$description = ($props['description']) ? $props['description'] : '';
	$btn_text = ($props['btn-text']) ? $props['btn-text'] : false;
	$btn_url = ($props['btn-url']) ? $props['btn-url'] : false;
	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
	$bg_img = ($props['bg-img']) ? $props['bg-img'] : false;
	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';

	$btn = '';

	if ($btn_text && $btn_url) {
		// code...
		$btn = "<a href='$btn_url' class='btn bg-white text-grey'>$btn_text</a>";
	}


	$bg_img = gp_get_img_url($bg_img, 'full');

	$bg_video = ($props['bg-video']) ? $props['bg-video'] : false;
	$bg_video = gp_get_file_url($bg_video);

	$min_height_style = "min-height: $min_height;";
	$bg_color = "background-color:$bg_color;";
	$bg_img = $bg_img ? "background-image:url($bg_img);" : "";

	if ($bg_video) {

		$bg_video = "
    <video autoplay loop poster muted class='bg-video'>
       <source src='$bg_video' type='video/mp4'>
     </video>";
	}

	$style = "style='$min_height_style $bg_color $bg_img'";

	$html = "
    <div class='shortcode RightTextOverVidOrImageWithButton $text_type' $style>
      $bg_video
      <div class='container'>

        <div class='row jc-end'>

          <div class='col s6 general-content scroll-animate'>
            <h2 class='bold'>$title</h2>
            <p>$description</p>
            <div class='btn-container'>
              $btn
            </div>
          </div>

        </div>
      </div>
    </div>

  ";


	return $html;
}

add_shortcode('gp_RightTextOverVidOrImageWithButton', 'RightTextOverVidOrImageWithButton_shortcode');