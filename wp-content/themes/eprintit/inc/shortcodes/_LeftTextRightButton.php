<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_LeftTextRightButton
// text-type= 'light'
// min-height=''

// bg-color=''
// bg-img='23'

// title='Take ePRINTit for a spin'
// caption='Try it today and see how easy secure cloud printing can be!'

// left-button-text='Live Demo'
// right-button-text='Video Demo'
// left-button-url=''
// right-button-url=''

// ]
function LeftTextRightButton_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'min-height' => '342px',

		'bg-color' => false,
		'bg-img'   => false,

		'title'   => false,
		'caption' => false,

		'left-button-text'  => false,
		'right-button-text' => false,
		'left-button-url'   => false,
		'right-button-url'  => false,

	];

	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = ($props['min-height']) ? $props['min-height'] : '342px';

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
	$bg_img = ($props['bg-img']) ? $props['bg-img'] : false;

	$title = ($props['title']) ? $props['title'] : false;
	$caption = ($props['caption']) ? $props['caption'] : false;

	$left_button_text = ($props['left-button-text']) ? $props['left-button-text'] : false;
	$right_button_text = ($props['right-button-text']) ? $props['right-button-text'] : false;
	$left_button_url = ($props['left-button-url']) ? $props['left-button-url'] : false;
	$right_button_url = ($props['right-button-url']) ? $props['right-button-url'] : false;

	if ($bg_color) {
		$bg_color = "background-color: $bg_color;";
	}

	if ($bg_img) {
		$bg_img = gp_get_file_url($bg_img);
		$bg_img = "background-image: url($bg_img);";
	}
	$min_height_style = "min-height:$min_height;";

	$style = "style='$bg_color $bg_img $min_height_style'";

	$html = "
    <div class='shortcode LeftTextRightButton flex ai-center bg-light-grey $text_type' $style>
      <div class='container'>
        <div class='row'>
          <div class='col s8'>
            <h2>$title</h2>
            <h3>$caption</h3>
          </div>
          <div class='col s4 jc-end'>
            <a href='$left_button_url' class='btn text-blue bg-white'>$left_button_text</a>
            <a href='$right_button_url' class='btn text-blue bg-white'>$right_button_text</a>
          </div>
        </div>
      </div>
    </div>";

	return $html;
}

add_shortcode('gp_LeftTextRightButton', 'LeftTextRightButton_shortcode');