<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

function gp_testimonial_pfi_shortcode($atts, $content = null)
{
    $defaults = [
        'id'  => '',

        'image'   => '',
        'bg-color'=>"",
        'bg-img'=>"",
        'text-type'=>"",
    ];
    $props = shortcode_atts($defaults, $atts);

    $id = !empty($props['id']) ? $props['id'] : "";
    $text_type = trim($props['text-type']) == "" ? "dark" : "light";

    $bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
    $bg_img = ($props['bg-img']) ? $props['bg-img'] : false;
    if ($bg_color) {
        $bg_color = "background-color: $bg_color;";
    }
    if ($bg_img) {
        $bg_img = gp_get_file_url($bg_img);
        $bg_img = "background-image: url($bg_img);";
    }
    $bg_style = (!$bg_img) ? $bg_color : $bg_img;

    $style = "style='$bg_style '";

    $image = ($props['image']) ? $props['image'] : false;
    if ($image) {
        $image = gp_get_file_url($image);
        $image = "<img src='$image' alt=''>";
    }

    $content = apply_filters('the_content', $content);

    $html = "
        <div class='shortcode pfi-shortcode TestimonialPfi $text_type' $style  id='$id' > 
                <div class='tpfi-inner'>                                        
                    <div class='tpfi-image'>
                        $image
                    </div> 
                     <div class='tpfi-content'>
                        $content
                    </div>                              
                </div>                
        </div>            
    ";
    return $html;

}
add_shortcode('gp_TestimonialPfi', 'gp_testimonial_pfi_shortcode');


