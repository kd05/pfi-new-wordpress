<?php

if (!defined('ABSPATH')) {
	exit;
} // Exit if accessed directly

// [gp_LetsPrintNow
// bg-color='#000'
// bg-img='23'
// ]
function LetsPrintNow_shortcode($atts, $content = null)
{

	$defaults = [
		'min-height' => '774px',
		'bg-color'   => false,
		'title-1'    => 'Let&#39s Print Now',
		'title-2'    => 'Anything, Anywhere, Anytime',
		'bg-img'     => 584,
		'icon-1'     => 616,
		'icon-2'     => 615,
		'img-phone'  => 584,
		'url-1'      => '',
		'url-2'      => '',
		'target-1'   => '_blank',
		'target-2'   => '_blank',
	];

	$props = shortcode_atts($defaults, $atts);

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : '';
	$bg_color = "background-color:$bg_color;";

	$bg_img = isset($props['bg-img']) ? $props['bg-img'] : '';
	$bg_img = gp_get_img_url($bg_img);
	$bg_img = "background-image:url($bg_img);";

	$img_phone = gp_img_url_from_shortcode_att($props['img-phone']);

	$url_1 = gp_href_from_shortcode_att($props['url-1']);
	$target_1 = $props['target-1'];
	$icon_url_1 = gp_img_url_from_shortcode_att($props['icon-1'], 'medium');

	$url_2 = gp_href_from_shortcode_att($props['url-2']);
	$target_2 = $props['target-2'];
	$icon_url_2 = gp_img_url_from_shortcode_att($props['icon-2'], 'medium');
	$bg_style = (!$props['bg-img']) ? $bg_color : $bg_img;

	$min_height_style = gp_min_height_from_shortcode_props($props);

	$style = "style='$bg_style $min_height_style'";

	$title_1 = $props['title-1'];
	$title_2 = $props['title-2'];

	$_content = apply_filters('the_content', $content);

	$hero = "
    <div class='shortcode LetsPrintNow' $style>
      <div class='container'>
        <div class='row'>
          <div class='col contentcol'>
            <div class='content general-content gc-white'>
              <h1 class=''>$title_1</h1>
              <h2 class=''>$title_2</h2>
              $_content
              <div class='btn-container'>
                <a href='$url_1' target='$target_1'><img src='$icon_url_1' /></a>
                <a href='$url_2' target='$target_2'><img src='$icon_url_2' /></a>
              </div>
            </div>
          </div>
          <div class='col imagecol'>
            <img src='$img_phone' />
          </div>
        </div>
      </div>
    </div>";

	return $hero;
}

add_shortcode('gp_LetsPrintNow', 'LetsPrintNow_shortcode');