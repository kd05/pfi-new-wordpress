<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

function gp_left_content_right_image_shortcode($atts, $content = null)
{
    $defaults = [
        'id'  => '',

        'title' => '',
        'image' => '',
        'image_align' => 'left'
    ];
    $props = shortcode_atts($defaults, $atts);

    $id = !empty($props['id']) ? $props['id'] : "";
    $title = !empty($props['title']) ? $props['title'] : "";

    $image =  ($props['image']) ? $props['image'] : false;
    if ($image) {
        $image = gp_get_file_url($image);
        $image = "  <img src='$image' alt=''> ";
    }

    $align = !empty($props['image_align']) ? $props['image_align'] : "right";
    $content = apply_filters('the_content', $content);

    $html = "
        <div class='shortcode pfi-shortcode LeftContentRightImage $align'  id='$id' > 
                <div class='image-container' >
                    $image
                </div>
                <div class='info-container' >
                    <div class='lcri-title'>
                        <h1>$title</h1>
                    </div>
                    $content
                </div>
        </div>            
    ";
    return $html;

}
add_shortcode('gp_LeftContentRightImage', 'gp_left_content_right_image_shortcode');