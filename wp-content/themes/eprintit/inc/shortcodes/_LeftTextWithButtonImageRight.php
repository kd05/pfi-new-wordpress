<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_LeftTextWithButtonImageRight
// text-type= 'dark'
// min-height=''

// bg-color=''
// bg-image=''

// title=''
// image=''

// button_label=''
// Button1_Link=''
// ]
function LeftTextWithButtonImageRight_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'min-height' => false,

		'bg-color' => false,
		'bg-image' => false,

		'image' => false,

		'button_label' => false,
		'Button_Link'  => false,
	];

	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = ($props['min-height']) ? $props['min-height'] : '825px';

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
	$bg_img = ($props['bg-image']) ? $props['bg-image'] : false;

	$img = ($props['image']) ? $props['image'] : false;

	$button_label = ($props['button_label']) ? $props['button_label'] : '';
	$Button_Link = ($props['Button_Link']) ? $props['Button_Link'] : '';


	if ($bg_color) {
		# code...
		$bg_color = "background-color: $bg_color;";
	}

	if ($bg_img) {
		# code...
		$bg_img = gp_get_file_url($bg_img);
		$bg_img = "background-image: url($bg_img);";
	}

	$img = gp_get_file_url($img);

	$bg_style = (!$bg_img) ? $bg_color : $bg_img;
	$min_height_style = "min-height:$min_height;";

	$style = "style='$bg_style $min_height_style'";
	$content = do_shortcode($content);

	$hero = "
    <div class='shortcode LeftTextWithButtonImageRight flex ai-center $text_type' $style>

      <div class='container'>

        <div class='row'>

          <div class='col'>

            <div class='content'>
              $content
              <div class='btn-container'>
                <a href='$Button_Link' class='btn text-white bg-blue'>$button_label</a>
              </div>

            </div>
          </div>

          <div class='col'>
            <img src='$img' />
          </div>
          
        </div>
      </div>
    </div>
  ";

	return $hero;
}

add_shortcode('gp_LeftTextWithButtonImageRight', 'LeftTextWithButtonImageRight_shortcode');

