<?php

function gp_accodian_shortcode( $atts, $content = null ){
	$parameters = shortcode_atts( array(
		'title' => '',
		'open' => 'false',
	), $atts );
	$class = '';
	if($parameters['open']=='true'){
		$class = ' open';
	}
	ob_start();?>

	<div class="gp-accordian-container<?php echo $class; ?>">
		<div class="accordian-title">
			<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-minus-circle" aria-hidden="true"></i><p><?php echo $parameters['title']; ?></p>
		</div>
		<div class="accordian-content">
			<?php echo apply_filters('the_content', $content);?>
		</div>
	</div>

	<?php $output = ob_get_clean();

	return $output;

}
add_shortcode('gp_accordian', 'gp_accodian_shortcode');