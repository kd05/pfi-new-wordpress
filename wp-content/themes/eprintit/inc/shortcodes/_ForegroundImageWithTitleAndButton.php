<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_ForegroundImageWithTitleAndButton
//   text-type= ''
//   min-height= ''
//   bg-color= ''
//   title= ''
//   text= ''
//   button_link= ''
//   button_label= ''
// ]
function ForegroundImageWithTitleAndButton_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'min-height' => false,
		'image'      => '2519',

		'bg-color' => false,

		'title' => false,
		'text'  => false,

		'button_link'  => false,
		'button_label' => false,
	];

	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = ($props['min-height']) ? $props['min-height'] : '438px';

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : 'white';
	$img = ($props['image']) ? $props['image'] : false;

	$title = ($props['title']) ? ($props['title']) : false;
	$text = ($props['text']) ? ($props['text']) : false;

	$button_link = ($props['button_link']) ? ($props['button_link']) : false;
	$button_label = ($props['button_label']) ? ($props['button_label']) : false;

	if ($img) {
		$img = gp_get_file_url($img);
	}

	$btn = "";

	if ($button_label) {
		# code...
		$btn = "<a href='$button_link' class='btn bg-blue text-white'>$button_label</a>";
	}

	$min_height_style = "min-height: $min_height;";
	$bg_color = "background-color:$bg_color;";

	$text_html = !empty($text) ? "<h3 class='subtitle text-center mt-1 mb-1 scroll-animate'>$text</h3>" : "";
	$style = "style='$min_height_style $bg_color'";
	$html = "
    <div class='shortcode v-padding ForegroundImageWithTitleAndButton flex ai-center $text_type' $style>
      <div class='container'>
        <div class='row jc-center'>
          <div class='col s12'>
            <h2 class='text-center scroll-animate'>$title</h2>
            $text_html
          </div>
          <div class='col s12'>
            <div class='img-container mb-2 scroll-animate'><img src='$img'></div>
            <div class='btn-container text-center scroll-animate'>
              $btn
            </div>
          </div>
        </div>
      </div>
    </div>";
	return $html;
}

add_shortcode('gp_ForegroundImageWithTitleAndButton', 'ForegroundImageWithTitleAndButton_shortcode');