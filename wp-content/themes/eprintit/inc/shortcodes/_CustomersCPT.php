<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_CustomersCPT
// text-type= 'dark'
// min-height=''
// bg-color=''
// ]
function CustomersCPT_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'min-height' => false,

		'bg-color' => false,
		'bg-image' => false,
	];

	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = ($props['min-height']) ? $props['min-height'] : '220px';

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
	$bg_img = ($props['bg-image']) ? $props['bg-image'] : false;

	$arrow_left = "";
	$arrow_right = "";

	if ($bg_color) {
		# code...
		$bg_color = "background-color: $bg_color;";
	}
	if ($bg_img) {
		# code...
		$bg_img = gp_get_file_url($bg_img);
		$bg_img = "background-image: url( $bg_img );";
	}

	$args = array(
		'post_type'      => 'customers',
		'post_status'    => 'publish',
		'posts_per_page' => -1,
	);

	$q = new WP_Query($args);

	$slides = '';
	if ($q->have_posts()) {

		// Start looping over the query results.
		while ($q->have_posts()) {

			$q->the_post();

			$potential_id = get_the_ID();
			$title = get_the_title();
			$potential_url = gp_get_img_url_from_post($potential_id);
			$url = get_field('link');
			if (!$url || empty($url)) {
				$url = '#';
			}
			$slides .= "<div class='logo'><a href='$url' target='_blank' title='$title'><img src='$potential_url' alt='$title' /></a></div>";

		}
		wp_reset_postdata();
	}

	if ($q->post_count > 5) {
		$arrow_left = "
      <div class='arrow left-arrow text-red'>
        <span><i class='fa fa-chevron-left'></i></span>
      </div>
    ";
		$arrow_right = "
      <div class='arrow right-arrow text-red'>
        <span><i class='fa fa-chevron-right'></i></span>
      </div>
    ";
	}

	$min_height_style = "min-height:$min_height;";

	$style = "style='$bg_color $bg_img $min_height_style'";
	$title = __('Our Customers', 'wpBabywp');

	$html = "
    <div class='shortcode CustomersCPT flex ai-center $text_type' $style>
      <div class='lg-container'>
        <div class='row'>
          <div class='col s12 text-center'>
            <h2 class='title mb-1 scroll-animate'>$title</h2>
          </div>
          <div class='col s12 slider-container'>
            $arrow_left
            <div class='customers-slider'>
              $slides
            </div>
            $arrow_right
          </div>
        </div>
      </div>
    </div>";
	return $html;
}

add_shortcode('gp_CustomersCPT', 'CustomersCPT_shortcode');