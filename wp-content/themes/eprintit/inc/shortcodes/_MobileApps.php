<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_MobileApps
// min-height=''
// bg-color=''
// bg-image=''
// logo=''
// title=''
// title1=''
// Button1_Image=''
// Button1_Link=''
// title2=''
// Button2_Image=''
// Button2_Link=''
// ]
function MobileApps_shortcode($atts, $content = null)
{
    $defaults = [
        'min-height' => false,
        'text-type' => 'light',

        'bg-color' => false,
        'bg-image' => false,

        'logo' => false,
        'title' => false,

        'title1' => false,
        'button1_image' => 616,
        'button1_link' => false,

        'title2' => false,
        'button2_image' => 615,
        'button2_link' => false,
        'image' => false
    ];

    $props = shortcode_atts($defaults, $atts);

    $min_height = ($props['min-height']) ? $props['min-height'] : '825px';
    $text_type = $props['text-type'];

    $bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
    $bg_img = ($props['bg-image']) ? $props['bg-image'] : false;

    $logo = ($props['logo']) ? $props['logo'] : false;
    $title = ($props['title']) ? $props['title'] : '';

    $google_play = ($props['button1_image']) ? $props['button1_image'] : 616;
    $google_play_url = ($props['button1_link']) ? $props['button1_link'] : 'google_play_url';
    if (isset($props['Button1_Link']) && !empty($props['Button1_Link'])) {
        $google_play_url = $props['Button1_Link'];
    }

    $app_store = ($props['button2_image']) ? $props['button2_image'] : 615;
    $app_store_url = ($props['button2_link']) ? $props['button2_link'] : 'app_store_url';
    if (isset($props['Button2_Link']) && !empty($props['Button2_Link'])) {
        $app_store_url = $props['Button2_Link'];
    }
    $image = $props['image'] ? $props['image'] : 584;

    $content = apply_filters('the_content', $content);

    if ($bg_color) {
        # code...
        $bg_color = "background-color: $bg_color;";
    }

    if ($bg_img) {
        # code...
        $bg_img = gp_get_file_url($bg_img);
        $bg_img = "background-image: url($bg_img);";

    }

    if ($logo) {
        # code...
        $logo = gp_get_file_url($logo);
        $logo_styles = "background-image: url($logo);";
    }

    $logo_html = $logo ? "<div class='logo' style='$logo_styles'></div>" : "";

    $img = gp_get_img_url($image);
    $google_play = gp_get_img_url($google_play);
    if(!empty($google_play)) {
    	$google_play = "<a href='$google_play_url' target='_blank'><img src='$google_play' /></a>";
    }
    $app_store = gp_get_img_url($app_store);
    if(!empty($app_store)) {
    	$app_store = "<a href='$app_store_url' target='_blank'><img src='$app_store' /></a>";
    }
    $bg_style = (!$bg_img) ? $bg_color : $bg_img;
    $min_height_style = gp_min_height_from_shortcode_props($props);
    $style = "style='$bg_style $min_height_style'";

	$btnContainerClass = '';
	if(empty($app_store) || empty($google_play)) {
		$btnContainerClass = 'single';
	}

    $hero = "
    <div class='shortcode MobileApps $text_type' $style>
    	<div class='imagecol' style='background-image: url($img);'></div>
      <div class='lg-container'>
        <div class='row'>
          <div class='col contentcol'>
            <div class='content scroll-animate'>
              $logo_html
              <h2 class='title'>$title</h2>
              $content
              <div class='btn-container $btnContainerClass'>
              	$google_play
                $app_store
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  ";
    return $hero;
}

add_shortcode('gp_MobileApps', 'MobileApps_shortcode');
