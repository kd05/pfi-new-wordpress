<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_BusinessPartners
// text-type= 'dark'
// min-height=''
// bg-color=''
// ]
function BusinessPartners_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'min-height' => false,
		'bg-color'   => false,
	];

	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = ($props['min-height']) ? $props['min-height'] : '220px';

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;

	if ($bg_color) {
		# code...
		$bg_color = "background-color: $bg_color;";
	}

	$args = array(
		'post_type'      => 'partners',
		'post_status'    => 'publish',
		'posts_per_page' => -1,
	);
	$q = new WP_Query($args);

	$slides = '';

	if ($q->have_posts()) {

		// Start looping over the query results.
		while ($q->have_posts()) {

			$q->the_post();

			$potential_id = get_the_ID();
			$title = get_the_title();
			$potential_url = gp_get_img_url_from_post($potential_id);
			$url = get_field('partner_link');
			if (!$url || empty($url)) {
				$url = '#';
			}
			$slides .= "<div class='logo'><a href='$url' target='_blank' title='$title'><img src='$potential_url' alt='$title' /></a></div>";
		}
		wp_reset_postdata();
	}

	$min_height_style = "min-height:$min_height;";

	$style = "style='$bg_color $min_height_style'";

	$html = "
    <div class='shortcode BusinessPartners flex ai-center $text_type' $style>
      <div class='container'>
        <div class='row'>
          <div class='col'>
            <div class='logo-slider'>$slides</div>
          </div>
        </div>
      </div>
    </div>";

	return $html;
}

add_shortcode('gp_BusinessPartners', 'BusinessPartners_shortcode');
