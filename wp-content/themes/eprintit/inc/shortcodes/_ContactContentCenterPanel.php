<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

function gp_contact_content_center_panel_shortcode($atts, $content = null)
{
    $defaults = [
        'id'  => '',

        'title'      => 'Be Part of the team',
        'link_text'  => '',
        'link_url'  => '',

        'bg-color'=>"",
        'bg-img'      => '',
        'text-type'  =>  "",

    ];
    $props = shortcode_atts($defaults, $atts);

    $id = !empty($props['id']) ? $props['id'] : "";
    $text_type = trim($props['text-type']) == "" ? "dark" : "light";

    $title = !empty($props['title']) ? $props['title'] : "";
    $link_url = !empty($props['link_url']) ? $props['link_url'] : "";

    $link_text = !empty($props['link_text']) ?
        '<div class="cccp-git">
              <a href="'.$link_url.'">'.$props['link_text'].'</a>
         </div>' : "";

    $content = apply_filters('the_content', $content);

    $bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
    $bg_img = ($props['bg-img']) ? $props['bg-img'] : false;
    if ($bg_color) {
        $bg_color = "background-color: $bg_color;";
    }
    if ($bg_img) {
        $bg_img = gp_get_file_url($bg_img);
        $bg_img = "background-image: url($bg_img);";
    }
    $bg_style = (!$bg_img) ? $bg_color : $bg_img;
    $style = "style='$bg_style '";

    $html = "
        <div class='shortcode pfi-shortcode ContactContentCenterPanel $text_type' $style  id='$id' > 
                <div class='cccp-inner'>
                        <div class='cccp-title'>
                            <h1>$title</h1>
                        </div>
                        <div class='cccp-content'>
                            $content
                        </div>     
                        $link_text                
                </div>     
        </div>            
    ";
    return $html;

}
add_shortcode('gp_ContactContentCenterPanel', 'gp_contact_content_center_panel_shortcode');


