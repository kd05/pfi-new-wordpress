<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly


function gp_CenteredPanel($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'text-align' => 'center',
		'min-height' => '200px',

		'bg-color' => false,
		'bg-image' => false,

		'button_label' => false,
		'button_link'  => false,
	];

	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = $props['min-height'];

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
	$bg_img = ($props['bg-image']) ? $props['bg-image'] : false;

	$button_label = ($props['button_label']) ? $props['button_label'] : false;
	$Button_Link = ($props['button_link']) ? $props['button_link'] : false;
	$btn = "";

	if ($bg_color) {
		$bg_color = "background-color: $bg_color;";
	}

	if ($bg_img) {
		$bg_img = gp_get_file_url($bg_img);
		$bg_img = "background-image: url($bg_img);";
	}

	if ($button_label || $Button_Link) {
		$btn = "<a href='$Button_Link' class='btn text-white bg-blue'>$button_label</a>";
	}

	$bg_style = (!$bg_img) ? $bg_color : $bg_img;
	$min_height_style = "min-height:$min_height;";

	$style = "style='$bg_style $min_height_style'";
	$content = apply_filters('the_content', $content);
	$text_align = 'text-' . $props['text-align'];

	$hero = "
    <div class='shortcode CenteredPanel flex scroll-animate ai-center $text_type' $style>
      <div class='container'>
        <div class='row'>
          <div class='col s12'>
            <div class='content general-content scroll-animate $text_align'>
              $content
              <div class='btn-container scroll-animate'>$btn</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  	";
	return $hero;
}

add_shortcode('gp_CenteredPanel', 'gp_CenteredPanel');