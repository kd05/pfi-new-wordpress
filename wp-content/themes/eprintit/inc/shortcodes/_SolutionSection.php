<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

function gp_solution_section_shortcode($atts, $content = null)
{
    $defaults = [
        'id'  => '',

        'bg-color' => false,
        'bg-img' => '',
        'min-height' => '',
        'text-type' => 'light',
        'logo-image' => '',
        'bg-video'    => false,

        'title'      => '',
        'link_text'  => '',
        'link_url'  => '',
        'navigate_next_panel_text' => '',
        'navigate_next_panel_id' => '',
        'scroll_name' => ""
    ];

    $props = shortcode_atts($defaults, $atts);
    $text_type = trim($props['text-type']) == "" ? "light" : "dark";



    $id = !empty($props['id']) ? $props['id'] : "";
    $title = !empty($props['title']) ? $props['title'] : "";
    $link_url = !empty($props['link_url']) ? $props['link_url'] : "";

    $link_text = !empty($props['link_text']) ?
        '<div class="learn-more  scroll-animate ">
              <a href="'.$link_url.'">'.$props['link_text'].'</a>
         </div>' : "";

    $navigate_next_panel_id = !empty($props['navigate_next_panel_id']) ? $props['navigate_next_panel_id'] : "";
    $navigate_next_panel_text = !empty($props['navigate_next_panel_text']) ?
        '<div class="next-panel-animate">
                    <a href="javascript:void(0)"  data-animate="'.$navigate_next_panel_id.'">'.$props['navigate_next_panel_text'].'</a>
                </div>' : "";
    $scroll_name = !empty($props['scroll_name']) ? $props['scroll_name'] : "";
    $content = apply_filters('the_content', $content);


    $bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
    $bg_img = ($props['bg-img']) ? $props['bg-img'] : false;
    $logo_image =  ($props['logo-image']) ? $props['logo-image'] : false;
    if ($logo_image) {
        $logo_image = gp_get_file_url($logo_image);
        $logo_image = "  <img src='$logo_image' alt=''> ";
    }

    if ($bg_color) {
        $bg_color = "background-color: $bg_color;";
    }

    if ($bg_img) {
        $bg_img = gp_get_file_url($bg_img);
        $bg_img = "background-image: url($bg_img);";
    }

    $bg_style = (!$bg_img) ? $bg_color : $bg_img;



    $bg_video = ($props['bg-video']) ? $props['bg-video'] : false;
    $bg_video = gp_get_file_url($bg_video);
    $video_url = !empty($props['video']) ? $props['video'] : $bg_video;
    if ($bg_video) {
        $bg_video = "
                    <video autoplay loop poster muted class='bg-video'>
                       <source src='$bg_video' type='video/mp4'>
                     </video>
                    ";
    }



    $min_height = $props['min-height'];
    $min_height_style = "min-height:$min_height;";

    $style = "style='$bg_style $min_height_style'";



    $html = "
        <div class='shortcode pfi-shortcode SolutionSection $text_type' id='$id' scroll-name='$scroll_name'  $style>
                $bg_video
             <div class='grey-overlay'></div>
            <div class='container-1220 flex-center pfi-shortcode-inner'>
            
                <div class='solution-section-inner'>
                    <div class='solution-top  scroll-animate '>
                         <div class='solution-tl'>
                             <h1>$title</h1>
                         </div>
                        <div class='solution-tr'>
                            $logo_image
                        </div>
                    </div>
                    <div class='solution-main-content  scroll-animate '>
                        <div class='solution-mc-left'>
                            $content
                        </div>
                    </div>
                    $link_text
                </div>                
                $navigate_next_panel_text            
            </div>
        </div>            
    ";
    return $html;
}

add_shortcode('gp_SolutionSection', 'gp_solution_section_shortcode');