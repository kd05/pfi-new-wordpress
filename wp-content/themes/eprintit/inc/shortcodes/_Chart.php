<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

function Chart_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'    => 'dark',
		'min-height'   => '420px',
		'bg-color'     => false,
		'col1-heading' => '',
		'col2-heading' => '',
		'col3-heading' => '',
		'col4-heading' => '',
		'col5-heading' => '',
		'row1-col1'    => '',
		'row1-col2'    => '',
		'row1-col3'    => '',
		'row1-col4'    => '',
		'row1-col5'    => '',
		'row2-col1'    => '',
		'row2-col2'    => '',
		'row2-col3'    => '',
		'row2-col4'    => '',
		'row2-col5'    => '',
		'row3-col1'    => '',
		'row3-col2'    => '',
		'row3-col3'    => '',
		'row3-col4'    => '',
		'row3-col5'    => '',
		'row4-col1'    => '',
		'row4-col2'    => '',
		'row4-col3'    => '',
		'row4-col4'    => '',
		'row4-col5'    => '',
		'row5-col1'    => '',
		'row5-col2'    => '',
		'row5-col3'    => '',
		'row5-col4'    => '',
		'row5-col5'    => '',
		'row6-col1'    => '',
		'row6-col2'    => '',
		'row6-col3'    => '',
		'row6-col4'    => '',
		'row6-col5'    => '',
	];
	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = $props['min-height'];
	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
	$min_height_style = "min-height: $min_height;";
	$bg_color = "background-color:$bg_color;";

	$style = "style='$min_height_style $bg_color'";
	$content = apply_filters('the_content', $content);

	$check_icon_url = get_template_directory_uri() . '/assets/images/icons/check-icon.png';
	$close_icon_url = get_template_directory_uri() . '/assets/images/icons/close-icon.png';

	$chart_class = '';
	$total_cols = 0;

	$headings = "<div class='rows headings'><div class='row'>";
	for ($col = 1; $col <= 5; $col++) {
		$key = 'col' . $col . '-heading';
		if (isset($props[$key]) && !empty($props[$key])) {
			$headings .= "<div class='col col-$col'>";
			$headings .= $props[$key];
			$headings .= "</div>";
			$total_cols = $col;
		}
	}
	$headings .= "</div></div>";

	if($total_cols == 5) {
		$chart_class = ' full';
	}

	$rows = "<div class='rows'>";

	for ($row = 1; $row <= 6; $row++) {
		$rows .= "<div class='row row-$row'>";
		$cols = "";
		$add_cols = false;
		for ($col = 1; $col <= 5; $col++) {
			$key = 'row' . $row . '-col' . $col;
			if (isset($props[$key]) && !empty($props[$key])) {
				$cols .= "<div class='col col-$col'>";
				$value = $props[$key];
				$multi_value = explode(':', $value);

				if ($multi_value[0] === "checkbox") {
					$cols .= "<div class='icon'><img src='$check_icon_url' alt='' /></div>";
				} else if ($multi_value[0] === "closebox") {
					$cols .= "<div class='icon'><img src='$close_icon_url' alt='' /></div>";
				} else {
					$cols .= $multi_value[0];
				}

				if( isset($multi_value[1]) ) {
					$cols .= $multi_value[1];
				}

				if(!empty($value)) {
					$add_cols = true;
				}

				$cols .= '</div>';
			}
		}

		if($add_cols) {
			$rows .= $cols;
		}
		$rows .= '</div>';
	}
	$rows .= "</div>";

	$html = "
    <div class='shortcode Chart $text_type' $style>
    	<div class='container'><div class='content general-content'>$content</div></div>
    	<div class='container chartContainer$chart_class'>
    		<div class='chartTable'>
        	$headings
        	$rows
        	</div>
      </div>
    </div>";

	return $html;
}

add_shortcode('gp_Chart', 'Chart_shortcode');