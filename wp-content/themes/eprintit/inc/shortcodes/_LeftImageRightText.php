<?php
if (!defined('ABSPATH')) exit; // Exit if accessed directly

function gp_LeftImageRightText_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type' => 'dark',
		'image'     => 5127,
	];
	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$image = gp_get_file_url($props['image']);
	$content = apply_filters('the_content', $content);

	$html = "
    <div class='shortcode LeftImageRightText $text_type'>
    	<div class='content general-content'>
            <div class='content-wrapper'>
                <div class='content-image'><img src='$image' alt=''/></div>
                <div class='content-text'>$content</div>
            </div>
        </div>
    </div>";

	return $html;
}

add_shortcode('gp_LeftImageRightText', 'gp_LeftImageRightText_shortcode');