<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_TwoUpActionsWithButtons
//   min-height= '637px'
//   box1-title= ''
//   box1-description= ''
//   box1-btn-text= ''
//   box1-btn-url= ''
//   box1-bg-img= ''
//   box2-title= ''
//   box2-description= ''
//   box2-btn-text= ''
//   box2-btn-url= ''
//   box2-bg-img= ''
//   text-type= 'dark'
// ]
function TwoUpActionsWithButtons_shortcode($atts, $content = null)
{
	$defaults = [
		'min-height'       => '637px',
		'box1-title'       => false,
		'box1-description' => false,
		'box1-btn-text'    => false,
		'box1-btn-url'     => false,
		'box1-bg-img'      => false,
		'box2-title'       => false,
		'box2-description' => false,
		'box2-btn-text'    => false,
		'box2-btn-url'     => false,
		'box2-bg-img'      => false,
		// 'text-type' => 'dark',
	];
	$props = shortcode_atts($defaults, $atts);

	$min_height = ($props['min-height']) ? $props['min-height'] : '637px';
	$box1_title = ($props['box1-title']) ? $props['box1-title'] : '';
	$box1_description = ($props['box1-description']) ? $props['box1-description'] : '';
	$box1_btn_text = ($props['box1-btn-text']) ? $props['box1-btn-text'] : '';
	$box1_btn_url = ($props['box1-btn-url']) ? $props['box1-btn-url'] : '';
	$box1_bg_img = ($props['box1-bg-img']) ? $props['box1-bg-img'] : '';

	$box2_title = ($props['box2-title']) ? $props['box2-title'] : '';
	$box2_description = ($props['box2-description']) ? $props['box2-description'] : '';
	$box2_btn_text = ($props['box2-btn-text']) ? $props['box2-btn-text'] : '';
	$box2_btn_url = ($props['box2-btn-url']) ? $props['box2-btn-url'] : '';
	$box2_bg_img = ($props['box2-bg-img']) ? $props['box2-bg-img'] : '';

	$btn1 = '';
	$btn2 = '';

	if ($box1_btn_text && $box1_btn_url) {
		$btn = "<a href='$box1_btn_url' class='btn bg-teal text-white'>$box1_btn_text</a>";
	}
	if ($box2_btn_text && $box2_btn_url) {
		$btn2 = "<a href='$box2_btn_url' class='btn bg-teal text-white'>$box2_btn_text</a>";
	}

	$box1_bg_img = gp_get_img_url($box1_bg_img, 'full');
	$box2_bg_img = gp_get_img_url($box2_bg_img, 'full');

	$min_height_style = "min-height: $min_height;";
	$box1_bg_img = $box1_bg_img ? "background-image:url($box1_bg_img);" : "";
	$box2_bg_img = $box2_bg_img ? "background-image:url($box2_bg_img);" : "";

	$style = "style='$min_height_style'";
	$style1 = "style='$box1_bg_img'";
	$style2 = "style='$box2_bg_img'";

	$html = "
    <div class='shortcode TwoUpActionsWithButtons '$style>
      <div class='container-fluid'>
        <div class='row'>
          <div class='col s6 col-left' >
            <div class='overlay-container'$style1>
              <div class='overlay'></div>
              <div class='content position-relative general-content gc-white'>
                <h2 class='scroll-animate'>$box1_title</h2>
                <p class='scroll-animate'>$box1_description</p>
                <div class='btn-container scroll-animate'>
                  $btn 
                </div>
              </div>
            </div>
          </div>
          <div class='col s6 col-right' >
            <div class='overlay-container' $style2>
              <div class='overlay'></div>
              <div class='content position-relative general-content gc-white'>
                <h2 class='scroll-animate'>$box2_title</h2>
                <p class='scroll-animate'>$box2_description</p>
                <div class='btn-container scroll-animate'>
                  $btn2
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>";

	return $html;
}

add_shortcode('gp_TwoUpActionsWithButtons', 'TwoUpActionsWithButtons_shortcode');