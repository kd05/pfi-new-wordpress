<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_LeftTextVideoOrImageBack
//   min-height= ''
//   title= ''
//   description= ''
//   text-type= 'dark'
//   bg-color= ''
//   bg-img= ''
//   bg-video= ''
// ]
function LeftTextVideoOrImageBack_shortcode($atts, $content = null)
{
	$defaults = [
		'min-height'  => '800px',
		'title'       => false,
		'description' => false,
		'text-type'   => 'dark',
		'bg-color'    => false,
		'bg-img'      => false,
		'bg-video'    => false,
		'video'       => '',
		'play_button' => true,
		'btn-text'    => false,
		'btn-url'     => false,
	];

	$props = shortcode_atts($defaults, $atts);

	$min_height = ($props['min-height']) ? $props['min-height'] : '800px';
	$title = ($props['title']) ? $props['title'] : '';
	$description = ($props['description']) ? $props['description'] : '';
	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$bg_img = ($props['bg-img']) ? $props['bg-img'] : false;
	$play_btn_url = get_template_directory_uri() . "/assets/images/play.png";

	$btn_text = ($props['btn-text']) ? $props['btn-text'] : false;
	$btn_url = ($props['btn-url']) ? $props['btn-url'] : false;

	$bg_img = gp_get_img_url($bg_img, 'full');

	$bg_video = ($props['bg-video']) ? $props['bg-video'] : false;
	$bg_video = gp_get_file_url($bg_video);
	$video_url = !empty($props['video']) ? $props['video'] : $bg_video;

	$min_height_style = "min-height: $min_height;";
	$bg_color = "background-color:$bg_color;";
	if($bg_img) {
		$bg_img = "background-image:url($bg_img);";
	}

	if ($bg_video) {

		$bg_video = "
    <video autoplay loop poster muted class='bg-video'>
       <source src='$bg_video' type='video/mp4'>
     </video>

    ";
	}

	$style = "style='$min_height_style $bg_color $bg_img'";
	$btn = '';
	if ($btn_text && $btn_url) {
		$btn = "<a href='$btn_url' class='btn bg-blue text-white'>$btn_text</a>";
	}

	$html = "
    <div class='shortcode LeftTextVideoOrImageBack $text_type' $style>
      $bg_video
      <div class='container'>
        <div class='row'>
          <div class='col s6 general-content gc-white scroll-animate'>
            <h2>$title</h2>
            <p>$description</p>
            $btn
          </div>";

	if ($props['play_button'] && $props['play_button'] != 'false') {
		$html .= "<div class='col flex jc-end'><a data-fancybox='video' href='$video_url'><img src='$play_btn_url'></a></div>";
	}
	$html .= "
		</div>
      </div>
    </div>
  ";

	return $html;
}

add_shortcode('gp_LeftTextVideoOrImageBack', 'LeftTextVideoOrImageBack_shortcode');