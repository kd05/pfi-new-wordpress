<?php

if (!defined('ABSPATH')) {
	exit;
} // Exit if accessed directly

function HomePanel2_shortcode($atts, $content = null)
{

	$defaults = [
		'min-height' => '450px',
		'bg-color'   => '#f4f4f4',
		'title'      => '',
		'image'      => 81,
	];

	$props = shortcode_atts($defaults, $atts);

	$title = !empty($props['title']) ? $props['title'] : "";
	if(!empty($title)) {
		$title = "<h2 class='title scroll-animate'>$title</h2>";
	}

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : '';
	$bg_color = "background-color:$bg_color;";

	$bg_img = isset($props['bg-img']) ? $props['bg-img'] : '';
	$bg_img = gp_get_img_url($bg_img, 'full');
	$bg_img = "background-image:url($bg_img);";
	$image = gp_img_url_from_shortcode_att($props['image']);

	$bg_style = (!$props['bg-img']) ? $bg_color : $bg_img;
	$min_height_style = gp_min_height_from_shortcode_props($props);
	$style = "style='$bg_style $min_height_style'";
	$_content = apply_filters('the_content', $content);

	$icons_template = get_template_directory_uri() . '/assets/images';
	$word_icon = $icons_template . '/icons/word.png';
	$pdf_icon = $icons_template . '/icons/pdf.png';
	$excel_icon = $icons_template . '/icons/excel.png';
	$fax_icon = $icons_template . '/icons/fax.png';
	$barcode_icon = $icons_template . '/icons/barcode.png';

	$word_text = __('Microsoft Word', 'wpBabywp');
	$pdf_text = __('PDF', 'wpBabywp');
	$excel_text = __('Microsoft Excel', 'wpBabywp');
	$fax_text = __('Fax or Scan', 'wpBabywp');
	$barcode_text = __('CP Print Page Separator', 'wpBabywp');

	$icons = "<ul>";
	$icons .= "<li><span class='icon'><img src='$word_icon' alt=''/></span><span class='icon-title'>$word_text</span></li>";
	$icons .= "<li><span class='icon'><img src='$pdf_icon' alt=''/></span><span class='icon-title'>$pdf_text</span></li>";
	$icons .= "<li><span class='icon'><img src='$excel_icon' alt=''/></span><span class='icon-title'>$excel_text</span></li>";
	$icons .= "<li><span class='icon'><img src='$fax_icon' alt=''/></span><span class='icon-title'>$fax_text</span></li>";
	$icons .= "</ul>";
	$icons .= "<ul class='single'>";
	$icons .= "<li><span class='icon'><img src='$barcode_icon' alt=''/></span><span class='icon-title'>$barcode_text</span></li>";
	$icons .= "</ul>";

	$hero = "
    <div class='shortcode HomePanel2' $style>
      <div class='container'>
      	$title
        <div class='row'>
          <div class='col contentcol scroll-animate'>
            <div class='content general-content'>
              $_content
            </div>
          </div>
          <div class='imagescol'>
          	<div class='col imagecol scroll-animate'>
            	<img src='$image' />
          	</div>
          	<div class='col iconscol scroll-animate'>$icons</div>
          </div>
        </div>
      </div>
    </div>";

	return $hero;
}

add_shortcode('gp_HomePanel2', 'HomePanel2_shortcode');