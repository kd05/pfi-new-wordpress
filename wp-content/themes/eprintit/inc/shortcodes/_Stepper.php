<?php
if (!defined('ABSPATH')) exit; // Exit if accessed directly

function gp_Stepper_shortcode($atts, $content = null)
{

    $step_atts = array();
    for ($i = 1; $i <= 12; $i++) {
        $s_atts = array(
            'step' . $i => '',
            'step' . $i . '-img' => '',
            'step' . $i . '-title' => '',
            'step' . $i . '-content' => '',
            'step' . $i . '-content2' => '',
        );
        $step_atts = array_merge($step_atts, $s_atts);
    }

    $defaults = [
        'text-type' => 'dark',
        'min-height' => '625px',
        'bg-color' => false,
        'bg-img' => false,
        'step1' => 'Capture',
        'step1-img' => '',
        'step1-title' => '',
        'step1-content' => '',
        'step2' => 'Read',
        'step2-img' => '',
        'step2-title' => '',
        'step2-content' => '',
        'step3' => 'Classify',
        'step3-img' => '',
        'step3-title' => '',
        'step3-content' => '',
        'step4' => 'Extract',
        'step4-img' => '',
        'step4-title' => '',
        'step4-content' => '',
    ];
    $defaults = array_merge($defaults, $step_atts);
    $props = shortcode_atts($defaults, $atts);

    $text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? ' dark' : ' light';
    $min_height = $props['min-height'] ? $props['min-height'] : '';

    $bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
    $bg_img = ($props['bg-image']) ? $props['bg-image'] : false;

    if ($bg_color) {
        $bg_color = "background-color: $bg_color;";
    }

    if ($bg_img) {
        $bg_img = gp_get_file_url($bg_img);
        $bg_img = "background-image: url($bg_img);";
    }

    // Stepper
    $stepper_nav = '';
    $stepper_content = '';
    // Check for icons
    $stepper_class = '';
    $count = 0;
    for ($i = 1; $i <= 12; $i++) {
        $step_nav_title = $props['step' . $i];
        $active = $i === 1 ? " active" : "";
        $step_class = $i > 3 && $i == 4 ? " last-visible" : "";
        if (!empty($step_nav_title)) {
            $stepper_nav .= '<div class="step' . $active . $step_class . '" data-step="' . $i . '">';
            $stepper_nav .= '<div class="step-inner">';
            $stepper_nav .= '<div class="icon-wrapper">';
            $stepper_nav .= '<div class="icon"><span>' . $i . '</span></div>';
            $stepper_nav .= '</div>';
            $stepper_nav .= '<p>' . $step_nav_title . '</p>';
            $stepper_nav .= '</div>';
            $stepper_nav .= '</div>';

            if (!empty($props['step' . $i . '-img']) && !empty($props['step' . $i . '-title']) && !empty($props['step' . $i . '-content'])) {
                $title = $props['step' . $i . '-title'];
                $_content = $props['step' . $i . '-content'];
                $_content2 = isset($props['step' . $i . '-content2']) ? $props['step' . $i . '-content2'] : '';
                $img_src = gp_get_file_url($props['step' . $i . '-img']);
                $stepper_content .= '<div class="stepper-content' . $active . '" data-content="' . $i . '">';
                $stepper_content .= '<div class="stepper-img">';
                $stepper_content .= '<img src="' . $img_src . '" alt="Icon" />';
                $stepper_content .= '</div>';
                $stepper_content .= '<div class="stepper-text">';
                $stepper_content .= '<div class="stepper-text-inner">';
                $stepper_content .= '<h2>' . $title . '</h2>';
                $stepper_content .= '<p>' . $_content . '</p>';
                $stepper_content .= '<p>' . $_content2 . '</p>';
                $stepper_content .= '</div>';
                $stepper_content .= '</div>';
                $stepper_content .= '</div>';
            }
            $count++;
        }
    }

    if ($count > 4) {
        $stepper_class = ' show';
    }

    $stepper = '<div class="stepper-wrapper">';
    $stepper .= '<div class="stepper-nav">';
    $stepper .= '<div class="step-arrow prev"><i class="fa fa-chevron-left"></i></div>';
    $stepper .= '<div class="stepper-nav-inner">' . $stepper_nav . '</div>';
    $stepper .= '<div class="step-arrow next'.$stepper_class.'"><i class="fa fa-chevron-right"></i></div>';
    $stepper .= '</div>';
    $stepper .= '<div class="stepper-content-wrapper">';
    $stepper .= $stepper_content;
    $stepper .= '<div class="stepper-content-arrows">';
    $stepper .= '<div class="content-arrow prev"><i class="fa fa-chevron-left"></i></div>';
    $stepper .= '<div class="content-arrow next show"><i class="fa fa-chevron-right"></i></div>';
    $stepper .= '</div>';
    $stepper .= '</div>';
    $stepper .= '</div>';

    $bg_style = (!$bg_img) ? $bg_color : $bg_img;
    $min_height_style = !empty($min_height) ? "min-height:$min_height;" : '';
    $style = "style='$bg_style $min_height_style'";
    $content = apply_filters('the_content', $content);

    $html = "
    <div class='shortcode Stepper flex$text_type' $style>
    	<div class='container'>
    		<div class='row'>
    			<div class='col s12'>
    				<div class='content'>$content$stepper</div>
    			</div>
    		</div>
    	</div>
    </div>";

    return $html;
}

add_shortcode('gp_Stepper', 'gp_Stepper_shortcode');