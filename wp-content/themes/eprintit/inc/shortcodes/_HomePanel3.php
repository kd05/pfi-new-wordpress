<?php

if (!defined('ABSPATH')) {
	exit;
} // Exit if accessed directly

function HomePanel3_shortcode($atts, $content = null)
{

	$defaults = [
		'min-height' => '750px',
		'bg-color'   => '#ffffff',
		'title'      => '',
	];

	$props = shortcode_atts($defaults, $atts);

	$title = !empty($props['title']) ? $props['title'] : "";
	if(!empty($title)) {
		$title = "<h2 class='title scroll-animate'>$title</h2>";
	}

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : '';
	$bg_color = "background-color:$bg_color;";
	$bg_style = $bg_color;
	$min_height_style = gp_min_height_from_shortcode_props($props);
	$style = "style='$bg_style $min_height_style'";
	$_content = apply_filters('the_content', $content);

	$icons_template = get_template_directory_uri() . '/assets/images';
	$email_icon = $icons_template . '/icons/email.png';
	$box_icon = $icons_template . '/icons/box.png';
	$sharepoint_icon = $icons_template . '/icons/sharepoint.png';
	$ftp_icon = $icons_template . '/icons/ftp.png';
	$sharebase_icon = $icons_template . '/icons/sharebase.png';
	$thru_icon = $icons_template . '/icons/thru.png';
	$drive_icon = $icons_template . '/icons/google-drive.png';
	$docshare = $icons_template . '/icons/docshare.png';
	$dropbox = $icons_template . '/icons/dropbox.png';
	$fax_icon = $icons_template . '/icons/fax.png';
	$egnyte_icon = $icons_template . '/icons/egnyte.jpg';

	$email_text = __('Email', 'wpBabywp');
	$box_text = __('Box', 'wpBabywp');
	$sharepoint_365_text = __('Sharepoint 365', 'wpBabywp');
	$ftp_text = __('FTP', 'wpBabywp');
	$sharebase_text = __('Sharebase', 'wpBabywp');
	$thru_text = __('Thru', 'wpBabywp');
	$drive_text = __('Google Drive', 'wpBabywp');
	$docshare_text = __('Docshare', 'wpBabywp');
	$sharepoint_2013_text = __('Sharepoint 2013', 'wpBabywp');
	$dropbox_text = __('Dropbox', 'wpBabywp');
	$docshare_flex_text = __('Docshare Flex', 'wpBabywp');
	$fax_text = __('Fax', 'wpBabywp');
	$egnyte_text = __('Egnyte', 'wpBabywp');

	$icons = "<ul>";
	$icons .= "<li><span class='icon'><img src='$email_icon' alt='' width='62px'/></span><span class='icon-title'>$email_text</span></li>";
	$icons .= "<li><span class='icon'><img src='$box_icon' alt='' width='59px'/></span><span class='icon-title'>$box_text</span></li>";
	$icons .= "<li><span class='icon'><img src='$sharepoint_icon' alt='' width='62px'/></span><span class='icon-title'>$sharepoint_365_text</span></li>";
	$icons .= "<li><span class='icon'><img src='$ftp_icon' alt='' width='65px'/></span><span class='icon-title'>$ftp_text</span></li>";
	$icons .= "<li><span class='icon'><img src='$sharebase_icon' alt='' width='59px'/></span><span class='icon-title'>$sharebase_text</span></li>";
	$icons .= "<li><span class='icon'><img src='$thru_icon' alt='' width='69px'/></span><span class='icon-title'>$thru_text</span></li>";
	$icons .= "<li><span class='icon'><img src='$drive_icon' alt='' width='67px'/></span><span class='icon-title'>$drive_text</span></li>";
	$icons .= "<li><span class='icon'><img src='$docshare' alt='' width='59px'/></span><span class='icon-title'>$docshare_text</span></li>";
	$icons .= "<li><span class='icon'><img src='$sharepoint_icon' alt='' width='62px'/></span><span class='icon-title'>$sharepoint_2013_text</span></li>";
	$icons .= "<li><span class='icon'><img src='$dropbox' alt='' width='60px'/></span><span class='icon-title'>$dropbox_text</span></li>";
	$icons .= "<li><span class='icon'><img src='$docshare' alt='' width='59px'/></span><span class='icon-title'>$docshare_flex_text</span></li>";
	$icons .= "<li><span class='icon'><img src='$fax_icon' alt='' width='57px'/></span><span class='icon-title'>$fax_text</span></li>";
	$icons .= "<li><span class='icon'><img src='$egnyte_icon' alt='' width='57px'/></span><span class='icon-title'>$egnyte_text</span></li>";
	$icons .= "</ul>";

	$share_text = __('Share Your Documents to:', 'wpBabywp');
	$hero = "
    <div class='shortcode HomePanel3' $style>
      <div class='container'>
      	$title
        <div class='row'>
          <div class='col contentcol'>
          	<div class='bigicon scroll-animate'><i class='fa fa-share-alt'></i></div>
          	<p>$share_text</p>
          </div>
          <div class='col iconscol scroll-animate' data-delay='500'>$icons</div>
        </div>
        <div class='content general-content scroll-animate' data-delay='650'>$_content</div>
      </div>
    </div>";

	return $hero;
}

add_shortcode('gp_HomePanel3', 'HomePanel3_shortcode');