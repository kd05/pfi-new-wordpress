<?php
/**
 * Renders the map shortcode template
 */

// Get the locations from file
gp_render_locations();
?>
<div class="shortcode PrintLocations">
    <section class="hero-wrapper">
        <div id="search-wrapper" class="show open">
            <div class="sidebar-tab"><i class="fa fa-angle-double-left"></i></div>
            <form action="" method="post">
                <div class="row nospace">
                    <div class="input-wrapper grey">
                        <input type="text" placeholder="Enter a city">
                    </div>
                    <button class="button icon-only"><i class="fa fa-search"></i></button>
                </div>
            </form>
            <div class="results-wrapper">
                <div class="all-results-wrapper">
                    <div class="results-list">
                        <div class="result">
                            <p>FedEx Office</p>
                            <p>New York NY Lexington Ave</p>
                        </div>
                        <div class="result">
                            <p>FedEx Office 2</p>
                            <p>New York NY Lexington Ave</p>
                        </div>
                        <div class="result">
                            <p>FedEx Office 3</p>
                            <p>New York NY Lexington Ave</p>
                        </div>
                        <div class="result">
                            <p>FedEx Office 4</p>
                            <p>New York NY Lexington Ave</p>
                        </div>
                        <div class="result">
                            <p>FedEx Office 5</p>
                            <p>New York NY Lexington Ave</p>
                        </div>
                        <div class="result">
                            <p>FedEx Office 6</p>
                            <p>New York NY Lexington Ave</p>
                        </div>
                        <div class="result">
                            <p>FedEx Office 7</p>
                            <p>New York NY Lexington Ave</p>
                        </div>
                    </div>
                    <div class="results-bottom">
                        <div class="arrow left"><i class="fa fa-angle-left"></i></div>
                        <p>Viewing Results <span class="showing">21</span>-<span class="total">40</span></p>
                        <div class="arrow right"><i class="fa fa-angle-right"></i></div>
                    </div>
                </div>
                <div class="result-details-wrapper">
                    <div class="result-details-top">
                        <span class="back-to-results"><i class="fa fa-angle-left"></i>See all Results</span>
                        <div class="result-image" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/fedex-location.jpg')"></div>
                        <div class="result-title">
                            <p>FedEx Office</p>
                            <p>New York NY Lexington Ave</p>
                        </div>
                        <div class="result-meta">
                            <div><p><i class="fa fa-map-marker"></i>1122 Lexington Ave, New York, NY 10075, USA</p></div>
                            <div><p><i class="fa fa-phone"></i>(123)-456-789</p></div>
                            <div class="hours-wrapper">
                                <div><i class="fa fa-clock-o"></i>Store Hours: <span class="view-hours">Open <i class="fa fa-angle-down"></i></span>
                                    <ul class="open-hours">
                                        <li>Monday-Friday: 9AM-5PM</li>
                                        <li>Saturday: 9AM-5PM</li>
                                        <li>Sunday: 9AM-5PM</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="result-details-bottom">
                        <div class="file-upload-wrapper">
                            <form action="" method="post">
                                <label for="file-upload">
                                    <span>Select the files you want to print, or drop them here</span>
                                    <span class="faux-button">Select Your Files</span>
                                </label>
                                <input type="file" multiple name="file-upload" id="file-upload">
                            </form>
                        </div>
                        <div class="detail-actions">
                            <div class="action">
                                <i class="fa fa-share-alt"></i>
                                <span>Share</span>
                            </div>
                            <div class="action">
                                <i class="fa fa-mobile-phone"></i>
                                <span>Send to Phone</span>
                            </div>
                            <div class="action">
                                <div class="details-icon">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">
                                    <path class="svg-icon" d="M21.6,10c0.3,0.3,0.4,0.6,0.4,1s-0.1,0.7-0.4,1L12,21.6c-0.3,0.3-0.6,0.4-1,0.4s-0.7-0.1-1-0.4
                                        L0.4,12C0.1,11.7,0,11.4,0,11s0.1-0.7,0.4-1L10,0.4C10.3,0.1,10.6,0,11,0s0.7,0.1,1,0.4L21.6,10z M17.3,10.6
                                        c0.1-0.1,0.1-0.1,0.1-0.3s0-0.2-0.1-0.3l-3.7-3.4c-0.1-0.1-0.2-0.1-0.3,0S13.1,6.8,13.1,7v2.3H8.3c-0.4,0-0.7,0.1-1,0.4
                                        s-0.4,0.6-0.4,1v3.4c0,0.1,0,0.2,0.1,0.2s0.2,0.1,0.2,0.1h1.4c0.1,0,0.2,0,0.2-0.1s0.1-0.2,0.1-0.2v-2.8h4.1v2.3
                                        c0,0.1,0.1,0.2,0.2,0.3s0.3,0,0.3,0L17.3,10.6z"/>
                                    </svg>
                                </div><span>Directions</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="hero-map" class="hero-map" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/map-placeholder.jpg')"></div>
    </section>
</div>
