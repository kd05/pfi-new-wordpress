<?php

// see js shortcode_fix() too maybe
remove_filter('the_content', 'wpautop');
add_filter('the_content', 'wpautop', 99);
add_filter('the_content', 'shortcode_unautop', 100);

require_once '_HomePanel1.php';
require_once '_HomePanel2.php';
require_once '_HomePanel3.php';

//require_once '_chosen-featured.php';
//require_once '_make-btn.php';
require_once '_button.php';
//require_once '_wrapper.php';
//require_once '_split-row.php';
//require_once '_LetsPrintNow.php';
require_once '_MobileApps.php';
require_once '_ThreeBenefits.php';
require_once '_featured.php';
//require_once '_FindNearestLocation.php';
require_once '_LeftTextVideoOrImageBack.php';
require_once '_CenteredTextOverVidOrImage.php';
require_once '_RightTextOverVidOrImageWithButton.php';
require_once '_TwoUpActionsWithButtons.php';
require_once '_RightTextTwoThirdsOverVidOrImageWithLeftImageAndButton.php';
require_once '_testimonials.php';
require_once '_ForegroundImageOrVideo.php';
require_once '_Instructions.php';
require_once '_Solutions.php';
require_once '_5050RightImage.php';
require_once '_5050LeftImage.php';
require_once '_ForegroundImageWithTitleAndButton.php';
require_once '_LeftTextOverVidOrImageWithButton.php';
require_once '_CalculateYourEarnings.php';
require_once '_BusinessPartners.php';
require_once '_AccreditationCPT.php';
require_once '_CustomersCPT.php';
require_once '_BackgroundImageOrVideo.php';
require_once '_PotentialCPT.php';
require_once '_PrintLocations.php';
require_once '_FullyHosted.php';
require_once '_ExploreCPT.php';
require_once '_OurTeam.php';
require_once '_CenteredPanel.php';
require_once '_Accordion.php';
require_once '_uniPrint.php';
require_once '_LeftVideoRightText.php';
require_once '_Chart.php';
require_once '_IconGrid.php';
require_once '_MultiIconPanel.php';
require_once '_PlatformApplications.php';
require_once '_SolutionsHero.php';
require_once '_RightImageWithIcons.php';
require_once '_LeftImageWithIcons.php';
require_once '_Stepper.php';
require_once '_FooterForm.php';
require_once '_LeftImageRightText.php';

require_once '_LeftTextWithButtonImageRight.php';
require_once '_LeftTextRight1FieldForm.php';
require_once '_LeftTextRightButton.php';



//PFI
require_once '_VideoPopupSection.php';
require_once '_SolutionSection.php';
require_once '_NewsBlogSection.php';

require_once '_AboutBannerPanel.php';
require_once '_LeftContentOnImage.php';
require_once '_LeftContentRightImage.php';
require_once '_ContentCenterPanel.php';
require_once '_TestimonialPfi.php';
require_once '_ContactContentCenterPanel.php';
require_once '_LeftFixScrollAnimation.php';
require_once '_ThreeColumnPanel.php';
