<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_Instructions
//   min-height= '910px'
//   text-type= ''
//   bg-color= ''
//   bg-image= ''
//   title= ''
//   testimonial_1= ''
//   testimonial_2= ''
//   testimonial_3= ''
//   testimonial_4= ''
//   testimonial_5= ''
// ]
function Instructions_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'     => 'dark',
		'min-height'    => false,
		'bg-color'      => false,
		'bg-image'      => false,
		'title'         => false,
		'main-image'    => '',
		'testimonial_1' => false,
		'testimonial_2' => false,
		'testimonial_3' => false,
		'testimonial_4' => false,
		'testimonial_5' => false,
	];
	$props = shortcode_atts($defaults, $atts);

	$min_height = ($props['min-height']) ? $props['min-height'] : '800px';
	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? '#5a5a5a' : '#fefefe';
	$text_type = $props['text-type'];
	$bg_color = ($props['bg-color']) ? $props['bg-color'] : 'none';
	$bg_image = ($props['bg-image']) ? $props['bg-image'] : '';
	$title = ($props['title']) ? $props['title'] : '';

	// Get the placeholder
	$placeholder_img = get_template_directory_uri() . '/assets/images/placeholder-1.jpg';
	$main_image = !empty($props['main-image']) ? gp_get_img_url($props['main-image'], 'full') : $placeholder_img;

	// Get the testimonials
	$testimonials = array_filter($props, 'filter_instructions_post', ARRAY_FILTER_USE_BOTH);
	// Get the ids only
	$testimonials = array_values($testimonials);
	// Select a random id
	$index = rand(0, count($testimonials) - 1);
	$testimonials = !empty($testimonials) ? $testimonials[$index] : [];

	$bg_image = !empty($bg_image) ? gp_get_img_url($bg_image, 'full') : '';
	$min_height_style = "min-height: $min_height;";
	$bg_color = "background-color: $bg_color;";
	$bg_image = !empty($bg_image) ? "background-image: url('$bg_image');" : "";

	$cimg_style = "style='background-image:url($main_image);'";
	$style = "style='$min_height_style $bg_color $bg_image'";

	$selected_testimonial_html = '';

	// Get the testimonial post
	if (!empty($testimonials)) {
		$args = array(
			'post_type'      => 'testimonials',
			'status_type'    => 'publish',
			'p'              => $testimonials,
			'posts_per_page' => -1,
		);

		$q = new WP_Query($args);

		if ($q->have_posts()) {

			// Start looping over the query results.
			while ($q->have_posts()) {
				$q->the_post();

				$selected_testimonial_id = get_the_ID();
				$selected_testimonial_url = gp_get_img_url_from_post($selected_testimonial_id);
				if(!$selected_testimonial_url) {
					// Try profile pic url
					$profile_pic_url = get_field('profile_pic_url');
					$selected_testimonial_url = $profile_pic_url;
				}
				$selected_testimonial_title = get_the_title();
				$selected_testimonial_excerpt = get_the_excerpt();
				$selected_testimonial_excerpt = (strlen($selected_testimonial_excerpt) > 150) ? substr($selected_testimonial_excerpt, 0, 150) . '...' : $selected_testimonial_excerpt;
				$selected_testimonial_html = "
                  <img class='circle' src='$selected_testimonial_url'>
                  <div>
                    <p class='bold mb-1'>$selected_testimonial_title</p>
                    <p>$selected_testimonial_excerpt</p>
                  </div>
            ";
			}
			wp_reset_postdata();
		}
	}
	$right_html = '';
	if (!empty($selected_testimonial_html)) {
		$right_html = "<div class='col s6'>
            <div class='card'>
              <div class='cimg scroll-animate' $cimg_style></div>
              <div class='cbody'>
              <div class='media scroll-animate'>
                  $selected_testimonial_html
                </div>
              </div>
            </div>
          </div>";
	}

	$html = "
    <div class='shortcode Instructions' $style>

      <div class='container'>

        <div class='row'>
          <div class='col s6 general-content scroll-animate $text_type'>
            <h2 class='$text_type'>$title</h2>
            " . apply_filters('the_content', $content) . "
          </div>
          $right_html

        </div>
      </div>
    </div>
  ";
	return $html;
}

add_shortcode('gp_Instructions', 'Instructions_shortcode');

function filter_instructions_post($val, $key)
{
	return (strpos($key, 'testimonial') !== FALSE && $val);
}