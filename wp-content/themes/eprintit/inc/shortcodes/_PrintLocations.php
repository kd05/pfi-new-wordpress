<?php
if (!defined('ABSPATH')) { exit; } // Exit if accessed directly

function render_PrintLocations($atts, $content = null)
{
	// Start Buffer
	ob_start();

	// Include template
	get_template_part('inc/shortcodes/templates/map');

	$url = array(
		'directory' => get_template_directory_uri(),
	);
	wp_localize_script('global-js', 'theme', $url);

	// Get html
	$html = ob_get_clean();
	// Return
	return $html;
}

add_shortcode('gp_PrintLocations', 'render_PrintLocations');