<?php

if (!defined('ABSPATH')) {
	exit;
} // Exit if accessed directly

function HomePanel1_shortcode($atts, $content = null)
{

	$defaults = [
		'min-height'     => '774px',
		'bg-color'       => false,
		'bg-img'         => 4351,
        'bg-video'       => false,
		'button1_label'  => '',
		'button1_url'    => '',
		'button1_target' => '_blank',
		'button1_lightbox' => false,
		'button2_label'  => '',
		'button2_url'    => '',
		'button2_target' => '_blank',
        'button2_lightbox' => false,
	];

	$props = shortcode_atts($defaults, $atts);

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : '';
	$bg_color = "background-color:$bg_color;";

	$bg_img = isset($props['bg-img']) ? $props['bg-img'] : '';
	if(!empty($bg_img)) {
		$bg_img = gp_get_img_url($bg_img, 'full');
		$bg_img = "background-image:url($bg_img);";
	}

	$button1_label = !empty($props['button1_label']) ? $props['button1_label'] : "";
	$button1_url = !empty($props['button1_url']) ? $props['button1_url'] : "";
	$button1_target = !empty($props['button1_target']) ? $props['button1_target'] : "";
	$button2_label = !empty($props['button2_label']) ? $props['button2_label'] : "";
	$button2_url = !empty($props['button2_url']) ? $props['button2_url'] : "";
	$button2_target = !empty($props['button2_target']) ? $props['button2_target'] : "";

	$btns = "";
	if( !empty($button1_label) || !empty($button2_label) ) {
		$btns = '<div class="btn-container">';
		if( !empty($button1_label) ) {
		    $button1_attr = $props['button1_lightbox'] ? " data-fancybox='video' " : '';
			$btns .= "<a href='$button1_url' title='' target='$button1_target'$button1_attr class='btn'>$button1_label</a>";
		}
		if( !empty($button2_label) ) {
            $button2_attr = $props['button2_lightbox'] ? " data-fancybox='video' " : '';
			$btns .= "<a href='$button2_url' title='' target='$button2_target'$button2_attr class='btn'>$button2_label</a>";
		}
		$btns .= '</div>';
	}

	$has_video = '';
//	$min_height_style = gp_min_height_from_shortcode_props($props);
	$min_height_style = '';
	$style = "style='$min_height_style'";
	$_content = apply_filters('the_content', $content);
    $bg_style = (!$props['bg-img']) ? $bg_color : $bg_img;

	if ($props['bg-video']) {

	    $bg_content = "
	        <div class='panel-bg'>
	            <video autoplay loop poster muted class='bg-video'><source src='" . $props['bg-video']. "' type='video/mp4'></video>
            </div>
	    ";

	    $has_video = 'has-video';
    } else {
	    $bg_content = "
	        <div class='panel-bg' style='$bg_style'></div>
	    ";
    }

	$hero = "
    <div class='shortcode HomePanel1 $has_video' $style>
    <div class='upper-content'>
      <div class='lg-container'>
        <div class='row'>
            <div class='contentcol content general-content gc-black'>
              $_content
              $btns
            </div>
        </div>
      </div>
      </div>
      $bg_content
    </div>";

	return $hero;
}

add_shortcode('gp_HomePanel1', 'HomePanel1_shortcode');