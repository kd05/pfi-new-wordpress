<?php

if (!defined('ABSPATH')) {
	exit;
} // Exit if accessed directly

// [gp_OurTeam
// text-type= 'dark'
// min-height=''
// bg-color=''
// ]
function OurTeam_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'min-height' => false,
		'bg-color'   => false,
		'title'      => '',
		'subtitle'   => ''
	];

	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = ($props['min-height']) ? $props['min-height'] : '1374px';
	$title = $props['title'];
	$subtitle = $props['subtitle'];

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;

	if ($bg_color) {
		# code...
		$bg_color = "background-color: $bg_color;";
	}


	$args = array(
		'post_type'      => 'team',
		'post_status'    => 'publish',
		'orderby'        => 'menu_order',
		'order'          => 'DESC',
		'posts_per_page' => -1,
	);
	$q = new WP_Query($args);
	$post_count = $q->post_count;
	$tablet_slides = '';
	$phone_slides = '';
	$slides = '';

	$tablet_row = '';
	$phone_row = '';
	$row = '';

	$tablet_item = '';
	$phone_item = '';
	$item = '';

	$slide_rows = '';
	$counter = 1;
	if ($q->have_posts()) {

		// Start looping over the query results.
		while ($q->have_posts()) {

			$q->the_post();

			$team_member_id = get_the_ID();
			$team_member_img_url = gp_get_img_url_from_post($team_member_id);
			$team_member_name = get_the_title();
			$team_member_position = get_field('team_position');
			$team_member_excerpt = get_the_excerpt();
			$bg_image_url = "style='background-image:url($team_member_img_url)'";

			$href = get_the_permalink($team_member_id);

			$phone_item = "
                <div class='card'>
                <a href='$href' class='href-on-top'></a>
                  <div class='cimg circle' $bg_image_url></div>
                  <div class='cbody'>
                    <p class='name bold'>$team_member_name</p>
                    <p class='position text-red sbold'>$team_member_position</p>
                    <p class='excerpt'>$team_member_excerpt</p>
                  </div>
                </div>
            ";

			$tablet_item .= "
                <div class='card'>
                <a href='$href' class='href-on-top'></a>
                  <div class='cimg circle' $bg_image_url></div>
                  <div class='cbody'>
                    <p class='name bold'>$team_member_name</p>
                    <p class='position text-red sbold'>$team_member_position</p>
                    <p class='excerpt'>$team_member_excerpt</p>
                  </div>
                </div>
            ";

			$item .= "
                <div class='card'>
                <a href='$href' class='href-on-top'></a>
                  <div class='cimg circle' $bg_image_url></div>
                  <div class='cbody'>
                    <p class='name bold'>$team_member_name</p>
                    <p class='position text-red sbold'>$team_member_position</p>
                    <p class='excerpt'>$team_member_excerpt</p>
                  </div>
                </div>
            ";

			$phone_slides .= "
              <div class='slide'>
                $phone_item
              </div>
            ";


			if ($counter % 2 == 0) {
				# code...
				$tablet_row .= " 
                <div class='flex jc-center'>
                  $tablet_item
                </div>
              ";

				if ($counter !== $post_count) {
					# code...
					$tablet_item = "";
				}
			}


			if ($counter % 4 === 0) {
				# code..

				$tablet_slides .= "
                <div class='slide'>
                    $tablet_row
                </div>
              ";

				$tablet_row = "";
				$next_tablet_row = true;
			}

			if ($counter % 6 === 0) {
				# code..
				$slides .= "
                <div class='slide'>
                  <div class='flex jc-center'>
                    $item
                  </div>
                </div>
              ";


				if ($counter !== $post_count) {

					$item = "";
				}

			}


			if ($counter === $post_count) {

				//tablet
				// add items to row
				// add row to slide

				$tablet_slides .= "
                <div class='slide'>
                  <div class='flex jc-center'>
                    $tablet_item
                  </div>
                </div>
              ";


				// desktop
				// add items to row
				// add row to slide
				$slides .= "
                <div class='slide'>
                  <div class='flex jc-center'>
                    $item
                  </div>
                </div>
              ";


			}
			$counter++;
		}
		wp_reset_postdata();
	}

	$min_height_style = "min-height:$min_height;";

	$style = "style='$bg_color $min_height_style'";

	$html = "
    <div class='shortcode v-padding OurTeam flex ai-center $text_type' $style>

      <div class='lg-container'>

          <div class='row'>

            <div class='col s12 text-center general-content scroll-animate'>
              <h3 class='fw-400'>$title</h3>
              <h2>$subtitle</h2>
            </div>
            
            <div class='spacer type-80 after-title'></div>
            
            <div class='col'>
              <div class='container hide-tablet'>

                <div class='arrow left-team-arrow text-red'>
                  <span><i class='fa fa-chevron-left'></i></span>
                </div>
                <div class='team-slider'>
                  $slides
                </div>
                <div class='arrow right-team-arrow text-red'>
                  <span><i class='fa fa-chevron-right'></i></span>
                </div>

              </div>

              <div class='container tablet-phone'>

                <div class='arrow left-team-phone-arrow text-red'>
                  <span><i class='fa fa-chevron-left'></i></span>
                </div>
                <div class='team-slider-phone'>
                  $phone_slides
                </div>
                <div class='arrow right-team-phone-arrow text-red'>
                  <span><i class='fa fa-chevron-right'></i></span>
                </div>

              </div>

            </div>
            
          </div>
      </div>
    </div>";
	return $html;
}

add_shortcode('gp_OurTeam', 'OurTeam_shortcode');