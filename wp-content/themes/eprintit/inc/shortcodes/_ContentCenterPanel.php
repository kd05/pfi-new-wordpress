<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

function gp_content_center_panel_shortcode($atts, $content = null)
{
    $defaults = [
        'id'  => '',

        'title'      => 'Be Part of the team',
        'text-align' => 'center',
    ];
    $props = shortcode_atts($defaults, $atts);

    $id = !empty($props['id']) ? $props['id'] : "";

    $title = !empty($props['title']) ? $props['title'] : "";
    $text_align = 'text-' . $props['text-align'];

    $content = apply_filters('the_content', $content);

    $html = "
        <div class='shortcode pfi-shortcode ContentCenterPanel'  id='$id' > 
                <div class='ccp-inner'>
                        <div class='ccp-title'>
                            <h1>$title</h1>
                        </div>
                        <div class='ccp-content $text_align'>
                            $content
                        </div>                     
                </div>     
        </div>            
    ";
    return $html;

}
add_shortcode('gp_ContentCenterPanel', 'gp_content_center_panel_shortcode');


