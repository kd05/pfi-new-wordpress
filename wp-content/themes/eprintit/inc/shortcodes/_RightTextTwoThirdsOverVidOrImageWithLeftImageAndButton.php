<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_RightTextTwoThirdsOverVidOrImageWithLeftImageAndButton
//   min-height= '698px'
//   title= ''
//   description= ''
//   btn-text= ''
//   btn-url= ''
//   bg-img= ''
//   bg-img= ''
//   text-type= 'dark'
// ]
function RightTextTwoThirdsOverVidOrImageWithLeftImageAndButton_shortcode($atts, $content = null)
{
	$defaults = [
		'min-height' => '698px',
		'title'      => false,
		'btn-text'   => false,
		'btn-url'    => false,
		'img'        => false,
		'bg-img'     => false,
		'bg-color'   => false,
		'text-type'  => 'dark',
	];
	$props = shortcode_atts($defaults, $atts);

	$min_height = $props['min-height'];
	$title = ($props['title']) ? $props['title'] : '';
	$btn_text = ($props['btn-text']) ? $props['btn-text'] : false;
	$btn_url = ($props['btn-url']) ? $props['btn-url'] : false;
	$img = ($props['img']) ? $props['img'] : '2623';

	$bg_img = ($props['bg-img']) ? $props['bg-img'] : '';
	$bg_color = ($props['bg-color']) ? $props['bg-color'] : 'white';

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';

	$btn = '';

	if ($btn_text && $btn_url) {
		$btn_url = strip_tags($btn_url); // Strip out any tags
		// code...
		$btn = "<a href='$btn_url' class='btn bg-blue text-white'>$btn_text</a>";
	}

	$content = apply_filters('the_content', $content);

	$img = gp_get_img_url($img, 'full');
	$bg_img = !empty($bg_img) ? gp_get_img_url($bg_img, 'full') : '';

	$min_height_style = "min-height: $min_height;";
	$bg_img = !empty($bg_img) ? "background-image:url($bg_img);" : "";
	$bg_color = "background-color: $bg_color;";

	$style = "style='$min_height_style $bg_color $bg_img'";

	$html = "
    <div class='shortcode RightTextTwoThirdsOverVidOrImageWithLeftImageAndButton $text_type' $style>
      <div class='container' >
        <div class='row ai-center'>
          <div class='col s4 image scroll-animate' data-delay='400'>
            <img src='$img' />
          </div>
          <div class='col s8'>
            <div class='content general-content scroll-animate'>
              <h2 class=''>$title</h2>
              $content
              <div class='btn-container'>$btn</div>
            </div>
          </div>
        </div>
      </div>
    </div>";
	return $html;
}

add_shortcode('gp_RightTextTwoThirdsOverVidOrImageWithLeftImageAndButton', 'RightTextTwoThirdsOverVidOrImageWithLeftImageAndButton_shortcode');