<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_FullyHosted
// 'text-type'= 'dark'
// min-height=''
// bg-color=''
// bg-image=''
// title=''
// description=''
// button_label=''
// Button1_Link=''
// ]
function FullyHosted_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'min-height' => false,
		'bg-color'   => false,
		'bg-image'   => false,
	];

	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = ($props['min-height']) ? $props['min-height'] : '825px';

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
	$bg_img = ($props['bg-image']) ? $props['bg-image'] : false;

	if ($bg_color) {
		# code...
		$bg_color = "background-color: $bg_color;";
	}

	if ($bg_img) {
		# code...
		$bg_img = gp_get_file_url($bg_img);
		$bg_img = "background-image: url($bg_img);";

	}

	$bg_style = (!$bg_img) ? $bg_color : $bg_img;
	$min_height_style = "min-height:$min_height;";
	$style = "style='$bg_style $min_height_style'";

	$content = apply_filters('the_content', $content);
	$hero = "
    <div class='shortcode FullyHosted flex ai-center $text_type' $style>
      <div class='container'>
        <div class='row'>
          <div class='col s6 jc-center general-content'>
              $content
          </div>
        </div>
      </div>
    </div>";

	return $hero;
}

add_shortcode('gp_FullyHosted', 'FullyHosted_shortcode');