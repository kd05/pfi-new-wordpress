<?php

if (!defined('ABSPATH')) {
	exit;
} // Exit if accessed directly

// [gp_TestimonialsCPT
//   min-height= '910px'
//   bg-color= ''
//   title= ''
//   testimonials= ''
// ]
function TestimonialsCPT_shortcode($atts, $content = null)
{
	$defaults = [
		'min-height'   => '910px',
		'bg-color'     => false,
		'title'        => false,
		'testimonials' => false,
		'category'     => false,
	];
	$props = shortcode_atts($defaults, $atts);

	$min_height = $props['min-height'];
	$title = ($props['title']) ? $props['title'] : '';
	$testimonials = ($props['testimonials']) ? explode(',', $props['testimonials']) : false;
	$bg_color = ($props['bg-color']) ? $props['bg-color'] : 'white';

	$min_height_style = "min-height: $min_height;";
	$bg_color = "background-color: $bg_color;";

	$style = "style='$min_height_style $bg_color'";

	$left_url = get_template_directory_uri() . '/assets/images/left-arrow.png';
	$right_url = get_template_directory_uri() . '/assets/images/right-arrow.png';

	$cat = $props['category'] ? $props['category'] : '';

	$args = array(
		'post_type'      => 'testimonials',
		'post__in'       => $testimonials,
		'orderby'        => 'date',
		'post_status'    => 'publish',
		'posts_per_page' => -1,
	);
	if (!empty($cat)) {
		$args['tax_query'][] = array(
			'taxonomy' => 'testimonials_cat',
			'field'    => 'slug',
			'terms'    => $cat,
		);
	}
	$q = new WP_Query($args);

	$slides = '';
	$big_quote_src = get_template_directory_uri() . '/assets/images/big-quote.png';

	if ($q->have_posts()) {

		// Start looping over the query results.
		while ($q->have_posts()) {

			$q->the_post();

			$testimonial_id = get_the_ID();
			$profile_pic_url = get_field('profile_pic_url');
			$testimonial_url = !empty($profile_pic_url) ? $profile_pic_url : gp_get_img_url_from_post($testimonial_id);
			$testimonial_title = get_the_title();
			$post = get_post();
			$testimonial_excerpt = substr(strip_tags($post->post_content), 0, 300);
			$testimonial_date = get_the_date();

			$rating_html = '';
			$rating = get_field('star_rating');
			if ($rating && $rating > 0) {
				$rating_html .= '<div class="stars ratings">';
				foreach (range(1, 5) as $rate) {
					$active = $rating == $rate ? ' active' : '';
					$rating_html .= '<span class="star-' . $rate . $active . '"><i class="fa fa-star"></i></span>';
				}
				$rating_html .= '</div>';
			}

			$slides .= "
                <div class='testimonial'>
		            <div class='slide-inner'>
		            	<div class='col s4 overlay'></div>
		                <div class='image-icons'>
		                   <img src='$testimonial_url' class='img-icon'>
	                    </div>
	                    <div class='content'>
	                        <span class='quotes'>“</span>
	                        <p>$testimonial_excerpt</p>
	                    </div>
		                <div class='footer'>$rating_html<p class='text-white'>$testimonial_title, $testimonial_date</p></div>
					</div>
            	</div>
            ";
		}
		wp_reset_postdata();
	}


	$html = "
    <div class='shortcode TestimonialsCPT' $style>
      <div class='container'>
        <div class='row'>
          <div class='col s12 mb-5 general-content'>
            <h2 class='text-center scroll-animate'>$title</h2>
          </div>
          <div class='col s12'>
            <div class='testimonial-slider'>
              $slides
            </div>
            <div class='arrow slick-prev left-testimonial-arrow'>
              <span><i class='fa fa-chevron-left'></i></span>
            </div>
            <div class='arrow slick-next right-testimonial-arrow'>
              <span><i class='fa fa-chevron-right'></i></span>
            </div>
          </div>
        </div>
      </div>
    </div>";
	return $html;
}

add_shortcode('gp_TestimonialsCPT', 'TestimonialsCPT_shortcode');