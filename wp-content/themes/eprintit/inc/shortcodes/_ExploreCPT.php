<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_ExploreCPT
// text-type= 'dark'
// min-height=''
// title=''
// subtitle=''
// ]
function ExploreCPT_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'min-height' => false,
		'title'      => false,
		'subtitle'   => false,
		'category'   => false
	];

	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = ($props['min-height']) ? $props['min-height'] : '1300px';

	$title = ($props['title']) ? $props['title'] : '';
	$subtitle = ($props['subtitle']) ? $props['subtitle'] : '';
	$cat = $props['category'] ? $props['category'] : false;

	$args = array(
		'post_type'      => 'case-studies',
		'post_status'    => 'publish',
		'posts_per_page' => 4,
	);

	if ($cat) {
		$args['tax_query'][] = array(
			'taxonomy' => 'case_study_cat',
			'field'    => 'slug',
			'terms'    => $cat,
		);
	}
	$q = new WP_Query($args);

	$slides = '';
	$first_post = '';
	if ($q->have_posts()) {
		$i = 0;
		// Start looping over the query results.
		while ($q->have_posts()) {
			$q->the_post();
			$i++;

			$potential_id = get_the_ID();
			$potential_url = gp_get_img_url_from_post($potential_id);
			$potential_title = get_the_title();
			$content = get_the_excerpt();
			if (!empty($content)) {
				$content = substr($content, 0, 200) . '...';
			}
			$external_link = get_field('external_link');
			$potential_link = !$external_link || empty($external_link) ? get_permalink() : $external_link;
			$second_title = get_field('slogan');
			if ($i === 1) {
				$second_content = get_field('second_content', false, false);
				if (!empty($second_content)) {
					$second_content = substr($second_content, 0, 150);
					$second_content .= '...';
				}
				$first_post = "<div class='col s12 scroll-animate'>
		            <div class='card side bg-blue'>
		            	<a href='$potential_link' class='full-study-link' title='$potential_title'></a>
		              <div class='cimg' style='background-image: url($potential_url);'></div>
		              <div class='cbody'>
		                <p class='bold text-white'>$potential_title</p>
		                <p class='mb-1 text-white'>$second_title</p>
		                <p class='text-white'>$content</p>
		                <p class='text-white'>$second_content</p>
		              </div>
		            </div>
		          </div>";
			} else {
				$slides .= "<div class='card'>
					<a href='$potential_link' class='full-study-link' title='$potential_title'></a>
	              <div class='cimg' style='background-image: url($potential_url);'></div>
	              <div class='cbody'>
	                <p class='bold'>$potential_title</p>
	                <p class='mb-1'>$second_title</p>
	                <p>$content</p>
	              </div>
	            </div>";
			}

		}
		wp_reset_postdata();
	}

	$min_height_style = "min-height:$min_height;";
	$style = "style='$min_height_style'";

	$hero = "
    <div class='shortcode ExploreCPT scroll-animate flex ai-center $text_type' $style>
      <div class='container'>
        <div class='row jc-center'>
          <div class='col s12'>
            <h3 class='sub text-center scroll-animate'>$title</h3>
            <h2 class='main text-center bold scroll-animate'>$subtitle</h2>
          </div>
          $first_post
        </div>
        <div class='row jc-center'>
           $slides
        </div>
      </div>
    </div>";
	return $hero;
}

add_shortcode('gp_ExploreCPT', 'ExploreCPT_shortcode');