<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_AccreditationCPT
// text-type= 'dark'
// min-height=''
// bg-color=''
// ]
function AccreditationCPT_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'min-height' => false,
		'bg-color'   => false,
	];

	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = ($props['min-height']) ? $props['min-height'] : '586px';

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;

	if ($bg_color) {
		$bg_color = "background-color: $bg_color;";
	}

	$args = array(
		'post_type'      => 'accreditations',
		'post_status'    => 'publish',
		'posts_per_page' => -1,
	);
	$q = new WP_Query($args);
	$slides = '';

	if ($q->have_posts()) {

		// Start looping over the query results.
		while ($q->have_posts()) {

			$q->the_post();

			$potential_id = get_the_ID();
			$title = get_the_title();
			$potential_url = gp_get_img_url_from_post($potential_id);
			$url = get_field('link');
			if (!$url || empty($url)) {
				$url = '#';
			}
			$slides .= "<div class='logo'><a href='$url' target='_blank' title='$title'><img src='$potential_url' alt='' /></a></div>";
		}
		wp_reset_postdata();
	}

	$min_height_style = "min-height:$min_height;";

	$style = "style='$bg_color $min_height_style'";
	$title = __('Our Accreditation', 'wpBabywp');
	$subtitle = __('Certification & Awards', 'wpBabywp');

	$html = "
    <div class='shortcode v-padding AccreditationCPT flex ai-center $text_type' $style>
      <div class='container'>
        <div class='row'>
          <div class='col s12 text-center general-content'>
            <h3 class='mb-1 fw-400 scroll-animate'>$title</h3>
            <h2 class='mb-4 scroll-animate'>$subtitle</h2>
          </div>
          <div class='spacer type-70'></div>
          <div class='col'>
            <div class='accreditations-slider'>
                $slides
              </div>
          </div>
        </div>
      </div>
    </div>";
	return $html;
}

add_shortcode('gp_AccreditationCPT', 'AccreditationCPT_shortcode');