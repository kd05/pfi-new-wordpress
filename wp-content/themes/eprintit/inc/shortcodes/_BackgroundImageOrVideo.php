<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_BackgroundImageOrVideo
// text-type= 'dark'
// min-height=''
// bg-color=''
// bg-image=''
// bg-video=''
// button-label=''
// button-link=''
// button-type=''
// ]
function BackgroundImageOrVideo_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'min-height' => false,

		'bg-color' => false,
		'bg-image' => false,
		'bg-video' => false,

		'button-label' => false,
		'button-link'  => false,
		'button-type'  => 'red'
	];

	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = ($props['min-height']) ? $props['min-height'] : '220px';

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
	$bg_img = ($props['bg-image']) ? $props['bg-image'] : false;
	$bg_vid = ($props['bg-video']) ? $props['bg-video'] : false;

	$btn_text = ($props['button-label']) ? $props['button-label'] : false;
	$btn_link = ($props['button-link']) ? $props['button-link'] : false;
	$btn_type = ($props['button-type'] != 'red') ? 'white' : 'red';
	$btn = "";

	if ($bg_color) {
		$bg_color = "background-color: $bg_color;";
	}
	if ($bg_img) {
		$bg_img = gp_get_file_url($bg_img);
		$bg_img = "background-image: url( $bg_img );";
	}
	if ($bg_vid) {
		$bg_vid = "<video autoplay loop poster muted class='bg-video'><source src='$bg_vid' type='video/mp4'></video>";
	}

	if ($btn_type == 'red') {
		$btn = "<a href='$btn_link' class='btn bg-$btn_type text-white'>$btn_text</a>";
	} else {
		$btn = "<a href='$btn_link' class='btn bg-$btn_type text-grey'>$btn_text</a>";
	}

	$min_height_style = "min-height:$min_height;";

	$style = "style='$bg_color $bg_img $min_height_style'";

	$html = "
    <div class='shortcode BackgroundImageOrVideo flex ai-center scroll-animate $text_type' $style>
      $bg_vid
      <div class='container-fluid'>
        <div class='row'>
          <div class='col s12 text-center general-content scroll-animate'>
            <div class='btn-container'>$btn</div>
          </div>
        </div>
      </div>
    </div>";
	return $html;
}

add_shortcode('gp_BackgroundImageOrVideo', 'BackgroundImageOrVideo_shortcode');