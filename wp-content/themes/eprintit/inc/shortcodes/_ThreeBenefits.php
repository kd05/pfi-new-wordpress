<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly


// [gp_ThreeBenefits
// title='Live, work and play on-the-go with ePRINTit™!'
// subtitle='Access and print ANY file you want, WHEN you want, WHERE you want…'
//
// ben1_title=''
// ben1_desc=''
// ben1_link=''
//
// ben2_title=''
// ben2_desc=''
// ben2_link=''
//
// ben3_title=''
// ben3_desc=''
// ben3_link=''
// ]
function ThreeBenefits_shortcode($atts, $content = null)
{
	$defaults = [
		'min-height' => 780,

		'title'    => false,
		'subtitle' => false,

		'ben1_title'    => false,
		'ben1_desc'     => false,
		'ben1_link'     => false,
		'button1_label' => 'Read More',

		'ben2_title'    => false,
		'ben2_desc'     => false,
		'ben2_link'     => false,
		'button2_label' => 'Read More',

		'ben3_title'    => false,
		'ben3_desc'     => false,
		'ben3_link'     => false,
		'button3_label' => 'Read More',
		'class'         => ''
	];

	$props = shortcode_atts($defaults, $atts);

	$title = ($props['title']) ? $props['title'] : '';
	$subtitle = ($props['subtitle']) ? $props['subtitle'] : '';

	$ben1_title = ($props['ben1_title']) ? $props['ben1_title'] : '';
	$ben1_desc = ($props['ben1_desc']) ? $props['ben1_desc'] : '';
	$ben1_link = ($props['ben1_link']) ? $props['ben1_link'] : '';

	$ben2_title = ($props['ben2_title']) ? $props['ben2_title'] : '';
	$ben2_desc = ($props['ben2_desc']) ? $props['ben2_desc'] : '';
	$ben2_link = ($props['ben2_link']) ? $props['ben2_link'] : '';

	$ben3_title = ($props['ben3_title']) ? $props['ben3_title'] : '';
	$ben3_desc = ($props['ben3_desc']) ? $props['ben3_desc'] : '';
	$ben3_link = ($props['ben3_link']) ? $props['ben3_link'] : '';
	$button1_label = $props['button1_label'];
	$button2_label = $props['button2_label'];
	$button3_label = $props['button3_label'];

	$btn1_link = $ben1_link ? "<a href='$ben1_link' class='btn bg-blue text-white'>$button1_label</a>" : "";
	$btn2_link = $ben2_link ? "<a href='$ben2_link' class='btn bg-blue text-white'>$button2_label</a>" : "";
	$btn3_link = $ben3_link ? "<a href='$ben3_link' class='btn bg-blue text-white'>$button3_label</a>" : "";

	$min_height_style = gp_min_height_from_shortcode_props($props);
	$style = "style='$min_height_style'";
	$classes = !empty($props['class']) ? $props['class'] : '';

	$title_html = '';
	if (!empty($title) && !empty($subtitle)) {
		$title_html = "
          <div class='col s12 title'>
            <h2 class='scroll-animate'>$title</h2>
            <h3 class='scroll-animate'>$subtitle</h3>
          </div>";
	}

	$html = "
    <div class='shortcode ThreeBenefits $classes' $style>
      <div class='container'>
        <div class='row'>
        $title_html
            <div class='card scroll-animate'>
              <div class='cheader'>
                <h3 class=''>$ben1_title</h3>
              </div>
              <div class='cbody general-content'>
                <p class=''>$ben1_desc</p>
              </div>
              <div class='cfooter'>
                <div class='btn-container'>
                    $btn1_link
                </div>
              </div>
            </div>
            <div class='card scroll-animate'>
              <div class='cheader'>
                <h3 class=''>$ben2_title</h3>
              </div>
              <div class='cbody'>
                <p class=''>$ben2_desc</p>
              </div>
              <div class='cfooter'>
                <div class='btn-container'>
                    $btn2_link
                </div>
              </div>
            </div>
            <div class='card scroll-animate'>
              <div class='cheader'>
                <h3 class=''>$ben3_title</h3>
              </div>
              <div class='cbody'>
                <p class=''>$ben3_desc</p>
              </div>
              <div class='cfooter'>
                <div class='btn-container'>
                    $btn3_link
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  ";
	return $html;
}

add_shortcode('gp_ThreeBenefits', 'ThreeBenefits_shortcode');
