<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

function gp_video_popup_section_shortcode($atts, $content = null)
{

    $defaults = [
        'id' => '',

        'bg-color' => false,
        'bg-img' => '5585',
        'min-height' => '',
        'text-type' => 'light',

        'title'      => 'Digital Transformation Starts With Viewing Solutions Differently',
        'youtube_id' => '#',
        'youtube_text' => 'SEE OUR END TO END SOLUTION',
        'navigate_next_panel_text' => '',
        'navigate_next_panel_id' => '',
        'scroll_name' => ""
    ];
    $props = shortcode_atts($defaults, $atts);

    $text_type = trim($props['text-type']) == "" ? "light" : "dark";
    $id = !empty($props['id']) ? $props['id'] : "";

    $title = !empty($props['title']) ? $props['title'] : "";
    $youtube_id = $props['youtube_id'];
    $youtube_text = $props['youtube_text'];

    $youtube_text = !empty($youtube_id) ?
        "<a href='javascript:void(0)' class='popup-video'  data-video-id='$youtube_id'>$youtube_text</a>" : "";

    $navigate_next_panel_id = !empty($props['navigate_next_panel_id']) ? $props['navigate_next_panel_id'] : "";
    $navigate_next_panel_text = !empty($props['navigate_next_panel_text']) ?
        '<div class="next-panel-animate">
              <a href="javascript:void(0)"  data-animate="'.$navigate_next_panel_id.'">'.$props['navigate_next_panel_text'].'</a>
         </div>' : "";
    $scroll_name = !empty($props['scroll_name']) ? $props['scroll_name'] : "";


    $bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
    $bg_img = ($props['bg-img']) ? $props['bg-img'] : false;

    if ($bg_color) {
        $bg_color = "background-color: $bg_color;";
    }

    if ($bg_img) {
        $bg_img = gp_get_file_url($bg_img);
        $bg_img = "background-image: url($bg_img);";
    }

    $bg_style = (!$bg_img) ? $bg_color : $bg_img;

    $min_height = $props['min-height'];
    $min_height_style = "min-height:$min_height;";

    $style = "style='$bg_style $min_height_style'";

    $html = "
        <div class='shortcode pfi-shortcode VideoPopupSection $text_type' id='$id' scroll-name='$scroll_name' $style>
            <div class='grey-overlay'></div>
            <div class='container-1220 flex-center pfi-shortcode-inner'>
                <div class='pfi-shortcode-content scroll-animate'>
                    <h1>$title</h1>
                    $youtube_text
                </div>
                $navigate_next_panel_text
            </div>
        </div>
    ";

    if($youtube_id != ""){
        $html .= "
            <style>
                .VideoPopupSection .popup-video:after{  background: url('".get_template_directory_uri()."/assets/images/play-icon.png'); }
            </style>
        ";
    }

    return $html;
}
add_shortcode('gp_VideoPopupSection', 'gp_video_popup_section_shortcode');