<?php
if (!defined('ABSPATH')) exit; // Exit if accessed directly

function gp_LeftImageWithIcons_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'     => 'dark',
		'min-height'    => '625px',
		'bg-color'      => false,
		'bg-img'        => false,
		'image'         => 4371,
		'icon1-img'     => 4361,
		'icon1-title'   => '',
		'icon1-content' => '',
		'icon2-img'     => 4369,
		'icon2-title'   => '',
		'icon2-content' => '',
		'icon3-img'     => '',
		'icon3-title'   => '',
		'icon3-content' => '',
		'icon4-img'     => '',
		'icon4-title'   => '',
		'icon4-content' => '',
		'icon5-img'     => '',
		'icon5-title'   => '',
		'icon5-content' => '',
		'icon6-img'     => '',
		'icon6-title'   => '',
		'icon6-content' => '',
	];
	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = $props['min-height'] ? $props['min-height'] : '';

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
	$bg_img = ($props['bg-image']) ? $props['bg-image'] : false;

	if ($bg_color) {
		$bg_color = "background-color: $bg_color;";
	}

	if ($bg_img) {
		$bg_img = gp_get_file_url($bg_img);
		$bg_img = "background-image: url($bg_img);";
	}

	$image = gp_get_file_url($props['image']);

	// Check for icons
	$icons_class = '';
	$icons = '<div class="icon-wrapper">';
	for ($i = 1; $i <= 6; $i++) {
		if (!empty($props['icon' . $i . '-img']) && !empty($props['icon' . $i . '-title']) && !empty($props['icon' . $i . '-content'])) {
			$img_src = gp_get_file_url($props['icon' . $i . '-img']);
			$icons .= '<div class="unit">';
			$icons .= '<img src="' . $img_src . '" alt="Icon" />';
			$icons .= '<h3 class="title">' . $props['icon' . $i . '-title'] . '</h3>';
			$icons .= '<p>' . $props['icon' . $i . '-content'] . '</p>';
			$icons .= '</div>';
			if ($i == 6) {
				$icons_class = ' wide';
			}
		}
	}
	$icons .= '</div>';

	$bg_style = (!$bg_img) ? $bg_color : $bg_img;
	$min_height_style = !empty($min_height) ? "min-height:$min_height;" : '';
	$style = "style='$bg_style $min_height_style'";
	$content = apply_filters('the_content', $content);

	$html = "
    <div class='shortcode LeftImageWithIcons $text_type$icons_class' $style>
    	<div class='container'>
    		<div class='row'>
    			<div class='col s12'>
    				<div class='content general-content'>
    					<div class='content-wrapper'>
    						<div class='content-image' style='background-image: url($image)'></div>
    						<div class='content-text'>$content$icons</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>";

	return $html;
}

add_shortcode('gp_LeftImageWithIcons', 'gp_LeftImageWithIcons_shortcode');