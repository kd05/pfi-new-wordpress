<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly


function gp_news_blog_section_shortcode($atts, $content = null)
{
    $defaults = [
        'id'  => '',

        'bg-color' => false,
        'bg-img' => '',
        'min-height' => '',
        'text-type' => 'light',

        'title'      => '',
        'link_text'  => '',
        'link_url'  => '',
        'scroll_name' => ""
    ];

    $props = shortcode_atts($defaults, $atts);

    $text_type = trim($props['text-type']) == "" ? "light" : "dark";

    $id = !empty($props['id']) ? $props['id'] : "";
    $title = !empty($props['title']) ? $props['title'] : "";
    $link_url = !empty($props['link_url']) ? $props['link_url'] : "";

    $link_text = !empty($props['link_text']) ?
        '<div class="view-news">
                        <a href="'.$link_url.'">'.$props['link_text'].'</a>
                    </div>' : "";
    $scroll_name = !empty($props['scroll_name']) ? $props['scroll_name'] : "";


    $bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
    $bg_img = ($props['bg-img']) ? $props['bg-img'] : false;

    if ($bg_color) {
        $bg_color = "background-color: $bg_color;";
    }
    if ($bg_img) {
        $bg_img = gp_get_file_url($bg_img);
        $bg_img = "background-image: url($bg_img);";
    }

    $bg_style = (!$bg_img) ? $bg_color : $bg_img;
    $min_height = $props['min-height'];
    $min_height_style = "min-height:$min_height;";

    $style = "style='$bg_style $min_height_style'";




    $args = array(
        'numberposts' => 3,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'suppress_filters' => false,
        'post_type' => 'post'
    );
    $recent_news = wp_get_recent_posts( $args);
    $blog_html = "";

    foreach($recent_news as $news){
        $news_id = $news['ID'];
        $news_title = $news['post_title'];
        $news_excerpt = gp_excerptize($news['post_content'], 16);
        $news_featured_img = get_the_post_thumbnail_url($news_id, "large");
        $news_link = get_the_permalink($news_id);
//        echo '<pre>' . print_r( $news_featured_img, true ) . '</pre>';
        $blog_html .= "
            <div class='single-news'>
                <div class='news-img'  style=' background-image: url($news_featured_img)'>
                    <div class='grey-overlay'></div>
                    <a href='$news_link' class='green-overlay'><span>READ MORE</span></a>                               
                </div>
                <div class='news-content'>
                    <a href='$news_link'><h6>$news_title</h6></a>
                    <p>$news_excerpt</p>
                </div>                
            </div>
        ";
    }

    $html = "
        <div class='shortcode pfi-shortcode NewsBlogSection $text_type'  scroll-name='$scroll_name'  id='$id' $style>
             <div class='grey-overlay'></div>
            <div class='container-1220 flex-center pfi-shortcode-inner'>
            
                <div class='news-wrapper'>
                    <div class='news-title'>
                        <h1>$title</h1>
                    </div>                    
                    <div class='news-container'>
                        $blog_html
                    </div>
                    $link_text
                </div>
                                                            
            </div>
        </div>            
    ";
    return $html;

}

add_shortcode('gp_NewsBlogSection', 'gp_news_blog_section_shortcode');