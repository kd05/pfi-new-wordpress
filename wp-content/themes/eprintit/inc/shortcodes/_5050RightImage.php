<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_5050RightImage
// text-type= 'dark'
// min-height=''
// bg-color=''
// bg-image=''
// title=''
// description=''
// quote=''
// button_label=''
// Button1_Link=''
// ]
function RightImage_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'min-height' => '450px',
		'bg-color'   => false,
		'image'      => false,
		'centermode' => false,
	];

	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = $props['min-height'];

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
	$img = ($props['image']) ? $props['image'] : false;
	$centermode = $props['centermode'] ? true : false;
	$imageColClass = $centermode ? " centered" : "";

	if ($bg_color) {
		# code...
		$bg_color = "background-color: $bg_color;";
	}

	if ($img) {
		# code...
		$img = gp_get_file_url($img);
	}

	$bg_style = $bg_color;
	$min_height_style = "min-height:$min_height;";

	$style = "style='$bg_style $min_height_style'";
	$content = do_shortcode($content);

	$hero = "
    <div class='shortcode RightImage $text_type' $style>
      <div class='lg-container'>
        <div class='row ai-center jc-between'>
          <div class='col scroll-animate img-col$imageColClass'>              
            <img src='$img' />
          </div>
          <div class='col content-col'>
            <div class='content general-content'>
              $content
            </div>
          </div>
        </div>
      </div>
    </div>
  	";
	return $hero;
}

add_shortcode('gp_5050RightImage', 'RightImage_shortcode');