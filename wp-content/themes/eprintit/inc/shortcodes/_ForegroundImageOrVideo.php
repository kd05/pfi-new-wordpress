<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_ForegroundImageOrVideo
//   min-height= ''
//   bg-color= ''
//   media= ''
// ]
function ForegroundImageOrVideo_shortcode($atts, $content = null)
{
	$defaults = [
		'min-height' => '600px',
		'bg-color'   => false,
		'media'      => false,
		'title'      => '',
		'text-type'  => 'dark',
		'btn-text'   => '',
		'btn-url'    => '',
		'btn-color'  => 'blue',
	];

	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = $props['min-height'];
	$bg_color = ($props['bg-color']) ? $props['bg-color'] : 'white';
	$media = ($props['media']) ? $props['media'] : false;
	$title = $props['title'];
	$btn_text = $props['btn-text'];
	$btn_url = $props['btn-url'];
	$btn_color = $props['btn-color'];

	$media = gp_get_file_url($media);
	$file_type = strtolower(substr(strrchr($media, "."), 1));

	$media = ($file_type == 'jpg' || $file_type == 'png') ? "<div class='img-container scroll-animate' data-delay='300'><img src='$media'></div>" : "<video autoplay loop poster muted class=''><source src='$media' type='video/mp4'></video>";

	$min_height_style = "min-height: $min_height;";
	$bg_color = "background-color:$bg_color;";

	$content = apply_filters('the_content', $content);
	$style = "style='$min_height_style $bg_color'";

	$btn = '';
	if (!empty($btn_text) && !empty($btn_url)) {
		$btn_text_color = 'grey';
		if ($btn_color != 'white') {
			$btn_text_color = 'white';
		}
		$btn = "<a href='$btn_url' class='btn bg-$btn_color text-$btn_text_color'>$btn_text</a>";
	}

	$html = "
    <div class='shortcode ForegroundImageOrVideo flex ai-center $text_type' $style>
    	<div class='container'>
    		<div class='row'>
    			<div class='col s6'>
    				<div class='content general-content scroll-animate'>
    					<h2 class=''>$title</h2>
    					<div class='mb-5'>$content</div>
    					<div class='btn-container'>$btn</div>
                    </div>
                </div>
                <div class='col s6'>$media</div>
            </div>
        </div>
    </div>";

	return $html;
}

add_shortcode('gp_ForegroundImageOrVideo', 'ForegroundImageOrVideo_shortcode');
