<?php
/**
 * PlatformApplications Shortcode
 */
if (!defined('ABSPATH')) exit; // Exit if accessed directly

function gp_PlatformApplications_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'    => 'dark',
		'min-height'   => false,
		'bg-color'     => false,
		'bg-image'     => false,
		'app1-img'     => 4335,
		'app1-title'   => '',
		'app1-content' => '',
		'app1-link'    => '',
		'app2-img'     => 4337,
		'app2-title'   => '',
		'app2-content' => '',
		'app2-link'    => '',
		'app3-img'     => '',
		'app3-title'   => '',
		'app3-content' => '',
		'app3-link'    => '',
		'app4-img'     => '',
		'app4-title'   => '',
		'app4-content' => '',
		'app4-link'    => '',
        'app5-img'     => '',
        'app5-title'   => '',
        'app5-content' => '',
        'app5-link'    => '',
        'app6-img'     => '',
        'app6-title'   => '',
        'app6-content' => '',
        'app6-link'    => ''
	];

	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = $props['min-height'] ? $props['min-height'] : '';

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
	$bg_img = ($props['bg-image']) ? $props['bg-image'] : false;

	if ($bg_color) {
		$bg_color = "background-color: $bg_color;";
	}

	if ($bg_img) {
		$bg_img = gp_get_file_url($bg_img);
		$bg_img = "background-image: url($bg_img);";

	}

	// Check for icons
	$units = '';
	$count = 0;
	for ($i = 1; $i <= 6; $i++) {
        if(!isset($props['app'.$i.'-img']) || !isset($props['app'.$i.'-title']) || !isset($props['app'.$i.'-content'])) {
            continue;
        }
		if (!empty($props['app' . $i . '-img']) && !empty($props['app' . $i . '-title']) && !empty($props['app' . $i . '-content'])) {
			$img_src = gp_get_file_url($props['app' . $i . '-img']);
            $units .= '<div class="unit">';
            $units .= '<div class="text"><h3>' . $props['app' . $i . '-title'] . '</h3><div class="hidden"><p>' . $props['app' . $i . '-content'] . '</p></div></div>';
            $units .= '<div class="bg-img" style="background-image: url(' . $img_src . ');"></div>';
            if(!empty($props['app'.$i.'-link'])) {
                $units .= '<div><a href="' . $props['app' . $i . '-link'] . '" title="" class="unit-anchor"></a></div>';
            }
            $units .= '</div>';
            $count++;
		}
	}
    $counter_class = $count % 3 === 0 ? " triple-wrapper" : $count == 2 ? " dual-wrapper" : "";
    $icons = '<div class="unit-wrapper'.$counter_class.'">';
	$icons .= $units;
    $icons .= '</div>';



    $bg_style = (!$bg_img) ? $bg_color : $bg_img;
	$min_height_style = !empty($min_height) ? "min-height:$min_height;" : '';
	$style = "style='$bg_style $min_height_style'";

	$content = apply_filters('the_content', $content);
	$hero = "
    <div class='shortcode PlatformApplicationsPanel flex $text_type' $style>
      <div class='container'>
        <div class='row'>
          <div class='col'>
              <div class='content general-content'>$content$icons</div>
          </div>
        </div>
      </div>
    </div>";

	return $hero;
}

add_shortcode('gp_PlatformApplications', 'gp_PlatformApplications_shortcode');