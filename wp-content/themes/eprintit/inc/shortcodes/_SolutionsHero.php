<?php
if (!defined('ABSPATH')) exit; // Exit if accessed directly

function SolutionsHero_shortcode($atts, $content = null)
{
    $defaults = [
        'text-type' => 'light',
        'min-height' => false,
        'bg-color' => false,
        'bg-img' => 4331,
        'button1_label' => '',
        'button1_url' => '',
        'button1_target' => '_blank',
        'button1_lightbox' => false,
        'button2_label' => '',
        'button2_url' => '',
        'button2_target' => '_blank',
        'button2_lightbox' => false,
    ];

    $props = shortcode_atts($defaults, $atts);

    $text_type = $props['text-type'];
    $min_height = ($props['min-height']) ? $props['min-height'] : '590px';

    $bg_color = ($props['bg-color']) ? $props['bg-color'] : 'bg-color';
    $bg_img = ($props['bg-img']) ? $props['bg-img'] : '';

    $bg_img_src = !empty($bg_img) ? gp_get_file_url($bg_img) : '';
    $bg_color = !empty($bg_color) ? "background-color: $bg_color;" : "";
    $min_height_style = "min-height:$min_height;";

    $bg_img = !empty($bg_img) ? "<div class='bg-img' style='background-image: url($bg_img_src);'></div>" : '';

    $style = "style='$min_height_style $bg_color'";

    $button1_label = !empty($props['button1_label']) ? $props['button1_label'] : "";
    $button1_url = !empty($props['button1_url']) ? $props['button1_url'] : "";
    $button1_target = !empty($props['button1_target']) ? $props['button1_target'] : "";
    $button2_label = !empty($props['button2_label']) ? $props['button2_label'] : "";
    $button2_url = !empty($props['button2_url']) ? $props['button2_url'] : "";
    $button2_target = !empty($props['button2_target']) ? $props['button2_target'] : "";

    $btns = "";
    if (!empty($button1_label) || !empty($button2_label)) {
        $btns = '<div class="btn-container">';
        if( !empty($button1_label) ) {
            $button1_attr = $props['button1_lightbox'] ? " data-fancybox='video' " : '';
            $btns .= "<a href='$button1_url' title='' target='$button1_target'$button1_attr class='btn'>$button1_label</a>";
        }
        if( !empty($button2_label) ) {
            $button2_attr = $props['button2_lightbox'] ? " data-fancybox='video' " : '';
            $btns .= "<a href='$button2_url' title='' target='$button2_target'$button2_attr class='btn'>$button2_label</a>";
        }
        $btns .= '</div>';
    }

    $_content = apply_filters('the_content', $content);

    $html = "
    <div class='shortcode SolutionsHero flex ai-center $text_type' $style>
    	<div class='container'>
	        <div class='row'>
		        <div class='col s12'>
		        	<div class='content'>$_content$btns</div>
		        </div>
	        </div>
    	</div>
    	$bg_img
    </div>";

    return $html;
}

add_shortcode('gp_SolutionsHero', 'SolutionsHero_shortcode');