<?php
if (!defined('ABSPATH')) exit; // Exit if accessed directly

function gp_FooterForm_shortcode($atts, $content = null)
{
    $defaults = [
        'text-type' => 'light',
        'text-align' => 'center',
        'min-height' => '600px',
        'bg-color' => false,
        'bg-img' => '4349',
    ];

    $props = shortcode_atts($defaults, $atts);

    $text_type = $props['text-type'];
    $min_height = $props['min-height'];

    $bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
    $bg_img = ($props['bg-img']) ? $props['bg-img'] : false;

    if ($bg_color) {
        $bg_color = "background-color: $bg_color;";
    }

    if ($bg_img) {
        $bg_img = gp_get_file_url($bg_img);
        $bg_img = "background-image: url($bg_img);";
    }

    $bg_style = (!$bg_img) ? $bg_color : $bg_img;
    $min_height_style = "min-height:$min_height;";

    $style = "style='$bg_style $min_height_style'";
    $content = apply_filters('the_content', $content);
    $text_align = 'text-' . $props['text-align'];

    $hero = "
    <div class='shortcode FooterForm flex scroll-animate ai-center $text_type' $style>
      <div class='container'>
        <div class='row'>
          <div class='col s12'>
            <div class='content general-content scroll-animate $text_align'>
              $content
            </div>
          </div>
        </div>
      </div>
    </div>
  	";
    return $hero;
}

add_shortcode('gp_FooterForm', 'gp_FooterForm_shortcode');