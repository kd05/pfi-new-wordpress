<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_5050LeftImage
// 'text-type'= 'dark'
// min-height=''
// bg-color=''
// bg-image=''
// title=''
// description=''
// quote=''
// button_label=''
// Button1_Link=''
// ]
function LeftImage_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'min-height' => '450px',
		'bg-color'   => 'white',
		'image'      => false,
		'centermode' => false,
	];

	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = $props['min-height'];

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : '';
	$img = ($props['image']) ? $props['image'] : false;

	$centermode = $props['centermode'] ? true : false;
	$imageColClass = $centermode ? " centered" : "";

	if ($bg_color) {
		$bg_color = "background-color: $bg_color;";
	}

	if ($img) {
		# code...
		$img = gp_get_file_url($img);
	}

	$min_height_style = "min-height:$min_height;";

	$style = "style='$bg_color$min_height_style'";
	$content = apply_filters('the_content', $content);

	$hero = "
    <div class='shortcode LeftImage $text_type' $style>
      <div class='lg-container'>
        <div class='row ai-center jc-between'>
          <div class='col img-col $imageColClass'>
            <img src='$img' />
          </div>
          <div class='col content-col'>
            <div class='content general-content'>
              $content
            </div>
          </div>
        </div>
      </div>
    </div>
  	";
	return $hero;
}

add_shortcode('gp_5050LeftImage', 'LeftImage_shortcode');