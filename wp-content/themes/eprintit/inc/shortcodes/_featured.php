<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly


// [gp_featured
// min-height='800px'
// bg-color='#000'
// ]
function featured_shortcode($atts, $content = null)
{
	$defaults = [
		'min-height' => '780px',
		'bg-color'   => '#1F1F1F',
		'category'   => false,
		'title'      => 'Features',
		'subtitle'   => ''
	];

	$props = shortcode_atts($defaults, $atts);

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : '';
	$cat = $props['category'] ? $props['category'] : 'education';

	$bg_color_style = "background-color:$bg_color;";

	$left_url = get_template_directory_uri() . '/assets/images/left-arrow.png';
	$right_url = get_template_directory_uri() . '/assets/images/right-arrow.png';

	$min_height_style = gp_min_height_from_shortcode_props($props);

	$style = "style='$bg_color_style $min_height_style'";

	$slides = "";

	$args = array(
		'post_type'      => 'features',
		'post_status'    => 'published',
		'orderby'        => 'menu_order',
		'order'          => 'DESC',
		'posts_per_page' => -1,
		'tax_query'      => [
			[
				'taxonomy' => 'features_cat',
				'field'    => 'slug',
				'terms'    => $cat,
			],
		],
	);

	$q = new WP_Query($args);

	if ($q->have_posts()) {

		// Start looping over the query results.
		while ($q->have_posts()) {

			$q->the_post();

			$id = get_the_ID();
			$url = gp_get_img_url_from_post($id);
			$title = get_the_title();
			$excerpt = get_the_excerpt();
			$custom_link = get_field('link');
			$link = !empty($custom_link) ? $custom_link : get_permalink();
			$target = !empty($custom_link) ? 'target="_blank"' : '';
			$button_text = __("Learn More", 'wpBabywp');
			$slides .= "
              <div class='slide'>
                <div class='card' $target style='background-image:url($url)'>
                  <div class='overlay'></div>
                  <div class='cheader text-center'>
                    <h3 class='text-white'>$title</h3>
                  </div>
                  <div class='cbody'>
                    <div class='text text-center'>
                      <p class='text-white'>$excerpt</p>
                      <div class='btn-container'><span class='btn bg-grey text-white full-width'>$button_text</span></div>
                    </div>
                  </div>
                  <div class='clink'><a href='$link' class='slide-link'></a></div>
                </div>
              </div>";
		}
		wp_reset_postdata();
	}

	$title = $props['title'];
	$subtitle = !empty($props['subtitle']) ? sprintf("<h3 class='subtitle scroll-animate'>%s</h3>", $props['subtitle']) : "";

	$hero = "
    <div class='shortcode featured' $style>

      <div class='lg-container'>

        <div class='row'>

          <div class='col s12 text-center'>
            <h2 class='title scroll-animate'>$title</h2>
            $subtitle
          </div>

          <div class='col s12 slider-container'>

            <div class='arrow left-arrow text-gray'>
            	<span><i class='fa fa-chevron-left'></i></span>
            </div>
            <div class='featured-slider'>$slides</div>
            <div class='arrow right-arrow text-gray'>
            	<span><i class='fa fa-chevron-right'></i></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  ";

	return $hero;
}

add_shortcode('gp_featured', 'featured_shortcode');