<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_LeftTextRight1FieldForm
// text-type= 'light'
// min-height=''
// bg-color=''
// ]

function LeftTextRight1FieldForm_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'min-height' => false,
		'bg-color'   => false,
		'bg-img'     => false,
//		'action'     => 'https://go.pardot.com/l/593411/2018-09-13/dkrqx',
//		'method'     => 'POST',
		'text'       => false,
		'path'       => false,
		'wpforms_id' => 4110,

	];

	$props = shortcode_atts($defaults, $atts);
//	$props['action'] = '#';

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = ($props['min-height']) ? $props['min-height'] : '117px';

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;
	$bg_img = ($props['bg-img']) ? $props['bg-img'] : false;

	if ($bg_color) {
		$bg_color = "background-color: $bg_color;";
	}

	if ($bg_img) {
		$bg_img = gp_get_file_url($bg_img);
		$bg_img = "background-image: url($bg_img);";
	}

	$min_height_style = "min-height:$min_height;";
	$style = "style='$bg_color $bg_img $min_height_style'";

	// Get any error messages for this form
	$errormsg = "";
	$name = "";
	$email = "";
	if (isset($_GET['NAME']) && isset($_GET['EMAIL']) && isset($_GET['errorMessage']) && isset($_GET['errors'])) {
		$errormsg = '<div class="row error-message"><div class="col">' . str_replace('~', '', sanitize_text_field($_GET['errorMessage'])) . '</div></div>';
		$name = sanitize_text_field($_GET['NAME']);
		$email = sanitize_text_field($_GET['EMAIL']);
	}
	$form = "<form action='" . $props['action'] . "' method='" . $props['method'] . "' class='siteFormCheck'>
              <div class='input-group break'>
                <input type='text' name='NAME' class='input' placeholder='Enter your name' value='$name' required>
                <input type='email' name='EMAIL' class='input' placeholder='Enter your business email' value='$email' required>
                <span class='question-box'></span><input type='text' name='qans' class='input input-qans' placeholder='-' required/>
                <div class='right'>
                    <button type='submit' class='btn text-white bg-blue'>Request Info</button>
                </div>
              </div>

            </form>";

	$wpform_id = $props['wpforms_id'];
	$form = do_shortcode('[wpforms id="'.$wpform_id.'"]');

	$panel_text = __('Request more information', 'wpBabywp');

	$html = "
    <div class='shortcode LeftTextRight1FieldForm flex ai-center $text_type' $style>

      <div class='container'>
      	$errormsg
        <div class='row ai-center jc-between'>
          <div class='col s3'>
            <h3 class='bold text-white'>$panel_text</h3>
          </div>
          <div class='col s8'>
            $form
          </div>
        </div>
      </div>
    </div>
  ";


	return $html;
}

add_shortcode('gp_LeftTextRight1FieldForm', 'LeftTextRight1FieldForm_shortcode');
