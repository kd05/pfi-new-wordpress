<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

function gp_about_banner_panel_shortcode($atts, $content = null)
{
    $defaults = [
        'id'  => '',

        'bg-img' => '',
        'min-height' => '',
        "font-color" => "",

        'title'      => 'Digital Transformation Starts With Viewing Solutions Differently',
        'navigate_next_panel_text' => '',
        'navigate_next_panel_id' => ''
    ];
    $props = shortcode_atts($defaults, $atts);

    $id = !empty($props['id']) ? $props['id'] : "";

    $title = !empty($props['title']) ? $props['title'] : "";
    $font_color = trim($props['font-color']) != "" ? $props['font-color'] : false;
    if($font_color){
        $font_color = " style = 'color : $font_color ' ";
    }

    $min_height = $props['min-height'];
    $min_height_style = "min-height:$min_height;";

    $bg_img = ($props['bg-img']) ? $props['bg-img'] : false;
    if ($bg_img) {
        $bg_img = gp_get_file_url($bg_img);
        $bg_img = "background-image: url($bg_img);";
    }
    $bg_style =  $bg_img;
    $style = "style='$bg_style $min_height_style'";


    $navigate_next_panel_id = !empty($props['navigate_next_panel_id']) ? $props['navigate_next_panel_id'] : "";
    $navigate_next_panel_text = !empty($props['navigate_next_panel_text']) ?
        '<div class="next-panel-animate">
              <a href="javascript:void(0)" '.$font_color.' data-animate="'.$navigate_next_panel_id.'">'.$props['navigate_next_panel_text'].'</a>
         </div>' : "";


    $html = "
        <div class='shortcode pfi-shortcode AboutBannerPanel'  id='$id'  $style> 
                <div class='about-banner-content flex-center'>
                    <div class='ab-left'>
                        <h1 $font_color >$title</h1>
                        $navigate_next_panel_text
                    </div>                     
                </div>     
        </div>            
    ";
    return $html;

}
add_shortcode('gp_AboutBannerPanel', 'gp_about_banner_panel_shortcode');