<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_Solutions
// bg-color=''
// bg-image=''
// text-type='dark'

// title=''
// subtitle=''

// sol1_title=''
// sol1_subtitle=''
// sol1_link=''

// sol2_title=''
// sol2_subtitle=''
// sol2_link=''

// sol3_title=''
// sol3_subtitle=''
// sol3_link=''
// ]
function Solutions_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'light',
		'min-height' => false,

		'bg-color' => false,
		'bg-image' => false,

		'title'    => false,
		'subtitle' => false,

		'sol1_image'    => '',
		'sol1_title'    => false,
		'sol1_subtitle' => false,
		'sol1_link'     => false,

		'sol2_image'    => '',
		'sol2_title'    => false,
		'sol2_subtitle' => false,
		'sol2_link'     => false,

		'sol3_image'    => '',
		'sol3_title'    => false,
		'sol3_subtitle' => false,
		'sol3_link'     => false,
	];

	$props = shortcode_atts($defaults, $atts);

	$text_type = $props['text-type'];
	$min_height = ($props['min-height']) ? $props['min-height'] : '780px';

	$bg_color = ($props['bg-color']) ? $props['bg-color'] : 'bg-color';
	$bg_img = ($props['bg-image']) ? $props['bg-image'] : '';

	$title = ($props['title']) ? $props['title'] : '';
	$subtitle = ($props['subtitle']) ? $props['subtitle'] : '';

	$sol1_title = ($props['sol1_title']) ? $props['sol1_title'] : '';
	$sol1_subtitle = ($props['sol1_subtitle']) ? $props['sol1_subtitle'] : '';
	$sol1_link = ($props['sol1_link']) ? $props['sol1_link'] : '';

	$sol2_title = ($props['sol2_title']) ? $props['sol2_title'] : '';
	$sol2_subtitle = ($props['sol2_subtitle']) ? $props['sol2_subtitle'] : '';
	$sol2_link = ($props['sol2_link']) ? $props['sol2_link'] : '';

	$sol3_title = ($props['sol3_title']) ? $props['sol3_title'] : '';
	$sol3_subtitle = ($props['sol3_subtitle']) ? $props['sol3_subtitle'] : '';
	$sol3_link = ($props['sol3_link']) ? $props['sol3_link'] : '';


	$bg_img = !empty($bg_img) ? gp_get_file_url($bg_img) : '';
	$bg_img = !empty($bg_img) ? "background-image: url($bg_img);" : "";
	$bg_color = "background-color: $bg_color;";
	$min_height_style = "min-height:$min_height;";

	$style = "style='$min_height_style $bg_color $bg_img'";

	$sol1_image = !empty($props['sol1_image']) ? $props['sol1_image'] : false;
	$sol2_image = !empty($props['sol2_image']) ? $props['sol2_image'] : false;
	$sol3_image = !empty($props['sol3_image']) ? $props['sol3_image'] : false;

	if ($sol1_image) {
		$sol1_image = gp_get_img_url($sol1_image, 'full');
		$sol1_image = "background-image: url($sol1_image)";
	}

	if ($sol2_image) {
		$sol2_image = gp_get_img_url($sol2_image, 'full');
		$sol2_image = "background-image: url($sol2_image)";
	}

	if ($sol3_image) {
		$sol3_image = gp_get_img_url($sol3_image, 'full');
		$sol3_image = "background-image: url($sol3_image)";
	}

	if ($sol1_subtitle) {
		$subtitle1 = substr($sol1_subtitle, 0, 100);
		if (strlen($sol1_subtitle) > 100) {
			$sol1_subtitle = $subtitle1 . '...';
		}
	}

	if ($sol2_subtitle) {
		$subtitle2 = substr($sol2_subtitle, 0, 100);
		if (strlen($sol2_subtitle) > 100) {
			$sol2_subtitle = $subtitle2 . '...';
		}
	}

	if ($sol3_subtitle) {
		$subtitle3 = substr($sol3_subtitle, 0, 100);
		if (strlen($sol3_subtitle) > 100) {
			$sol3_subtitle = $subtitle3 . '...';
		}
	}

	$html = "
    <div class='shortcode Solutions $text_type' $style>
      <div class='container'>
        <div class='row'>
          <div class='col s12'>
            <div class='title-container'>
              <h2 class='title bold scroll-animate'>$title</h3>
              <p class='scroll-animate'>$subtitle</p>
            </div>
          </div>

          <div class='card text-white scroll-animate' style='$sol1_image'>
            <div class='cbody'>
            	<h3 class='title'>$sol1_title</h3>
            	<p class='subtitle'>$sol1_subtitle</p>
            </div>
            <a href='$sol1_link' title='$sol1_title' class='caction'></a>
          </div>

          <div class='card text-white scroll-animate' style='$sol2_image'>
            <div class='cbody'>
            	<h3 class='title'>$sol2_title</h3>
            	<p class='subtitle'>$sol2_subtitle</p>
            </div>
            <a href='$sol2_link' title='$sol2_title' class='caction'></a>
          </div>

          <div class='card text-white scroll-animate' style='$sol3_image'>
            <div class='cbody'>
            	<h3 class='title'>$sol3_title</h3>
            	<p class='subtitle'>$sol3_subtitle</p>
            </div>
            <a href='$sol3_link' title='$sol3_title' class='caction'></a>
          </div>
        </div>
      </div>
    </div>";

	return $html;
}

add_shortcode('gp_Solutions', 'Solutions_shortcode');