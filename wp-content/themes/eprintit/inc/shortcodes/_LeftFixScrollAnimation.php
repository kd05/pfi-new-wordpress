<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

function gp_fix_left_Scroll_animation_shortcode($atts, $content = null)
{
    $defaults = [
        'ids' => '',
    ];
    $props = shortcode_atts($defaults, $atts);
    $ids = !empty($props['ids']) ? $props['ids'] : "";
    $explode_ids = explode("|",$ids);
    
    $id_html = "";
    foreach ($explode_ids as $id => $val){
        $id_html .= "<div class='$val fix-circle'  data-animate='$val'><span>$val</span></div>";
    }

    $html = "
        <div class='shortcode pfi-shortcode LeftFixScrollAnimation'> 
            $id_html           
        </div>            
    ";
    return $html;
}
add_shortcode('gp_FixLeftScrollAnimation', 'gp_fix_left_Scroll_animation_shortcode');


