<?php

if (!defined('ABSPATH')) exit; // Exit if accessed directly

// [gp_CalculateYourEarnings
//   text-type= 'dark'
//   min-height= ''

//   title1= ''
//   title2= ''
//   title3= ''

//   btn-text=''
//   btn-url=''
//   btn-color=''

//   bg-color= ''
//   bg-img= ''
//   bg-video= ''
// ]
function CalculateYourEarnings_shortcode($atts, $content = null)
{
	$defaults = [
		'text-type'  => 'dark',
		'min-height' => '655px',

		'title1' => false,
		'title2' => false,
		'title3' => false,

		'btn-text'  => false,
		'btn-url'   => false,
		'btn-color' => 'blue',

		'bg-color' => false,
		'bg-img'   => false,
		'bg-video' => false,
	];
	$props = shortcode_atts($defaults, $atts);

	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? 'dark' : 'light';
	$min_height = ($props['min-height']) ? $props['min-height'] : '655px';

	$title1 = ($props['title1']) ? $props['title1'] : '';
	$title2 = ($props['title2']) ? $props['title2'] : '';
	$title3 = ($props['title3']) ? $props['title3'] : '';

	$btn_text = ($props['btn-text']) ? $props['btn-text'] : false;
	$btn_url = ($props['btn-url']) ? $props['btn-url'] : false;
	$btn_color = $props['btn-color'];
	$bg_color = ($props['bg-color']) ? $props['bg-color'] : false;

	$bg_img = ($props['bg-img']) ? $props['bg-img'] : false;
	$btn_text_color = 'grey';

	$btn = '';

	if ($btn_text && $btn_url) {
		if ($btn_color != 'white') {
			$btn_text_color = 'white';
		}
		$btn = "<a href='$btn_url' class='btn bg-$btn_color text-$btn_text_color'>$btn_text</a>";
	}

	if ($bg_img) {
		$bg_img = gp_get_file_url($bg_img, 'full');
		$bg_img = "background-image: url( $bg_img );";
	}

	$bg_video = ($props['bg-video']) ? $props['bg-video'] : false;

	$min_height_style = "min-height: $min_height;";
	$bg_color = "background-color:$bg_color;";

	if ($bg_video) {
		$bg_video = gp_get_file_url($bg_video);
		$bg_video = "
    <video autoplay loop poster muted class='bg-video'>
       <source src='$bg_video' type='video/mp4'>
     </video>
    ";
	}

	$style = "style='$min_height_style $bg_color $bg_img'";

	$html = "
    <div class='shortcode flex jc-center ai-center CalculateYourEarnings $text_type' $style>
      $bg_video
      <div class='container'>
      	<div class='row jc-start'>
          <div class='col s6'>
          	<div class='content general-content'>
          		<p class='title1 scroll-animate'>$title1</p>
          		<h3 class='title2 scroll-animate'>$title2</h3>
          		<h2 class='title3 scroll-animate'>$title3</h2>
          		<div class='btn-container scroll-animate'>$btn</div>
            </div>
          </div>
        </div>
      </div>
    </div>";

	return $html;
}

add_shortcode('gp_CalculateYourEarnings', 'CalculateYourEarnings_shortcode');