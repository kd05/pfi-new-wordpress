<?php

if (!defined('ABSPATH')) {
	exit;
} // Exit if accessed directly

// [gp_CenteredTextOverVidOrImage
//   min-height= ''
//   text-type= ''
//   title= ''
//   subtitle = ''
//   description= ''
//   btn-text = ''
//   btn-url = ''
//   bg-color= ''
//   bg-img= ''
//   bg-video= ''
// ]
function CenteredTextOverVidOrImage_shortcode($atts, $content = null)
{
	$defaults = [
		'min-height'  => '557px',
		'title'       => false,
		'subtitle'    => false,
		'description' => false,
		'btn-text'    => false,
		'btn-url'     => false,
		'text-type'   => 'dark',
		'bg-color'    => false,
		'bg-img'      => false,
		'bg-video'    => false,
	];

	$props = shortcode_atts($defaults, $atts);

	$min_height = ($props['min-height']) ? $props['min-height'] : '557px';
	$title = ($props['title']) ? $props['title'] : false;
	$subtitle = ($props['subtitle']) ? $props['subtitle'] : false;
	$btn_text = ($props['btn-text']) ? $props['btn-text'] : false;
	$btn_url = ($props['btn-url']) ? $props['btn-url'] : false;
	$description = ($props['description']) ? $props['description'] : false;
	$bg_color = ($props['bg-color']) ? $props['bg-color'] : 'white';
	$text_type = ($props['text-type'] == 'dark' || $props['text-type'] == '') ? '#5a5a5a' : '#fefefe';
	$btn = '';

	if ($btn_text && $btn_url) {
		// code...
		$btn = "<a href='$btn_url' class='btn bg-blue text-white'>$btn_text</a>";
	}

	$bg_img = ($props['bg-img']) ? $props['bg-img'] : false;
	$bg_img = gp_get_img_url($bg_img);

	$bg_video = ($props['bg-video']) ? $props['bg-video'] : false;
	$bg_video = gp_get_file_url($bg_video);

	$min_height_style = "min-height: $min_height;";
	$bg_color = "background-color:$bg_color;";
	$bg_img = !empty($bg_img) ? "background-image:url($bg_img);" : "";
	$text_type_style = "color:$text_type;";

	if ($bg_video) {

		$bg_video = "
    <video autoplay loop poster muted class='bg-video'>
       <source src='$bg_video' type='video/mp4'>
     </video>";
	}

	$style = "style='$min_height_style $bg_color $bg_img'";

	ob_start();
	?>
    <div class='CenteredTextOverVidOrImage' <?php echo $style; ?>>
		<?php echo $bg_video; ?>
        <div class='container'>
            <div class='row'>
                <div class='col text-center general-content'>
					<?php if ($title) { ?>
                        <h2 class='scroll-animate' style='<?php echo $text_type_style; ?>'><?php echo $title; ?></h2>
					<?php } ?>
					<?php if ($subtitle) { ?>
                        <h3 class='scroll-animate' style='<?php echo $text_type_style; ?>'><?php echo $subtitle; ?></h3>
					<?php } ?>
					<?php if ($description) { ?>
                        <p class="scroll-animate" style='<?php echo $text_type_style; ?>'><?php echo $description; ?></p>
					<?php } ?>
					<?php if ($btn) { ?>
                        <div class='btn-container scroll-animate'><?php echo $btn; ?></div>
					<?php } ?>
                </div>
            </div>
        </div>
    </div>
	<?php
	$html = ob_get_clean();
	return $html;
}

add_shortcode('gp_CenteredTextOverVidOrImage', 'CenteredTextOverVidOrImage_shortcode');