<?php

function gp_two_col_open_shortcode($atts, $content = null)
{
	$parameters = shortcode_atts(array(
		'align'   => 'middle',
		'nobar'   => 'true',
		'landing' => 'false',
        'nospacing' => false,
	), $atts);
	$class = 'gp-two-col gp-align-' . $parameters['align'];
	$class .= $parameters['nobar'] == 'false' ? '' : ' gp-no-bar';
	$class .= $parameters['landing'] == 'true' ? ' landing' : '';
	$col_class = $parameters['nospacing'] ? ' nospacing' : '';
	ob_start(); ?>
    <div class="shortcode TwoColShortcode<?php echo $col_class; ?>">
    <div class="lg-container">
    <div class="<?php echo $class; ?>">
	<?php $output = ob_get_clean();

	return $output;
}

add_shortcode('gp_two_col_open', 'gp_two_col_open_shortcode');

function gp_two_col_close_shortcode($atts, $content = null)
{

	ob_start(); ?>
    </div>
    </div>
    </div>
	<?php $output = ob_get_clean();

	return $output;
}

add_shortcode('gp_two_col_close', 'gp_two_col_close_shortcode');

function gp_three_col_open_shortcode($atts, $content = null)
{
	$parameters = shortcode_atts(array(
		'align' => 'middle',
		'nobar' => 'false',
	), $atts);
	if ($parameters['nobar'] == 'true') {
		$class = 'gp-no-bar ';
	} else {
		$class = '';
	}
	ob_start(); ?>
    <div class="shortcode ThreeColShortcode">
    <div class="lg-container">
    <div class="<?php echo $class; ?> gp-three-col gp-align-<?php echo $parameters['align']; ?>">
	<?php $output = ob_get_clean();

	return $output;
}

add_shortcode('gp_three_col_open', 'gp_three_col_open_shortcode');


function gp_three_col_close_shortcode($atts, $content = null)
{

	ob_start(); ?>
    </div>
    </div>
    </div>
	<?php $output = ob_get_clean();

	return $output;
}

add_shortcode('gp_three_col_close', 'gp_three_col_close_shortcode');

function gp_left_shortcode($atts, $content = null)
{
	$content = apply_filters('the_content', $content);
	$content = do_shortcode($content);
	ob_start(); ?>
    <div class="gp-col gp-left">
		<?php echo $content; ?>
    </div>
	<?php $output = ob_get_clean();

	return $output;
}

add_shortcode('gp_left', 'gp_left_shortcode');

function gp_middle_shortcode($atts, $content = null)
{
	$content = apply_filters('the_content', $content);
	$content = do_shortcode($content);
	ob_start(); ?>
    <div class="gp-col gp-middle">
		<?php echo $content; ?>
    </div>
	<?php $output = ob_get_clean();

	return $output;
}

add_shortcode('gp_middle', 'gp_middle_shortcode');

function gp_right_shortcode($atts, $content = null)
{
	$content = apply_filters('the_content', $content);
	$content = do_shortcode($content);
	ob_start(); ?>
    <div class="gp-col gp-right">
		<?php echo $content; ?>
    </div>
	<?php $output = ob_get_clean();

	return $output;
}

add_shortcode('gp_right', 'gp_right_shortcode');

function gp_embed_youtube_shortcode($atts, $content = null)
{
	$parameters = shortcode_atts(array(
		'code'   => '',
		'height' => '450',
		'width'  => '1400',
		'center' => 'true',
	), $atts);
	$url = "https://www.youtube-nocookie.com/embed/" . $parameters['code'] . '?rel=0&showinfo=0';
	if ($parameters['center'] == 'true') {
		$center = ' style="text-align: center"';
	} else {
		$center = '';
	}
	ob_start();
	?>

    <div class="gp-video-container" <?php echo $center; ?>>
        <iframe src="<?php echo $url; ?>" width="<?php echo $parameters['width']; ?>"
                height="<?php echo $parameters['height']; ?>" frameborder="0" allowfullscreen="allowfullscreen"
                mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen"
                oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"></iframe>
    </div>

	<?php
	$output = ob_get_clean();
	return $output;
}

add_shortcode('embed_video', 'gp_embed_youtube_shortcode');

function gp_pricing_container_shortcode($atts, $content = null)
{
	ob_start();
	?>

    <div class="gp-pricing-container">
		<?php echo apply_filters('the_content', $content); ?>
    </div>

	<?php
	$output = ob_get_clean();

	return $output;
}

add_shortcode('gp_pricing_container', 'gp_pricing_container_shortcode');

function gp_pricing_box_shortcode($atts, $content = null)
{
	$a = shortcode_atts(array(
		'title'    => '',
		'price'    => '',
		'top_link' => '',
		'try_link' => '',
		'top_text' => '',
		'try_text' => '',
		'line1'    => '',
		'line2'    => '',
		'line3'    => '',
		'show_mo'  => 'true',
		'feature'  => 'Features:',
	), $atts);
	ob_start();
	?>
    <div class="pricing-item-container">
        <div class="item">
            <div class="item-container">
                <div class="box effect8">
                    <div class="title">
						<?php echo $a['title']; ?>
                    </div>
                    <div class="price-detail">
						<?php if ($a['show_mo'] == 'false') {
							$mo = '';

						} else {
							//$mo = '<span class="month">&nbsp;/ mo*</span>';
							$mo = '';

						}; ?>
                        <div class="price">
							<?php echo $a['price']; ?><?php echo $mo; ?>
                        </div>
                        <div class="details">
							<?php if ($a['line1']) {
								echo '<p>' . $a['line1'] . '</p>';
							} ?>
							<?php if ($a['line2']) {
								echo '<p>' . $a['line2'] . '</p>';
							} ?>
							<?php if ($a['line3']) {
								echo '<p>' . $a['line3'] . '</p>';
							} ?>
                        </div>
                    </div>
                    <div class="link">
                        <div class="text">
                            <a href="<?php echo $a['top_link']; ?>"><?php echo $a['top_text']; ?></a>
                        </div>
                    </div>
                </div>
                <div class="features">
                    <div class="title">
						<?php echo $a['feature']; ?>
                    </div>
                    <div class="list">
						<?php echo apply_filters('the_content', $content); ?>
                    </div>
                    <div class="button-container center">
                        <div class="button red">
                            <a href="<?php echo $a['try_link']; ?>"><?php echo $a['try_text']; ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<?php
	$output = ob_get_clean();

	return $output;
}

add_shortcode('pricing_box', 'gp_pricing_box_shortcode');

function gp_heading_shortcode($atts, $content)
{
	$content = apply_filters('the_content', $content);
	$output = '<div class="shortcode Heading">' . $content . '</div>';
	return $output;
}

add_shortcode('gp_Heading', 'gp_heading_shortcode');

function gp_FullWidthImage_shortcode($atts, $content)
{
	$props = shortcode_atts(array(
		'image' => false,
		'align' => '',
	), $atts);

	$image = '';
	if ($props['image']) {
		$img = gp_get_img_url($props['image'], 'full');
		$image = "<a href='$img' data-fancybox='image'><img src='$img' alt='' /></a>";
	}

	$class = $props['align'] ? ' text-' . $props['align'] : '';

	$content = apply_filters('the_content', $content);
	$output = "<div class='shortcode FullWidthImage'>
        <div class='row flex-none'>
        <div class='col image-col'>$image</div>
        <div class='col content-col general-content$class'>$content</div>
        </div>
    </div>";
	return $output;
}

add_shortcode('gp_FullWidthImage', 'gp_FullWidthImage_shortcode');

function gp_ContainerOpen_shortcode($atts, $content)
{
	$props = shortcode_atts(array(
		'type' => 'large',
	), $atts);
	$container_class = $props['type'] == 'small' ? "container" : "lg-container";
	$output = '<div class="shortcode GPContainer"><div class="'.$container_class.'">';
	return $output;
}

add_shortcode('gp_ContainerOpen', 'gp_ContainerOpen_shortcode');

function gp_ContainerClose_shortcode($atts, $content)
{
	$output = '</div></div>';
	return $output;
}

add_shortcode('gp_ContainerClose', 'gp_ContainerClose_shortcode');

/**
 * Short Code for spacer
 */
function gp_spacer_shortcode($atts)
{
	$a = shortcode_atts(array(
		'pixel' => '30',
	), $atts);
	$rand = generateRandomString();
	if ($a['pixel'] == 'top') {
		$a['pixel'] = '80';
	}
	if ($a['pixel'] == 'bottom') {
		$a['pixel'] = '55';
	}
	ob_start();
	?>
    <style>
        .gp-responsive-spacer-<?php echo $rand; ?> {
            padding-bottom: <?php echo (int) $a['pixel'] * 1;?>px;
        }

        @media screen and (max-width: 1024px) {
            .gp-responsive-spacer-<?php echo $rand; ?> {
                padding-bottom: <?php echo (int) $a['pixel'] * .76; ?>px;
            }
        }

        @media screen and (max-width: 700px) {
            .gp-responsive-spacer-<?php echo $rand; ?> {
                padding-bottom: <?php echo (int) $a['pixel'] * .52; ?>px;
            }
        }
    </style>
    <div class="gp-responsive-spacer-<?php echo $rand; ?>"></div>

	<?php
	$output = ob_get_clean();

	return $output;
}

add_shortcode('gp_spacer', 'gp_spacer_shortcode');

function generateRandomString($length = 10)
{
	$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

function up_embed_youtube_lightbox_shortcode($atts)
{
	$parameters = shortcode_atts(array(
		'bg-img'    => '',
		'title'     => '',
		'video-url' => '',
		'link'      => '',
	), $atts);
	$bg_img = $parameters['bg-img'];
	$title = $parameters['title'];
	$video_url = $parameters['video-url'];
	$link = $parameters['link'];
	ob_start();
	?>

    <div class="video-outer">
        <div class="video">
            <div class="video-inner">
                <div class="image-container" style="background-image:url('<?php echo $bg_img; ?>')">
                    <a data-fancybox="video" href="<?php echo $video_url; ?>"></a>
                </div>
            </div>
        </div>
        <div class="text">
            <?php if ($link == '') { ?>
                <p class="video-title"><?php echo $title; ?></p>
            <?php } else { ?>
                <p class="video-title"><?php echo $title; ?></p>
            <?php } ?>
        </div>
    </div>

	<?php
	$output = ob_get_clean();
	return $output;
}

add_shortcode('embed_video_lightbox', 'up_embed_youtube_lightbox_shortcode');

function gp_VideoContainerOpen_shortcode($atts) {
	$output = '<div class="shortcode videoArchive"><div class="container"><div class="video-section"><div class="video-section-inner">';
	return $output;
}
add_shortcode('gp_VideoContainerOpen', 'gp_VideoContainerOpen_shortcode');

function gp_VideoContainerClose_shortcode($atts) {
	$output = '</div></div></div></div>';
	return $output;
}
add_shortcode('gp_VideoContainerClose', 'gp_VideoContainerClose_shortcode');


function gp_white_paper_shortcode ( $atts, $content = null ){
	$content = apply_filters('the_content', $content);
	$content = do_shortcode($content);
	ob_start();
	?>
    <div class="shortcode white-paper">
        <div class="content general-content"><?php echo $content; ?></div>
    </div>
	<?php
	$output = ob_get_clean();
	return $output;
}
add_shortcode('white_paper', 'gp_white_paper_shortcode');

function gp_ButtonsShortcode($atts, $content) {

	$parameters = shortcode_atts(array(
		'buttons' => 'two'
	), $atts);

	$button_class = $parameters['buttons'];

	$content = apply_filters('the_content', $content);
	$output = '<div class="shortcode gpButtons '.$button_class.'">' . $content . '</div>';
	return $output;
}
add_shortcode('gp_Buttons', 'gp_ButtonsShortcode');