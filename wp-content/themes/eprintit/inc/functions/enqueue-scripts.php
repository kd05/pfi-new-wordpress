<?php

function global_scripts()
{
	// Adding styles file in the header
	wp_enqueue_style('global-styles', get_template_directory_uri() . '/assets/styles/dist/style.css', array(), false, 'all');
	wp_enqueue_style('fontawesome-styles', get_template_directory_uri() . '/assets/styles/dist/font-awesome.min.css', array(), false, 'all');
	wp_enqueue_style('select2-styles', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css', array(), false, 'all');
	wp_enqueue_style('slick-styles', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array(), false, 'all');
	wp_enqueue_style('fancy-styles', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.1/jquery.fancybox.min.css', array(), false, 'all');
    wp_enqueue_style('modal-video-css', get_template_directory_uri() . '/assets/styles/dist/modal-video.min.css', array(), false, 'all');
    wp_enqueue_style('slick-theme', get_template_directory_uri() . '/assets/styles/dist/slick-theme.css', array(), false, 'all');

	// Adding scripts file in the footer
	wp_enqueue_script('global-js', get_template_directory_uri() . '/assets/scripts/dist/scripts.js', array('jquery', 'select2-js'), false, true);
	wp_register_script('select2-js', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js');
	wp_enqueue_script('fancy-js', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.1/jquery.fancybox.min.js', array('jquery'), false, true);
	wp_enqueue_script('map-async-defer', "https://maps.googleapis.com/maps/api/js?key=AIzaSyAQjub4O2uErltfGF6FZaqpNoljineTTEM&libraries=places&callback=mapInitialize", array('global-js'), null, true);
//	wp_enqueue_script('google-recaptcha', "https://www.google.com/recaptcha/api.js");
}

// add_action('wp_head', 'global_scripts', 999);
add_action('wp_enqueue_scripts', 'global_scripts', 999);
