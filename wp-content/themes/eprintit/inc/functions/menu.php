<?php
// Register menus
register_nav_menus(
	array(
		'main-nav'          => __('The Main Menu', 'wpBaby'),   // topbar nav
		'main-nav-logged'   => __('The Main Menu (Logged In)', 'wpBaby'),   // topbar nav for logged in users
		'second-nav'        => __('The Second Menu', 'wpBaby'),   // topbar nav
		'second-nav-logged' => __('The Second Menu (Logged In)', 'wpBaby'),   // topbar nav 2 for logged in users
		'footer-learn-more' => __('Footer learn more Menu', 'wpBaby'), // footer nav
		'footer-resources'  => __('Footer resources Menu', 'wpBaby'), // footer nav
		'footer-partners'   => __('Footer partners Menu', 'wpBaby'), // footer nav
		'footer-copyright'  => __('Footer Copyright Menu', 'wpBaby'), // footer nav
	)
);

// The Top Menu
function build_topbar_nav()
{

    wp_nav_menu(array(
        'container'      => false,
        'theme_location' => 'main-nav'
    ));
}

function build_topbar2_nav()
{
//	$theme_location = is_user_logged_in() ? 'second-nav-logged' : 'second-nav';
	wp_nav_menu(array(
		'container'      => false,
		'theme_location' => 'main-nav'
	));
}

function build_learn_more_menu()
{
	// code...
	wp_nav_menu(array(
		'container'      => false,                           // Remove nav container
		'menu_class'     => 'footer-menu',       // Adding custom nav class
		'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'theme_location' => 'footer-learn-more',                    // Where it's located in the theme
		'depth'          => 2,                                   // Limit the depth of the nav
		'fallback_cb'    => false                         // Fallback function (see below)
	));
}

function build_resources_menu()
{
	// code...
	wp_nav_menu(array(
		'container'      => false,                           // Remove nav container
		'menu_class'     => 'footer-menu',       // Adding custom nav class
		'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'theme_location' => 'footer-resources',                    // Where it's located in the theme
		'depth'          => 2,                                   // Limit the depth of the nav
		'fallback_cb'    => false                         // Fallback function (see below)
	));
}

function build_partners_menu()
{
	// code...
	wp_nav_menu(array(
		'container'      => false,                           // Remove nav container
		'menu_class'     => 'footer-menu',       // Adding custom nav class
		'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'theme_location' => 'footer-partners',                    // Where it's located in the theme
		'depth'          => 2,                                   // Limit the depth of the nav
		'fallback_cb'    => false                         // Fallback function (see below)
	));
}

// use to build a navigation based on subpages.
// first make a custom field which allows for menu id to be stored by the page.
// then loop through each parent page and check if it contains a nav id.
// get the nav id and pass it into this.
function build_child_nav($menu_id)
{
	# code...
	wp_nav_menu(array(
		'theme_location' => 'side-nav',
		'menu'           => $menu_id
	));
}


