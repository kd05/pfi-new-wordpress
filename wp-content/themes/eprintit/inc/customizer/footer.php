<?php
function mytheme_customize_footer_register($wp_customize)
{
	//All our sections, settings, and controls will be added here
	// NOTE: class for WYSIWIG editor
	class WYSIWIG_Custom_Control extends WP_Customize_Control
	{
		/**
		 * Render the content on the theme customizer page
		 */
		public function render_content()
		{
			?>
            <label>
                <!-- <span class="customize-text_editor"><?php echo esc_html($this->label); ?></span> -->
				<?php// $settings = array(
				//      'textarea_name' => $this->id
				//    );
				//  wp_editor($this->value(), $this->id, $settings ); ?>
            </label>
			<?php
		}
	}

	$wp_customize->add_panel('footer_panel', array(
		'title'       => 'Footer',
		'description' => 'This is panel Description',
		'priority'    => 100,
	));

	// NOTE: FOOTER: SECTION
	$wp_customize->add_section('footer_default_section', array(
		'title'    => __('Default', 'eprintit'),
		'priority' => 100,
		'panel'    => 'footer_panel'
	));
	$wp_customize->add_section('footer_links_section', array(
		'title'    => __('Links', 'eprintit'),
		'priority' => 300,
		'panel'    => 'footer_panel'
	));

	$wp_customize->add_setting('footer_logo_image', array(
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('footer_subscribe_title', array(
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('footer_subscribe_description', array(
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('footer_subscribe_action', array(
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('footer_copyright', array(
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('footer_facebook', array(
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('footer_twitter', array(
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('footer_instagram', array(
		'transport' => 'refresh',
	));

	$wp_customize->add_setting('footer_linkedin', array(
		'transport' => 'refresh',
	));
	$wp_customize->add_setting('footer_youtube', array(
		'transport' => 'refresh',
	));

	// NOTE: FOOTER BACKGROUND IMAGE CONTROLLER
	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'footer_logo_image', array(
		'label'    => __('Logo', 'eprinit'),
		'section'  => 'footer_default_section',
		'settings' => 'footer_logo_image',
	)));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_subscribe_title', array(
		'label'    => __('Subscription Panel Title', 'eprinit'),
		'section'  => 'footer_default_section',
		'settings' => 'footer_subscribe_title',
		'type'     => 'text',
	)));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_subscribe_description', array(
		'label'    => __('Subscription Panel Description', 'eprinit'),
		'section'  => 'footer_default_section',
		'settings' => 'footer_subscribe_description',
		'type'     => 'text',
	)));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_subscribe_action', array(
		'label'    => __('Subscription Form Action', 'eprinit'),
		'section'  => 'footer_default_section',
		'settings' => 'footer_subscribe_action',
		'type'     => 'text',
	)));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_copyright', array(
		'label'    => __('Copyright Text', 'eprinit'),
		'section'  => 'footer_default_section',
		'settings' => 'footer_copyright',
		'type'     => 'text',
	)));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_facebook', array(
		'label'    => __('Facebook URL', 'eprinit'),
		'section'  => 'footer_links_section',
		'settings' => 'footer_facebook',
		'type'     => 'text',
	)));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_twitter', array(
		'label'    => __('Twitter URL', 'eprinit'),
		'section'  => 'footer_links_section',
		'settings' => 'footer_twitter',
		'type'     => 'text',
	)));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_instagram', array(
		'label'    => __('Instagram URL', 'eprinit'),
		'section'  => 'footer_links_section',
		'settings' => 'footer_instagram',
		'type'     => 'text',
	)));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_linkedin', array(
		'label'    => __('LinkedIn URL', 'eprinit'),
		'section'  => 'footer_links_section',
		'settings' => 'footer_linkedin',
		'type'     => 'text',
	)));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize, 'footer_youtube', array(
		'label'    => __('YouTube URL', 'eprinit'),
		'section'  => 'footer_links_section',
		'settings' => 'footer_youtube',
		'type'     => 'text',
	)));

}

add_action('customize_register', 'mytheme_customize_footer_register');