<?php
/**
 * Template part for displaying posts
 *
 * Used for single, index, archive, search.
 */
?>
<?php add_hero_class(); ?>
<?php get_header() ?>
<?php
/*the_title(); */
global $post;

$img_url = "";

if (has_post_thumbnail($post->ID)) {
	$img_url = gp_get_img_url_from_post($post->ID, 'full');
}
$temp = get_the_date('d M y');
$date = explode(" ", $temp);

$post_type = get_post_type();
$slogan = get_field('slogan') ? get_field('slogan') : "";
$title = get_the_title();
$content = get_the_content();
$temp_dir = get_template_directory_uri();
$upload_dir = wp_upload_dir('2018/09')['url'];

$top_img = (get_field('top_image')) ? gp_get_img_url(get_field('top_image'), 'full') : $img_url;
$middle_img = (get_field('middle_image')) ? gp_get_img_url(get_field('middle_image'), 'full') : $img_url;
$bottom_img = (get_field('bottom_image')) ? gp_get_img_url(get_field('bottom_image'), 'full') : $img_url;

$middle_bg = "style='background-image: url($middle_img);'";
$bottom_img = "style='background-image: url($bottom_img);'";

$top_content = get_field('top_content');
$middle_content = get_field('middle_content');
$bottom_content = get_field('bottom_content');
?>
    <section class='features'>
        <article>
            <div class="grid hero flex ai-center bg-orange"
                 style="<?php echo !empty($img_url) ? 'background-image: url(' . $img_url . ')' : ''; ?>">
                <div class="overlay"></div>
                <div class="container">

                    <div class="shortcode row">
                        <div class="col s12">
                            <h2 class='text-center mb-1 text-white'><?php echo $post_type; ?></h2>
							<?php if ($slogan) { ?>
                                <h3 class='text-center text-white'><?= $slogan ?></h3>
							<?php } ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="grid breadcrumbs mb-5">
                <div class="container bg-white flex p0">
                    <div class="crumb home text-white"><a href="<?php echo get_bloginfo('url'); ?>">Home</a></div>
                    <div class="crumb post-type"><a
                                href="<?php echo get_bloginfo('url'); ?>/features"><?php echo lcfirst($post_type); ?></a>
                    </div>
                    <div class="crumb post-name"><span><?= $title ?></span></div>
                </div>
            </div>


            <div class="grid header top-featured-content">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <h2 class='bold mb-1'><?= $title ?></h2>
                        </div>
                        <div class="col s12">
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
								<?php get_template_part('parts/loop', 'single'); ?>
							<?php endwhile; endif; ?>
                        </div>
                    </div>
                    <div class="row top-lower-section jc-end">
                        <div class="col s6 general-content">
							<?= $top_content ?>
                        </div>
                        <div class="col s6">
                            <div class="top image-container flex jc-center">
                                <figure>
                                    <img src="<?= $top_img ?>" alt="">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>

				<?php if (!empty($middle_content)) { ?>
                    <div class="bg-container flex ai-center middle-feature" <?= $middle_bg ?>>
                        <div class="container-fluid">
                            <div class="row jc-center">
                                <div class="col s6 text-center general-content">
									<?= $middle_content ?>
                                </div>
                            </div>
                        </div>
                    </div>
				<?php } ?>

				<?php if (!empty($bottom_content)) { ?>
                    <div class="container section2">
                        <div class="row">
                            <div class="col s12">
                                <div class="featured-img-container">
                                    <figure>
                                        <div class="orange-box"></div>
                                        <div class="img" <?= $bottom_img ?>></div>
                                    </figure>
                                </div>
								<?= $bottom_content ?>
                            </div>
                        </div>
                    </div>
				<?php } ?>
            </div>


        </article>
    </section>
<?php get_footer() ?>