<?php add_hero_class(); ?>
<?php get_header() ?>
<?php
/**
 * Template part for displaying posts
 *
 * Used for single, index, archive, search.
 */
/*the_title(); */
global $post;

$id = get_the_ID();
$img_url = "";

if (has_post_thumbnail($post->ID)) {
	$img_url = gp_get_img_url_from_post($post->ID, 'full');
}
$temp_date = get_the_date('d M y');
$temp_dir = get_template_directory_uri();
$date = explode(" ", $temp_date);

$post_type = get_post_type();
$slogan = "The strength of the team is each member. The strength of each member is the team.";
$title = get_the_title();

$big_image = get_field('big_image');
$big_image = ($big_image) ? gp_get_img_url($bg_img, 'full') : $img_url;
$big_image = "style='background-image:url($big_image);'";

$second_title = get_field('second_title');
$second_content = get_field('second_content');
$second_content = do_shortcode($second_content);
?>
    <section class='potential'>
        <article>
            <div class="grid hero flex ai-center bg-orange">
                <div class="overlay"></div>
                <div class="container">
                    <div class="shortcode row">
                        <div class="col s12">
                            <h2 class='text-center mb-1 text-white'><?= $post_type ?></h2>
                            <h3 class='text-center text-white'><?= $slogan ?></h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid breadcrumbs">
                <div class="container bg-white flex p0">
                    <div class="crumb home text-white"><span>Home</span></div>
                    <div class="crumb post-type"><span><?= $post_type ?></span></div>
                    <div class="crumb post-name"><span><?= $title ?></span></div>
                </div>
            </div>
            <div class="grid header mt-5">
                <div class="container">
                    <div class="row">
                        <div class="col s6">
                            <h2 class='mb-1'><?= $title ?></h2>
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

								<?php get_template_part('parts/loop', 'single'); ?>

							<?php endwhile; endif; ?>
                        </div>
                        <div class="col s6">
                            <div class="featured-img-container">
                                <figure>
                                    <div class="orange-box"></div>
                                    <div class='img' <?= $big_image ?>></div>
                                </figure>
                            </div>
                        </div>
                        <div class="col s12 mt-5">
                            <h3 class='mb-1'><?= $second_title ?></h3>
							<?= $second_content ?>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section>
<?php get_footer() ?>