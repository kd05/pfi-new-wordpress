<?php
/**
 * Template part for displaying posts
 *
 * Used for single, index, archive, search.
 */
?>
<?php add_hero_class(); ?>
<?php get_header() ?>
<?php
global $post;

$id = get_the_ID();
$img_url = "";

if (has_post_thumbnail($post->ID)) {
	$img_url = gp_get_img_url_from_post($post->ID, 'full');
}
$temp_date = get_the_date('d M y');
$temp_dir = get_template_directory_uri();
$date = explode(" ", $temp_date);

$post_type = get_post_type();
$slogan = get_field('slogan') ? get_field('slogan') : "The strength of the team is each member. The strength of each member is the team.";
$title = get_the_title();

$big_image_url = get_field('big_image');
$big_image_url = ($big_image_url) ? $big_image_url : $img_url;
$big_image_url = "style='background-image:url($big_image_url);'";

$second_title = get_field('second_title');
$second_content = get_field('second_content');
$second_content = do_shortcode($second_content);
$objective_content = get_field('objective_content', false, false);
$objective_content = apply_filters('the_content', $objective_content);

?>
    <section class='potential'>
        <article>
            <div class="grid hero flex ai-center bg-orange" style="<?php echo !empty($img_url) ? 'background-image: url('. $img_url .')' : ''; ?>">
                <div class="overlay"></div>
                <div class="container">

                    <div class="shortcode row">
                        <div class="col s12">
                            <h2 class='text-center mb-1 text-white'>Case Studies</h2>
                            <h3 class='text-center text-white slogan'><?= $slogan ?></h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="grid breadcrumbs">
                <div class="container bg-white flex p0">
                    <div class="crumb home text-white"><span><a href="<?php echo get_bloginfo('url'); ?>">Home</a></span></div>
                    <div class="crumb post-type"><span><a href="<?php echo get_bloginfo('url'); ?>/case-studies">Case Studies</a></span></div>
                    <?php
                        $category = wp_get_object_terms(get_the_ID(), 'case_study_cat', array('number'  => 1));
                        if( !empty($category) ) {
                            $category = current($category);
                            ?>
                            <div class="crumb post-type post-cat"><span><?php echo $category->name; ?></span></div>
                            <?php
                        }
                    ?>
                    <div class="crumb post-name"><span><?= $title ?></span></div>
                </div>
            </div>

            <div class="grid header mt-5">
                <div class="container">
                    <div class="row">
                        <div class="col s6">
                            <div class="content">
                                <h1 class="title"><?= $title ?></h1>
	                            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		                            <?php get_template_part('parts/loop', 'single'); ?>
	                            <?php endwhile; endif; ?>
                            </div>
                        </div>
                        <div class="col s6">
                            <div class="featured-img-container">
                                <figure>
                                    <div class="orange-box"></div>
                                    <div class='img' <?= $big_image_url ?>></div>
                                </figure>
                            </div>
                        </div>
                        <div class="col s12 mt-5">
                            <div class="content">
	                            <?php echo $objective_content; ?>
                            </div>
                        </div>
                        <div class="col s12 mt-3">
                            <div class="content">
                                <h3 class='subtitle bold'><?= $second_title ?></h3>
	                            <?= $second_content ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </article>
    </section>

<?php get_footer();