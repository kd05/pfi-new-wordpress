<?php
/**
 * The template for displaying 404 (page not found) pages.
 *
 * For more info: https://codex.wordpress.org/Creating_an_Error_404_Page
 */

get_header(); ?>
<section class="grid flex ai-center">
    <div class="row">
        <div class="col s12">
            <article class="general-content text-center">
                <header class="article-header">
                    <h1 class="text-grey"><?php _e( 'Epic 404 - Article Not Found', 'wpBabywp' ); ?></h1>
                </header> <!-- end article header -->
                <footer>
                    <p><?php _e( 'The article you were looking for was not found, but maybe try looking again!', 'wpBabywp' ); ?></p>
                </footer>
            </article>
        </div>
    </div>
</section>
<?php get_footer(); ?>
