<?php
/**
 * Template part for displaying page content in page.php
 */
?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
    <?php
    ob_start();
    the_content();
    $content =  ob_get_clean();
    echo $content;
    ?>
</div>