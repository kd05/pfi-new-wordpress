<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php
		the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );

		echo '<div class="entry-meta">';
		$byline = '';
		if( get_post_type() == "post" ) {
			// Get the author name; wrap it in a link.
			$byline = sprintf(
			/* translators: %s: post author */
				__( 'by %s', '' ),
				'<span class="author vcard">'.get_the_author().'</span>'
			);
        }

		// Finally, let's write all of this to the page.
		echo '<span class="posted-on">' . gp_time_link() . '</span><span class="byline"> ' . $byline . '</span>';
		echo '</div><!-- .entry-meta -->';
		?>
	</header><!-- .entry-header -->

	<?php if ( '' !== get_the_post_thumbnail() && ! is_single() && !is_search() ) : ?>
		<div class="post-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<?php if(get_post_type() === "post") { ?>
					<?php the_post_thumbnail( 'full' ); ?>
                <?php } else { ?>
					<?php the_post_thumbnail( 'medium' ); ?>
                <?php } ?>
			</a>
		</div><!-- .post-thumbnail -->
	<?php endif; ?>

	<div class="entry-content">
		<?php
		the_excerpt();
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->