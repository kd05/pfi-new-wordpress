<?php
$logo_id = get_theme_mod('custom_logo');
$logo_url = gp_get_img_url($logo_id);

if ( !isset($logo_url) || $logo_url == "" ) {
  $logo_url = get_template_directory_uri() . '/assets/images/logo.png';
}
?>
<style media="screen">
  .nav-logo {
    background-image: url('<?= $logo_url; ?>');
  }

</style>
