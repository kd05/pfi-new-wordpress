<?php get_template_part('parts/nav', 'logo-styles'); ?>
<?php get_template_part('parts/info', 'bar'); ?>
<?php
if(is_front_page()) {
    $svg_log = get_template_directory_uri() . '/assets/images/logo-home.svg';
    $nav_class= "home-topbar";
} else {
    $svg_log = get_template_directory_uri() . '/assets/images/logo.svg';
    $nav_class= "";
}
?>
<nav class="topbar <?php echo $nav_class; ?>" role="navigation" aria-label="Main Navigation">

    <div class="nav-wrapper  hm-container">

        <div class="temp-bar"
             style="width:100%;height:77px;background-color:transparent;position:absolute;top:0;z-index:99;"></div>

        <div class="navtop-left">
            <div class="nav-logo" style="z-index:999;">
                <a href="<?= home_url() ?>"><span class="scr-reader-text">Home</span></a>
            </div>


            <style media="screen">
                .nav-logo-svg{
                    background-image: url('<?= $svg_log; ?>');
                }
            </style>
            <div class="nav-logo-svg" >
                <a href="<?= home_url() ?>"></a>
            </div>
        </div>


        <div class="nav">

            <!-- nav walker goes here -->
            <div class="menu-wrapper2 pages-menu-wrapper2">
				<?php build_topbar_nav() ?>
            </div>

            <div class="menu-wrapper2 language-menu">
                <?php build_topbar_nav() ?>
            </div>
            
            <div class="menu-wrapper">
				<?php // echo get_search_form(); ?>
				<?php build_topbar_nav() ?>
            </div>

            <button id="nav-btn" class="hamburger hamburger--squeeze" type="button" style="z-index:999;"
                    aria-label="Menu" aria-controls="navigation">

                <span class="hamburger-box">
                  <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>

    </div>
</nav>