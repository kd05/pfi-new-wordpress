<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<?php
$featured_image = has_post_thumbnail() ? get_the_post_thumbnail_url(null, 'full') : get_template_directory_uri() . '/assets/images/hero-placeholder.jpg';
?>
<div class="single">
    <div class="grid hero flex ai-center" style="background-image: url(<?php echo $featured_image; ?>);">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <?php the_title( '<h2 class="entry-title bold text-center text-white">', '</h2>' ); ?>
                    <p class="text-white text-center">
                    <?php
                    // Get the author name; wrap it in a link.
                    $byline = sprintf(
                    /* translators: %s: post author */
	                    __( 'by %s', 'wpBabywp' ),
	                    '<span class="author vcard">'.get_the_author().'</span>'
                    );

                    // Finally, let's write all of this to the page.
                    echo '<span class="posted-on">' . gp_time_link() . '</span><span class="byline"> ' . $byline . '</span>';
                    ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="grid">
    <div class="container">
        <div class="row">
            <div class="col s8">
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <div class="entry-content content general-content">
				        <?php
				        /* translators: %s: Name of current post */
				        the_content( sprintf(
					        __( 'Continue reading<span class="screen-reader-text"> "%s"</span> <i class="fa fa-long-arrow-right"></i>', 'wpBabywp' ),
					        get_the_title()
				        ) );

				        wp_link_pages( array(
					        'before'      => '<div class="page-links">' . __( 'Pages:', 'wpBabywp' ),
					        'after'       => '</div>',
					        'link_before' => '<span class="page-number">',
					        'link_after'  => '</span>',
				        ) );
				        ?>
                    </div><!-- .entry-content -->

                </article><!-- #post-## -->
            </div>
            <div class="col s4 jc-end">
                <div class="archive-categories">
			        <?php echo gp_get_categories(); ?>
                </div>
            </div>
        </div>
    </div>
</div>