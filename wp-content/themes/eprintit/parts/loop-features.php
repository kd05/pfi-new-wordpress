<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="row">
        <?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
            <div class="col s3">
                <div class="post-thumbnail">
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail( 'thumbnail' ); ?>
                    </a>
                </div><!-- .post-thumbnail -->
            </div>
        <?php endif; ?>
        <div class="col s8">
            <header class="entry-header">
                <?php
                the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
                ?>
            </header><!-- .entry-header -->
            <div class="entry-content">
                <?php
                the_excerpt();
                ?>
            </div><!-- .entry-content -->
        </div>
    </div>

</article><!-- #post-## -->