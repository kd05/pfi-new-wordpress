<?php add_hero_class(); ?>
<?php get_header(); ?>

<?php
$post_type = get_post_type();
$featured_image = get_template_directory_uri() . '/assets/images/archive-placeholder.jpg';
if($post_type === "features") {
    if(is_tax()) {
        $page_title = get_the_archive_title();
    } else {
	    $page_title = ucfirst($post_type);
    }
} else {
    $blog_page = get_page_by_title('blog');
    $featured_image = has_post_thumbnail($blog_page) ? get_the_post_thumbnail_url($blog_page, 'full') : $featured_image;
    $page_title = get_the_archive_title();
}

?>
    <div id="archives-panel">
        <section class="single">
            <div class="grid hero flex ai-center" style="background-image: url(<?php echo $featured_image; ?>);">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <h2 class="text-center text-white"><?php echo $page_title; ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <div class="row reverse">
                    <div class="col s4 jc-end">
                        <div class="archive-categories">
                            <?php
                            $tax = $post_type === "features" ? "features_cat" : "category";
                            ?>
                            <?php echo gp_get_categories($tax); ?>
                        </div>
                    </div>
                    <div class="col s8">
                        <div class="archive-panel-wrap">
			                <?php
			                if (have_posts()) :

				                /* Start the Loop */
				                while (have_posts()) : the_post();
					                if($post_type === "features") {
						                get_template_part('parts/loop', 'features');
					                } else {
						                get_template_part('parts/loop', 'posts');
					                }
				                endwhile;

			                else :
				                ?>
                                <p class="no-posts"><?php _e('Sorry, no posts matched your criteria.'); ?></p>
				                <?php
			                endif;
			                ?>

                        </div>
                        <div class="pagination">
			                <?php
			                if (function_exists('wp_pagenavi')) {
				                wp_pagenavi();
			                }
			                ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php get_footer();