<?php add_hero_class(); ?>
<?php get_header() ?>
<section>
    <article class="">
        <div class="">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<?php the_content(); ?>

                <!-- TODO: EVERYTHING BELOW IS TEMPORARY UNTIL SHORTCODES ARE MADE -->

                <!--          <div class='shortcode MultiIconPanel flex scroll-animate ai-center'>-->
                <!--              <div class='container'>-->
                <!--                  <div class='row'>-->
                <!--                      <div class='col s12'>-->
                <!--                          <div class='content general-content scroll-animate'>-->
                <!--                              <h2 class="Heading dark">Industries Solutions</h2>-->
                <!--                              <p>Aliquam consectetur nisi enim, et facilisis nibh porta vitae. Morbi accumsan-->
                <!--                                  efficitur turpis nec consectetur. Praesent scelerisque id diam nec interdum.-->
                <!--                                  Sed dictum dolor vel consequat tempus.</p>-->
                <!--                              <div class="icon-wrapper">-->
                <!--                                  <div class="unit">-->
                <!--                                      <img src="--><?php //echo gp_get_img_url(97, 'medium'); ?><!--" alt="">-->
                <!--                                      <h3 class="title">Healthcare</h3>-->
                <!--                                      <p>An excellent way of preparing a project's wireframe.</p>-->
                <!--                                  </div>-->
                <!--                                  <div class="unit">-->
                <!--                                      <img src="--><?php //echo gp_get_img_url(99, 'medium'); ?><!--" alt="">-->
                <!--                                      <h3 class="title">Retail</h3>-->
                <!--                                      <p>An excellent way of preparing a project's wireframe.</p>-->
                <!--                                  </div>-->
                <!--                                  <div class="unit">-->
                <!--                                      <img src="--><?php //echo gp_get_img_url(100, 'medium'); ?><!--" alt="">-->
                <!--                                      <h3 class="title">Public Sector</h3>-->
                <!--                                      <p>An excellent way of preparing a project's wireframe.</p>-->
                <!--                                  </div>-->
                <!--                              </div>-->
                <!--                          </div>-->
                <!--                      </div>-->
                <!--                  </div>-->
                <!--              </div>-->
                <!--          </div>-->

				<?php if (false) { ?>
                    <div class='shortcode PlatformApplicationsPanel flex scroll-animate'>
                        <div class='container'>
                            <div class='row'>
                                <div class='col s12'>
                                    <div class='content general-content scroll-animate'>
                                        <h2 class="Heading dark">
                                            <img src="<?php echo gp_get_img_url(21, 'thumbnail'); ?>" alt="">Platform
                                            Applications</h2>
                                        <p>The next-generation document capture and processing platforms</p>
                                        <div class="unit-wrapper">
                                            <a href="#" class="unit">
                                                <div class="text">
                                                    <h3>Invoice Processing</h3>
                                                    <div class="hidden">
                                                        <p>Maecenas volutpat justo sodales, placerat orci vitae, ornare
                                                            lorem.
                                                            Quisque est mi, tempus sit amet torto et, vehicula
                                                            risus.</p>
                                                    </div>
                                                </div>
                                                <div class="bg-img"
                                                     style="background-image: url('<?php echo gp_get_img_url(102, 'medium'); ?>');"></div>
                                            </a>
                                            <a href="#" class="unit">
                                                <div class="text">
                                                    <h3>Referral Management</h3>
                                                    <div class="hidden">
                                                        <p>Maecenas volutpat justo sodales, placerat orci vitae, ornare
                                                            lorem.
                                                            Quisque est mi, tempus sit amet torto et, vehicula
                                                            risus.</p>
                                                    </div>
                                                </div>
                                                <div class="bg-img"
                                                     style="background-image: url('<?php echo gp_get_img_url(101, 'medium'); ?>');"></div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				<?php } ?>

			<?php endwhile; endif; ?>
        </div>
    </article>
</section>
<?php get_footer() ?>