<?php

// CPT setup
require_once(get_template_directory() . '/inc/customPostType/featured/featured.php');
require_once(get_template_directory() . '/inc/customPostType/testimonials/testimonials.php');
require_once(get_template_directory() . '/inc/customPostType/instructions/instructions.php');
require_once(get_template_directory() . '/inc/customPostType/potentials/potentials.php');
require_once(get_template_directory() . '/inc/customPostType/partners/partners.php');
require_once(get_template_directory() . '/inc/customPostType/accreditations/accreditations.php');
require_once(get_template_directory() . '/inc/customPostType/customers/customers.php');
require_once(get_template_directory() . '/inc/customPostType/team/team.php');
require_once(get_template_directory() . '/inc/customPostType/icons/icons.php');
// customizer setup
require_once(get_template_directory() . '/inc/customizer/customizer.php');
// functions setup
require_once(get_template_directory() . '/inc/functions/functions.php');
// includes all custom shortcodes.
require_once(get_template_directory() . '/inc/shortcodes/shortcodes.php');

// Hide the admin bar
add_filter('show_admin_bar', '__return_false');

/**
 * Gets a nicely formatted string for the published date.
 */
function gp_time_link()
{

	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	$time_string = sprintf($time_string,
		get_the_date(DATE_W3C),
		get_the_date()
	);

	if (get_the_time('U') !== get_the_modified_time('U')) {
		$time_string = '<time class="updated" datetime="%1$s">%2$s</time>';
		$time_string = sprintf($time_string,
			get_the_modified_date(DATE_W3C),
			get_the_modified_date()
		);
	}

// Wrap the time string in a link, and preface it with 'Posted on'.
	return sprintf(
	/* translators: %s: post date */
		__('<span class="screen-reader-text">Posted on</span> %s', 'eprintit'),
		'<a href="' . esc_url(get_permalink()) . '" rel="bookmark">' . $time_string . '</a>'
	);
}

/**
 * Exclude shortcode city from search results
 * Exclude the partner portal template pages
 * @param $wp_query WP_Query
 */
function gp_remove_shortcode_city_from_search($wp_query)
{
	if (!is_admin() && is_search()) {
		// Get the shortcode city page
		$page = get_page_by_title('Shortcode City');
		if ($page) {
			$wp_query->set('post__not_in', array($page->ID));
		}

		// Hide partner portal templates for guest users
		if (!is_user_logged_in()) {
			// Get the template pages
			$pages = get_pages(array(
				'meta_key'    => '_wp_page_template',
				'meta_value'  => 'templates/partner-portal.php',
			));
			if (!empty($pages)) {
			    $ids = wp_list_pluck($pages, 'ID');
			    if($page) {
			        $ids[] = $page->ID;
                }
				$wp_query->set('post__not_in', $ids);
			}
		}

	}
}

add_action('pre_get_posts', 'gp_remove_shortcode_city_from_search');

function gp_get_categories($taxonomy = 'category')
{
	$cats = get_categories(array('taxonomy' => $taxonomy));
	if (!empty($cats)) {
		?>
        <h3 class="archive-title">Categories</h3>
        <ul>
			<?php
			foreach ($cats as $category) {
				?>
                <li>
                    <a href="<?php echo get_term_link($category); ?>"
                       title="<?php echo $category->term_name; ?>"><?php echo $category->name; ?></a>
                </li>
				<?php
			}
			?>
        </ul>
		<?php
	}
}

function gp_render_locations()
{

	// Get csv
	$csv = ABSPATH . '/PPL_Cities.csv';

	// Read
	$handle = fopen($csv, "r");

	// Default locations
	$locations = array();

	if ($handle !== FALSE) {

		// Get the header columns
		$columns = fgetcsv($handle, 1000, ",");

		// Get locations
		while ($row = fgetcsv($handle, 0, ',')) {
			$locations[] = array_combine($columns, $row);
		}

		// Close stream
		fclose($handle);
	}

	?>
    <script type="application/javascript">
        var map_locations = <?php echo wp_json_encode($locations); ?>
    </script>
	<?php

}

/**
 * Filter out the WPForms attributes
 * @param $atts
 * @param $form_data
 * @return mixed
 */
function gp_wpforms_contact_form_atts($atts, $form_data)
{
	// Check for a custom action
	$custom_action = isset($form_data['settings']['form_custom_action']) ? $form_data['settings']['form_custom_action'] : "";
	if (!empty($custom_action)) {
		$atts['atts']['action'] = $custom_action;
		$atts['atts']['enctype'] = ""; // Pardot doesn't allow enctype
	}
	// Return
	return $atts;
}

add_filter('wpforms_frontend_form_atts', 'gp_wpforms_contact_form_atts', 10, 2);

/**
 * Add form action field to WPForms general settings
 * @param $settings
 */
function gp_wpforms_form_settings_general($settings)
{
	wpforms_panel_field(
		'text',
		'settings',
		'form_custom_action',
		$settings->form_data,
		esc_html__('Form Custom Action', 'wpforms'),
		array(
			'default' => $settings->form->custom_action,
		)
	);
}

add_action('wpforms_form_settings_general', 'gp_wpforms_form_settings_general');

function gp_wpforms_process($fields, $entry, $form_data)
{
	echo '<pre>' . print_r($fields, TRUE) . '</pre>';
	echo '<pre>' . print_r($form_data, TRUE) . '</pre>';
	echo '<pre>' . print_r($entry, TRUE) . '</pre>';
	echo '<pre>' . print_r($_POST, TRUE) . '</pre>';
	exit;
}

//add_action('wpforms_process', 'gp_wpforms_process', 10, 3);

/**
 * Redirect Partner after successful login and appends errors to the URL for displaying on the page
 *
 * @param string $redirect_to URL to redirect to.
 * @param string $request URL the user is coming from.
 * @param object $user Logged user's data.
 * @return string
 */

function my_login_redirect($redirect_to, $request, $user)
{
	if (is_wp_error($user) && isset($_SERVER['HTTP_REFERER']) && !strpos($_SERVER['HTTP_REFERER'], 'wp-admin') && !strpos($_SERVER['HTTP_REFERER'], 'wp-login.php')) {
		//Login failed, find out why...
		$error_types = array_keys($user->errors);
		//Error type seems to be empty if none of the fields are filled out
		$error_type = 'both_empty';
		//Otherwise just get the first error (as far as I know there
		//will only ever be one)
		if (is_array($error_types) && !empty($error_types)) {
			$error_type = $error_types[0];
		}
		$login_page = get_page_by_path('partner-login');
		$redirect_to = get_permalink($login_page->ID);
		$redirect_to = add_query_arg(array(
			'login-failed' => $error_type
		), $redirect_to);
		wp_redirect($redirect_to);
		exit;
	} else {
		//is there a user to check?
		if (isset($user->roles) && is_array($user->roles)) {
			//check for partner
			if (in_array('partner', $user->roles)) {
				// redirect them to another URL, in this case, the homepage
				$page = get_page_by_path('partner-portal');
				$redirect_to = get_permalink($page->ID);
			}
		}

		return $redirect_to;
	}
}

add_filter('login_redirect', 'my_login_redirect', 10, 3);

add_filter ('wpforms_frontend_confirmation_message', 'restrict_form_formula', 10, 2);
function restrict_form_formula($confirmation_message, $form_data) {
	if ($form_data['id'] == 5321) {
		$field_staff_collecting_data = $_POST['wpforms']['fields'][1];
		$staff_data_entry = $_POST['wpforms']['fields'][2];
		$forms_per_day_per_staff = $_POST['wpforms']['fields'][3];
		$avg_pages_per_form = $_POST['wpforms']['fields'][4];
		$form_length = $_POST['wpforms']['fields'][5];
		$calculation = ($field_staff_collecting_data * $staff_data_entry) / $forms_per_day_per_staff;
		return $calculation;
	}
	else {
		return $confirmation_message;
	}
}

/**
 * Disable form fields to make read-only
 * To apply, add CSS class 'wpf-disable-field' (no quotes) to field in form builder
 *
 */
function wpf_dev_disable_field() {
	?>
	<script type="text/javascript">

		jQuery(function($) {
			$(".wpf-disable-field input").attr("readonly", true);
		});

	</script>
	<?php
}
add_action( 'wpforms_wp_footer', 'wpf_dev_disable_field' );