<?php
/**
 * Template Name: Contact Page
 */
add_hero_class();
get_header(); ?>

<?php
$slogan = get_field('slogan');
$featured_image = has_post_thumbnail() ? get_the_post_thumbnail_url(null, 'full') : get_template_directory_uri() . '/assets/images/hero-placeholder.jpg';
?>
    <div id="contentPage">
        <section class="single">
            <div class="grid hero flex ai-center" style="background-image: url(<?php echo $featured_image; ?>);">
                <div class="col s12">
                    <h2 class="text-white text-center bold"><?php echo get_the_title(); ?></h2>
					<?php if ($slogan) { ?>
                        <h3 class="subtitle text-center text-white"><?php echo $slogan; ?></h3>
					<?php } ?>
                </div>
            </div>
        </section>
		<?php
		$shortcode_above_content = get_field('shortcode_above_content', false, false);
		$shortcode_above_content = apply_filters('the_content', $shortcode_above_content);
		echo $shortcode_above_content;
		?>
        <section class="grid flex mt-5 mb-5">
            <div class="container">
                <div class="row">
                    <div class="col s6">
                        <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
                            <div class="general-content">
								<?php
								if (have_posts()) :
									while (have_posts()) :
										the_post();
										the_content();
									endwhile;
								endif;
								?>
                            </div>
                        </article>
                    </div>
                    <div class="col s6">
						<?php echo do_shortcode('[wpforms id="3465"]'); ?>
                    </div>
                </div>
            </div>
        </section>
		<?php
		$shortcode_content = get_field('shortcode_content', false, false);
		$shortcode_content = apply_filters('the_content', $shortcode_content);
		echo $shortcode_content;
		?>
    </div>
<?php get_footer();