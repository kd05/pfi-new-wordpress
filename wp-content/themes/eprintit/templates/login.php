<?php
/**
 * Template Name: Login
 */
add_hero_class();
get_header(); ?>
	<section class="grid">
		<div class="container">
			<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
				<div class="general-content">
					<?php
					if (have_posts()) :
						while (have_posts()) :
							the_post();
							the_content();
						endwhile;
					endif;
					?>
					<?php get_sidebar('login'); ?>
				</div>
			</article>
		</div>
	</section>
<?php get_footer();