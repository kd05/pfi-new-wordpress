<?php
/**
 * Template Name: Partner Portal
 */

// Not logged in
if(!is_user_logged_in()) {
	// Redirect to login
	$redirect_page = get_page_by_path('partner-login');
	$redirect_link = get_permalink($redirect_page->ID);
	wp_redirect($redirect_link);
	exit;
}
add_hero_class();
get_header(); ?>
	<section>
		<article class="">
			<div class="">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php the_content(); ?>

				<?php endwhile; endif; ?>
			</div>
		</article>
	</section>
<?php get_footer();