<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
?>
<!-- Theme Template Code -->
<div id="login-register-password">

	<?php global $user_ID, $user_identity, $current_user, $pardotURL;
	wp_get_current_user();
	$logoutredirect = 'partner-login';
	if (!$user_ID) { ?>
        <div class="tab_container_login">
            <div id="tab1_login" class="tab_content_login">

				<?php $register = $_GET['register'];
				$reset = $_GET['reset'];
				if ($register == true) { ?>

                    <h3>Success!</h3>
                    <p>Check your email for the password and then return to log in.</p>

				<?php } elseif ($reset == true) { ?>

                    <h3>Success!</h3>
                    <p>Check your email to reset your password.</p>

				<?php } else {
					$error = $_GET['login-failed'];

					switch ($error) {
						case 'both_empty':
							echo "<p class='login-error'><strong>Please enter your credentials below to login.</strong></p>";
							break;
						case 'empty_password':
							echo "<p class='login-error'><strong>Please enter your password below.</strong></p>";
							break;
						case 'invalid_username':
							echo "<p class='login-error'><strong>Please enter your email below.</strong></p>";
							break;
						case 'incorrect_password':
							echo "<p class='login-error'><strong>Incorrect password, re-enter your password below or <a href='". get_bloginfo('url') ."/my-account/lost-password/'>reset your password</a>.</strong></p>";
							break;
						case 'empty_username':
							echo "<p class='login-error'><strong>Please enter your email below.</strong></p>";
							break;
					}
					?>

				<?php }
				echo "<p>Use your email to login to the partner portal or <a href='". get_bloginfo('url') ."/my-account/lost-password/'>reset your password</a>.</p>";

				$args = array(
					'form_id'        => 'loginform-partner',
					'label_username' => __('Email'),
					'label_password' => __('Password'),
					'label_remember' => __('Remember Me'),
					'label_log_in'   => __('Login'),
					'remember'       => true
				);
				wp_login_form($args);

				?>
            </div>
        </div>

	<?php } else { // is logged in
		?>

        <div class="sidebox">
            <div class="usericon">
				<?php global $userdata;
				echo get_avatar($userdata->ID, 60); ?>
            </div>
            <div class="userinfo">
                <p>You&rsquo;re logged in as <strong><?php echo $user_identity; ?></strong></p>
                <p>
                    <a href="<?php echo wp_logout_url('partner-login'); ?>">Log out</a>
                </p>
            </div>
        </div>

	<?php } ?>

</div>