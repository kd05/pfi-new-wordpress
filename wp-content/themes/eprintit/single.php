<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
add_hero_class();
get_header(); ?>

    <div id="single-post">
        <?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();
					get_template_part( 'parts/content', 'post' );
				endwhile; // End of the loop.
        ?>
    </div>

<?php get_footer();