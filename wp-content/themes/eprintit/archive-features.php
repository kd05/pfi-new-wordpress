<?php add_hero_class(); ?>
<?php get_header(); ?>

<?php
$featured_image = get_template_directory_uri() . '/assets/images/archive-placeholder.jpg';
?>
    <div id="archives-panel">
        <section class="single">
            <div class="grid hero flex ai-center" style="background-image: url(<?php echo $featured_image; ?>);">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <h2 class="text-center text-white"><?php echo __('Features', 'wpBabywp'); ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <div class="row reverse">
                    <div class="col s4 jc-end">
                        <div class="archive-categories">
                            <?php echo gp_get_categories('features_cat'); ?>
                        </div>
                    </div>
                    <div class="col s8">
                        <div class="archive-panel-wrap">
                            <?php
                            if (have_posts()) :

                                /* Start the Loop */
                                while (have_posts()) : the_post();
                                    get_template_part('parts/loop', 'features');
                                endwhile;

                            else :
                                ?>
                                <p class="no-posts"><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                                <?php
                            endif;
                            ?>

                        </div>
                        <div class="pagination">
                            <?php
                            if (function_exists('wp_pagenavi')) {
                                wp_pagenavi();
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php get_footer();