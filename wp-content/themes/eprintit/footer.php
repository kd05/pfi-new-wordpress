<?php
$url = get_template_directory_uri();

$footer_logo_image = get_theme_mod('footer_logo_image');
$footer_copyright = get_theme_mod('footer_copyright');

?>
</main>


<div class="site-footer">

    <div class="footer-container">
        <div class="footer-left">
            <div class="f-logo">
                <img src="<?php echo $footer_logo_image; ?>" alt="">
            </div>
            <div class="f-menu">
                <?php

                $args = array(
                    'menu' => 'PFI Footer Menu',
                    'container' => false,
                    'depth' => '1',
                );
                wp_nav_menu($args);
                ?>
            </div>
        </div>

        <div class="footer-right">
            <p class="copy"><?php echo $footer_copyright; ?></p>
<!--            <p>Website by GeekPower Web Design In Toronto</p>-->
        </div>

    </div>

</div>

<?php wp_footer(); ?>
</body>
</html>