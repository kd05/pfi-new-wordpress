<?php add_hero_class(); ?>
<?php get_header(); ?>
<?php
/**
 * Template part for displaying posts
 *
 * Used for single, index, archive, search.
 */
/*the_title(); */
global $post;

$id = get_the_ID();
$img_url = "";

if (has_post_thumbnail($post->ID)) {
	$img_url = gp_get_img_url_from_post($post->ID, 'full');
}
$temp_date = get_the_date('d M y');
$temp_dir = get_template_directory_uri();
$date = explode(" ", $temp_date);

$slogan = get_field('slogan') ? get_field('slogan') : "Alone we can do so little, together we can do so much.";

$title = get_the_title();
$content = get_the_content();

$position = get_field('team_position');

$quote = get_field('quote');
$quote = do_shortcode($quote);
$bg_img = gp_get_img_url(3491);

?>
    <section class='team'>
        <article>

            <div class="grid hero flex ai-center bg-orange" style="background-image: url(<?php echo $bg_img; ?>);">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <h2 class='bold text-center mb-1 text-white'>Our Team</h2>
                            <h3 class='slogan text-center text-white'><?= $slogan ?></h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="grid breadcrumbs">
                <div class="container bg-white flex p0">
                    <div class="crumb home text-white"><a href="<?php echo get_bloginfo('url'); ?>">Home</a></div>
                    <div class="crumb post-type"><span>Our Team</span></div>
                    <div class="crumb post-name"><span><?= $title ?></span></div>
                </div>
            </div>

            <div class="grid header mt-5">
                <div class="container">
                    <div class="row">
                        <div class="col s6">
                            <div class="featured-img-container">
                                <figure>
                                    <img class='circle' src="<?= $img_url ?>" alt="">
                                </figure>
                            </div>
                        </div>
                        <div class="col s6">

                            <div class="title-container">
                                <h2 class='name bold'><?= $title ?></h2>
                                <div class="bar"></div>
                                <p class='position mb-1'> <?= $position ?></p>
                            </div>

                            <div class="quote-content">
                                <span class="quote-icon med"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/big-quote.png" alt=""/></span>
		                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			                        <?php get_template_part('parts/loop', 'single'); ?>
		                        <?php endwhile; endif; ?>
                            </div>

                            <?php // The content was switched with quote because the client entered all the quotes in content area in the backend ?>
                            <div class="content general-content">
                                <?php echo $quote; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </article>
    </section>
<?php get_footer();