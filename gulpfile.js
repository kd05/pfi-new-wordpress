var gulp = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    filter = require('gulp-filter'),
    plugin = require('gulp-load-plugins')();

// Set local URL if using Browser-Sync
const LOCAL_URL = 'localhost',

    // Settings for build of assets and jshint
    SOURCE = {
        scripts: [
            // you can add more js file dir here
            './wp-content/themes/eprintit/assets/scripts/js/**/*.js'
        ],
        sass: './wp-content/themes/eprintit/assets/styles/sass/**/*.+(scss|sass)',
        images: './wp-content/themes/eprintit/assets/images/**/*',
        php: ['**/*.php', '!inc/**/*.php']
    },

    ASSETS = {
        styles: 'wp-content/themes/eprintit/assets/styles/dist/',
        scripts: 'wp-content/themes/eprintit/assets/scripts/dist/',
        images: 'wp-content/themes/eprintit/assets/images/',
        all: 'wp-content/themes/eprintit/assets/'
    },

    JSHINT_CONFIG = {
        "node": true,
        "globals": {
            "document": true,
            "jQuery": true
        }
    };

// GULP FUNCTIONS
// JSHint, concat, and minify JavaScript
gulp.task('scripts', function() {

    // Use a custom filter so we only lint custom JS
    const CUSTOMFILTER = filter(ASSETS.scripts + 'js/**/*.js', {restore: true});

    return gulp.src(SOURCE.scripts)
        .pipe(plugin.plumber(function(error) {
            gutil.log(gutil.colors.red(error.message));
            this.emit('end');
        }))
        .pipe(plugin.sourcemaps.init())
        .pipe(plugin.babel({
            presets: ['env'],
            "plugins": [
                "babel-polyfill",
                'syntax-async-functions'
            ],
            compact: true,
            ignore: ['what-input.js']
        }))
        .pipe(CUSTOMFILTER)
        .pipe(plugin.jshint(JSHINT_CONFIG))
        .pipe(plugin.jshint.reporter('jshint-stylish'))
        .pipe(CUSTOMFILTER.restore)
        .pipe(plugin.concat('scripts.js'))
        .pipe(plugin.uglify())
        .pipe(plugin.sourcemaps.write('.')) // Creates sourcemap for minified JS
        .pipe(gulp.dest(ASSETS.scripts))
});

// Compile Sass, Autoprefix and minify
gulp.task('sass', function() {
    return gulp.src(SOURCE.sass)
        .pipe(plugin.plumber(function(error) {
            gutil.log(gutil.colors.red(error.message));
            this.emit('end');
        }))
        .pipe(plugin.sourcemaps.init())
        .pipe(plugin.sass())
        .pipe(plugin.autoprefixer({
            browsers: [
                'last 2 version', 'safari 5', 'ie 9','ie 10', 'ie 11', 'opera 12.1', 'ios 6', 'android 4'
            ]
        }))
        .pipe(plugin.cssnano())
        .pipe(plugin.sourcemaps.write('.'))
        .pipe(gulp.dest(ASSETS.styles));
});

gulp.task('styles', gulp.parallel('sass'));

// Browser-Sync watch files and inject changes
gulp.task('browsersync', function() {

    gulp.watch(SOURCE.sass, gulp.parallel('sass'));
    gulp.watch(SOURCE.scripts, gulp.parallel('scripts'));

});